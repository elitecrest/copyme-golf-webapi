﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
//using System.Web.Script.Serialization;
using Ikkos.Models;
using System.Data.Entity;
using Ikkos.Models.DAL;
//using System.Data.Objects.DataClasses;
using System.Drawing;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net.Mail;

namespace Ikkos.Controllers
{
    public class VideosV2Controller : ApiController
    {
        public static string Message = "";
        Ikkos.Utility.CustomResponse Result = new Utility.CustomResponse();


        [HttpPost]
        public Utility.CustomResponse AdminAddVideo_V2(VideosInfoDTO video)
        {
            try
            {
                // we need to get the role for the user who posted the video and then if he is admin we need to place video status as Approved else pending

                Random r = new Random();
                string filename = "" + r.Next(999999) + DateTime.Now.Millisecond.ToString() + r.Next(99999).ToString() + "." + video.Format;
                string filename1 = "" + r.Next(999999) + DateTime.Now.Millisecond.ToString() + r.Next(99999).ToString() + "." + video.ThumbnailFormat;

                // windows azure blob uploading
                // ------------------------------------------------------- video uploading stat----------------------------------

                //  UploadVideoFileNew(video.Base64String, "test.3gp");

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccountvideo = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClientvideo = storageAccountvideo.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer containervideo = blobClientvideo.GetContainerReference("videosstore");

                // Create the container if it doesn't already exist.
                containervideo.CreateIfNotExists();


                containervideo.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess = Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideo = containervideo.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.


                byte[] binarydatavideo = Convert.FromBase64String(video.Base64String);

                //using (var fileStream = binarydata)
                //{
                blockBlobvideo.Properties.ContentType = "video/" + video.Format;
                blockBlobvideo.UploadFromByteArray(binarydatavideo, 0, binarydatavideo.Length);

                //UploadFromStream(fileStream);
                //}


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideourl = containervideo.GetBlockBlobReference(filename);

                string imageurlvideo = blockBlobvideourl.Uri.ToString();

                video.URL = imageurlvideo;


                // blob addition end
                //      ----------------------------video uploading end-----------------------------------------------------------


                //thumbnail start
                // windows azure blob uploading

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccountimage = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClientimage = storageAccountimage.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer containerimage = blobClientimage.GetContainerReference("imagesstore");

                // Create the container if it doesn't already exist.
                containerimage.CreateIfNotExists();


                containerimage.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobimage = containerimage.GetBlockBlobReference(filename1);

                // Create or overwrite the "myblob" blob with contents from a local file.


                byte[] binarydataimage = Convert.FromBase64String(video.ThumbnailString);

                blockBlobimage.Properties.ContentType = "image/" + video.ThumbnailFormat;
                blockBlobimage.UploadFromByteArray(binarydataimage, 0, binarydataimage.Length);

                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobimageurl = containerimage.GetBlockBlobReference(filename1);

                string imageurlimage = blockBlobimageurl.Uri.ToString();
                video.ThumbNailURL = imageurlimage;

                // we need to get the role for the user who posted the video and then if he is admin we need to place video status as Approved else pending
                video.Status = 1;
                int vid = DAL2.AdminAddVideo_V2(video);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = vid;
                Result.Message = CustomConstants.Video_Added_Successfully;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse UpdateVideo_V2(VideosInfoDTO video)
        {
            try
            {
                if (UsersDAL.CheckVideoExists(video.ID))
                {
                    DAL2.UpdateVideo_V2(video);
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.Video_Updated_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Video_DoesNot_Exists;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse UpdateProcessedVideo_V2(ProcessedVideoDTO ProcessedVideo)
        {
            if (UsersDAL.CheckVideoExists(ProcessedVideo.VideoID))
            {
                DAL2.UpdateProcessedVideo_V2(ProcessedVideo);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = ProcessedVideo.VideoID;
                Result.Message = CustomConstants.Video_Updated_Successfully;
            }
            else
            {
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = CustomConstants.Video_DoesNot_Exists;
            }

            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddUserRecordedVideo(RecordedVideoInfoDTO RecVideodto)
        {
            var video = DAL2.AddUserRecordedVideo(RecVideodto);
            if (video > 0)
            {
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = video;
                Result.Message = CustomConstants.Video_Added_Successfully;
            }
            else
            {
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = "Recorded Video Adding Failed";
            }

            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse UpdateVideoThumbnail(RecordedThumbnailDTO thumbnailDto)
        {
            var video = DAL2.UpdateVideoThumbnail(thumbnailDto);
            if (video.Id > 0)
            {
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = video;
                Result.Message = CustomConstants.ThumbnailUpdateSuccess;
            }
            else
            {
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = CustomConstants.ThumbnailUpdateFail;
            }

            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetRecordedVideosByUserId(int UserId)
        {
            try
            {
                List<RecordedVideoFullDTO> videos = new List<RecordedVideoFullDTO>();

                if (UserId > 0)
                {
                    videos = DAL2.GetRecordedVideosByUserId(UserId);

                    if (videos.Count > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = videos;
                        Result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = "Access Denied";
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse DeleteRecordedVideo(int videoId, int userid)
        {
            try
            {
                int VideoId = DAL2.DeleteRecordedVideo(videoId, userid);
                if (VideoId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videoId;
                    Result.Message = CustomConstants.Video_Deleted_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Video_DoesNot_Exists;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public Utility.CustomResponse GetPromotionVideo()
        {
            try
            {
                var video = DAL2.GetPromotionVideo();
                if (video.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = video;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

    }
}


