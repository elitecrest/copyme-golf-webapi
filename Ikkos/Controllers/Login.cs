﻿using Ikkos.Models;
using Ikkos.Models.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Security;
using WebMatrix.WebData;
 

namespace Ikkos.Controllers
{
    public class LoginController : ApiController
    {

        public static string customerkey = System.Configuration.ConfigurationManager.AppSettings["CustomerKey"].ToString();
        public static string customersecret = System.Configuration.ConfigurationManager.AppSettings["CustomerSecret"].ToString();
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();
        public static Utility.CustomResponse res = new Utility.CustomResponse();
        
        [HttpPost]
        public Utility.CustomResponse Registration(string userName, string password)
        {
            try
            {

                WebSecurity.CreateUserAndAccount(userName, password);          
               
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = userName;
                Result.Message = CustomConstants.Registered_Successfully;


            }
            catch (MembershipCreateUserException e)
            {
                ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse LoginUser(string userName, string password)
        {
            try
            {
                bool isLogin = WebSecurity.Login(userName, password, persistCookie: true);

                if (isLogin)
                {
                  
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.Log_In_Successful;
                    Result.Response = userName;
                  
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.User_Does_Not_Exist;
                }
                // todo: add other conditions
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpPost]
        public Utility.CustomResponse ForgotPassword(string email)
        {

            try
            {
               string token = WebSecurity.GeneratePasswordResetToken(email);
               int isSent = SendMail(email, token);
               // string confirmPassword = "123456";
               //bool isReset = ResetPassword(token, confirmPassword);
               if (isSent == 0)
               {
                   Result.Status = Utility.CustomResponseStatus.Successful;                 
                   Result.Message = CustomConstants.MailSent;

               }
               else
               {
                   Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                   Result.Message = CustomConstants.Email_Not_sent;
               }
                // todo: add other conditions
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }

        }

        public static int SendMail(string username, string token)
        {
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ForgotPassword.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$Name$$", username);
              //  myString = myString.Replace("$$UserEmail$$", email);
             //   myString = myString.Replace("$$Password$$", password);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + username + ""));
                Msg.Subject = " Forgot Password";
                Msg.Body = "hi";// myString.ToString();
                Msg.IsBodyHtml = true;
                SmtpClient SmtpMail = new SmtpClient();
                SmtpMail.Host = "smtp.gmail.com";
                SmtpMail.Port = 587;
                SmtpMail.EnableSsl = true;
                SmtpMail.UseDefaultCredentials = false;
                SmtpMail.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());
                SmtpMail.Timeout = 1000000;
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                SmtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ResetPassword(string token, string confirmPassword)
        {
            bool isReset = false;
            try
            {
                isReset = WebSecurity.ResetPassword(token, confirmPassword);
                if (isReset)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.ResetPassword;

                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Email_Not_sent;
                }
            

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
            return Result;
        }

        [HttpPut]
        public Utility.CustomResponse ChangePassword(string name, string oldpassword, string newpassword)
        {
            Ikkos.Utility.CustomResponse Result1 = new Utility.CustomResponse();

            try
            {
                if (WebSecurity.ChangePassword(name, oldpassword, newpassword))
                {
                    Result1.Status = Utility.CustomResponseStatus.Successful;
                    Result1.Message = CustomConstants.UpdateUser_Update_Successfully;
                }
                else
                {
                    Result1.Status = Utility.CustomResponseStatus.Successful;
                    Result1.Message = CustomConstants.Password_MisMatch;
                }

            }
            catch (Exception ex)
            {
                Result1.Status = Utility.CustomResponseStatus.Exception;
                Result1.Message = ex.Message;
            }
            return Result1;
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }


        //WordPress APIS


        public Utility.CustomResponse GetUserSubscription(int UserId)
        {
            try
            {
                bool isaccess = false;
                string content = string.Empty;
                string content1 = string.Empty;
                CustomerDTO customerDTO = new CustomerDTO();

                //List<Download> downloadDTO = new List<Download>();
               // List<Download> downloadDTO = new List<Download>();
                string email = UsersDAL.GetEmailByUserId(UserId);
                string api = "wc-api/v2/customers/email/" + email + "?consumer_key=" + customerkey + "&consumer_secret=" + customersecret;
                //string api = "/demo/wc-api/v2/customers/email/" + email + "?consumer_key=" + customerkey + "&consumer_secret=" + customersecret;
                JavaScriptSerializer serializer1 = new JavaScriptSerializer();
                serializer1.MaxJsonLength = 1000000000;
                Newtonsoft.Json.Linq.JArray resp1 = null;
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("https://ikkos.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync(api).Result;
                if (response.IsSuccessStatusCode)
                {
                    Stream st = response.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(st);
                    content = reader.ReadToEnd(); 
                    if(content.Contains("errors"))
                    {
                        isaccess = false;
                    }
                    else
                    {
                        customerDTO = JsonDeserialize<CustomerDTO>(content);
                        if(customerDTO != null)
                        {
                            int custid = customerDTO.customer.id;
                            string downloadApi = "wc-api/v2/customers/" + custid + "/downloads?consumer_key=" + customerkey + "&consumer_secret=" + customersecret;
                           // string downloadApi = "/demo/wc-api/v2/customers/" + custid + "/downloads?consumer_key=" + customerkey + "&consumer_secret=" + customersecret;
                            HttpResponseMessage response1 = client.GetAsync(downloadApi).Result;
                            if (response1.IsSuccessStatusCode)
                            {
                                Stream st1 = response1.Content.ReadAsStreamAsync().Result;
                                StreamReader reader1 = new StreamReader(st1);
                                content1 = reader1.ReadToEnd(); 
                                DownloadDTO downloadDTO = JsonDeserialize<DownloadDTO>(content1); 
                                List<Download> download = downloadDTO.downloads.ToList();
                                if (download.Count == 0)
                                {
                                    isaccess = false;
                                }
                                else
                                {
                                    string accessExpries = download.First().access_expires;
                                    if (Convert.ToDateTime(accessExpries).AddDays(1) >=  DateTime.Now)
                                    {
                                        isaccess = true;
                                    }
                                }
                            
                            }

                        }
                    }

                }


                Result.Status = Utility.CustomResponseStatus.Successful;               
                Result.Response = isaccess;


                // todo: add other conditions
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }

   

        public static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }
        
    }
}
