﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
//using System.Web.Script.Serialization;
using Ikkos.Models;
using System.Data.Entity;
using Ikkos.Models.DAL;
//using System.Data.Objects.DataClasses;
using System.Drawing;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net.Mail;
using System.Web.Security;
using WebMatrix.WebData;


namespace Ikkos.Controllers
{
    public class UsersController : ApiController
    {
        public static string Message = "";
        Ikkos.Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse RegisterDevice(string deviceId, int deviceType)
        {
            try
            {
                var currentDevice = UsersDAL.RegisterDevice(deviceId, deviceType);

                if (currentDevice.AppID.Length > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = currentDevice;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Access_Denied;
                }
                // todo: add other conditions
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public Utility.CustomResponse Login(string userName, string password)
        {
            try
            {
                bool isLogin = WebSecurity.Login(userName, password, persistCookie: true);
                if (isLogin)
                {
                    var currentUser = UsersDAL.Login(userName, password);

                    if (currentUser.ID > 0)
                    {
                        //currentUser.UserSports = null;
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = currentUser;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.User_Does_Not_Exist;
                    }
                    // todo: add other conditions
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.User_Does_Not_Exist;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public Utility.CustomResponse AdminLogin(string userName, string password)
        {
            try
            {
                int role = UsersDAL.GetUserRoleByEmail(userName);
                // todo: get user id from username.
                if (role == 1)
                {
                    var currentUser = UsersDAL.Login(userName, password, isAdmin: true);

                    if (currentUser.ID > 0)
                    {

                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = currentUser;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.InValid_Credential;
                    }
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Access_Denied;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpPost]
        public Utility.CustomResponse ForgotPassword(string userName)
        {
            //algoritham 1. we need to search for corresponding email id and prepare message and send it to related email.
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {
                if (WebSecurity.UserExists(userName))
                {
                    string token = WebSecurity.GeneratePasswordResetToken(userName);

                    //need to send a mail to user

                    if (SendMail(userName, token) == 0)
                    {
                        res.Status = Utility.CustomResponseStatus.Successful;
                        res.Message = CustomConstants.ForgotPassword_successfully;
                    }
                    else
                    {
                        res.Status = Utility.CustomResponseStatus.UnSuccessful;
                        res.Message = Message;

                        //"Failed to Send Email, Please try again later";
                    }
                }
                else
                {
                    res.Status = Utility.CustomResponseStatus.UnSuccessful;
                    res.Message = CustomConstants.User_Does_Not_Exist;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }

            return res;
        }

      

        [HttpGet]
        public Utility.CustomResponse ResetPassword(string token, string confirmPassword)
        {
            bool isReset = false;
            try
            {
                isReset = WebSecurity.ResetPassword(token, confirmPassword);
                if (isReset)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.ResetPassword;

                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.ResetPassword_Failed;
                }


            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
            return Result;
        }


        public static int SendMail(string username, string token)
        {
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ForgotPassword.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$Name$$", username);
                myString = myString.Replace("$$Token$$", token);
                //   myString = myString.Replace("$$Password$$", password);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + username + ""));
                Msg.Subject = " Forgot Password";
                Msg.Body = myString.ToString();
                Msg.IsBodyHtml = true;
                SmtpClient SmtpMail = new SmtpClient();
                SmtpMail.Host = "smtp.gmail.com";
                SmtpMail.Port = 587;
                SmtpMail.EnableSsl = true;
                SmtpMail.UseDefaultCredentials = false;
                SmtpMail.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());
                SmtpMail.Timeout = 1000000;
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                SmtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }


        public static int SendMail(string username, string email, string password)
        {
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("../ForgotPassword.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$Name$$", username);
                myString = myString.Replace("$$UserEmail$$", email);
                myString = myString.Replace("$$Password$$", password);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + email + ""));
                Msg.Subject = " Forgot Password";
                Msg.Body = myString.ToString();
                Msg.IsBodyHtml = true;
                SmtpClient SmtpMail = new SmtpClient();
                SmtpMail.Host = "smtp.gmail.com";
                SmtpMail.EnableSsl = true;
                SmtpMail.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());
                SmtpMail.Timeout = 1000000;
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                SmtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }
        [HttpPost]
        public Utility.CustomResponse UserFeedback(FeedbackDTO fb)
        {
            //algoritham 1. we need to search for corresponding email id and prepare message and send it to related email.
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {
                UsersDAL.AddFeedback(fb);
                res.Status = Utility.CustomResponseStatus.Successful;
                res.Message = "Thanks for your feedback !";
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }

            return res;
        }

        [HttpPost]
        public Utility.CustomResponse Registration(UserInfoDTO userDTO)
        {
            try
            {

                // userDTO.Country = "USA";
                if (userDTO.PhoneID == 0)
                {
                    userDTO.PhoneID = 9;
                }
                if (!WebSecurity.UserExists(userDTO.Email))
                {
                    WebSecurity.CreateUserAndAccount(userDTO.Email, userDTO.Password);

                    int id = UsersDAL.AddUser(userDTO);
                    if (id > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = id;
                        Result.Message = CustomConstants.Registered_Successfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.User_Exist_With_Email;
                    }
                } 
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.User_Exist_With_Email;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPut]
        public Utility.CustomResponse UpdateUser(UserInfoDTO user)
        {
            Ikkos.Utility.CustomResponse Result1 = new Utility.CustomResponse();

            try
            {
                if (WebSecurity.UserExists(user.Email))
                {
                    if (UsersDAL.CheckUserByEmail(user.Email))
                    {
                        UsersDAL.UpdateUser(user);
                        Result1.Status = Utility.CustomResponseStatus.Successful;
                        Result1.Message = CustomConstants.UpdateUser_Update_Successfully;
                        Result1.Response = user.ID;
                    }
                    else
                    {
                        Result1.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result1.Message = CustomConstants.User_Does_Not_Exist;
                    }
                }
            }
            catch (Exception ex)
            {
                Result1.Status = Utility.CustomResponseStatus.Exception;
                Result1.Message = ex.Message;
            }
            return Result1;
        }

        // Add videorating
        [HttpPost]
        public Utility.CustomResponse AddRating(VideosRatingDTO rating)
        {
            try
            {
                if (rating.UserId != 0)
                {
                    //   rating.UserId =Convert.ToInt32(Userid);
                    UsersDAL.AddRating(rating);
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.Video_rating_Added_Successfully;
                    Result.Response = rating.Rating;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Access_Denied;
                    Result.Response = rating.Rating;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddVisibility(Visibility_DTO visibility)
        {
            try
            {
                if (visibility.UpdatedBy != 0)
                {
                    //   rating.UserId =Convert.ToInt32(Userid);
                    UsersDAL.AddVideoVisibility(visibility);
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.Visibility_Update_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Visibility_Update_Fail;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPut]
        public Utility.CustomResponse ChangePassword(int userid, string oldpassword, string newpassword)
        {
            Ikkos.Utility.CustomResponse Result1 = new Utility.CustomResponse();

            try
            {
                if (UsersDAL.ChangePassword(userid, oldpassword, newpassword) == 1)
                {
                    Result1.Status = Utility.CustomResponseStatus.Successful;
                    Result1.Message = CustomConstants.UpdateUser_Update_Successfully;
                }
                else
                {
                    Result1.Status = Utility.CustomResponseStatus.Successful;
                    Result1.Message = CustomConstants.Password_MisMatch;
                }

            }
            catch (Exception ex)
            {
                Result1.Status = Utility.CustomResponseStatus.Exception;
                Result1.Message = ex.Message;
            }
            return Result1;
        }


        //<summary>This API is used to return UserDetails </summary>
        [HttpGet]
        public Utility.CustomResponse GetUserDetails(string UserName)
        {
            try
            {
                var userDetails = UsersDAL.GetUserDetails(UserName);
                if (userDetails.ID > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = userDetails;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }


        [HttpPost]
        public Utility.CustomResponse ExpertRegistration(ExpertDTO expertDTO)
        {
            try
            {
                if (!UsersDAL.CheckUserByEmail(expertDTO.Email))
                {
                    //expertDTO.Country = "USA";
                    if (expertDTO.PhoneID == 0)
                    {
                        expertDTO.PhoneID = 9;
                    }

                    int userid = UsersDAL.AddExpertUser(expertDTO);

                    if (userid > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = userid;
                        Result.Message = CustomConstants.Registered_Successfully;
                    }

                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.User_Exist_With_Email;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPut]
        public Utility.CustomResponse UpdateExpertUser(ExpertDTO expertDTO)
        {

            try
            {
                if (UsersDAL.CheckUserByEmail(expertDTO.Email))
                {

                    UsersDAL.UpdateExpertUser(expertDTO);
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = expertDTO.ID;
                    Result.Message = CustomConstants.UpdateUser_Update_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.User_Does_Not_Exist;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse DeleteUserPreferences(int userid)
        {
            try
            {
                UsersDAL.DeleteUserPreferences(userid);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.Video_Deleted_Successfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse AddException(ExceptionDTO exceptionDTO)
        {
            try
            {
                int id = UsersDAL.AddException(exceptionDTO);
                if (id > 0)
                {
                    Result.Response = id;
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.Exception_Added_Successfully;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddPayment(PaymentDto paymentDto)
        {
            try
            {
                var paymentId =  UsersDAL.AddPayment(paymentDto);
                Result.Response = paymentId;
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.Payment_added_Successfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetPayment(int userId)
        {
            try
            {
                var payments = UsersDAL.GetPayment(userId);
                Result.Response = payments;
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.Details_Get_Successfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse UsageTrack()
        {
            try
            {
                var orgs = UsersDAL.UsersTracking();
                if (orgs != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = orgs;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }

        //[HttpPost]
        //public Utility.CustomResponse CreateUserPassword(UserInfoDTO userDTO)
        //{
        //    try
        //    {
        //        if (!UsersDAL.CheckUserByEmail(userDTO.Email))
        //        {
        //            // userDTO.Country = "USA";
        //            if (userDTO.PhoneID == 0)
        //            {
        //                userDTO.PhoneID = 9;
        //            }
        //            var _response = WebSecurity.CreateUserAndAccount(userDTO.Email, userDTO.Password,
        //          new
        //          {
        //              FirstName = userDTO.FirstName,
        //              LastName = userDTO.LastName,
        //              Email = userDTO.Email,                       
        //              PhoneNumber = userDTO.PhoneNumber,
        //              Address1 = userDTO.Address1,
        //              UserName = userDTO.UserName,
        //              PhoneID = userDTO.PhoneID,
        //              DateCreated = DateTime.Now,
        //              CreatedByUserId = -1 // This is obviously a gaff TODO: Decouple User from Base as not all of these fields are appropriate.
        //          });

        //            //if (id > 0)
        //            //{
        //            //    Result.Status = Utility.CustomResponseStatus.Successful;
        //            //    Result.Response = id;
        //            //    Result.Message = CustomConstants.Registered_Successfully;
        //            //}
        //            //else
        //            //{
        //            //    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
        //            //    Result.Message = CustomConstants.User_Exist_With_Email;
        //            //}
        //        }
        //        else
        //        {
        //            Result.Status = Utility.CustomResponseStatus.UnSuccessful;
        //            Result.Message = CustomConstants.User_Exist_With_Email;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Result.Status = Utility.CustomResponseStatus.Exception;
        //        Result.Message = ex.Message;
        //    }
        //    return Result;
        //}

    }
}
