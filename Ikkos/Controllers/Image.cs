﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
//using System.Web.Script.Serialization;
using Ikkos.Models;
using System.Data.Entity;
using Ikkos.Models.DAL;
 
using System.Drawing;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net.Mail;

//using Microsoft.SqlServer.Dac;

namespace Ikkos.Controllers
{
    public class ImageController : ApiController
    {
        public static string Message = "";
        Ikkos.Utility.CustomResponse Result = new Utility.CustomResponse();
        // JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();


        [HttpPost]
        public Utility.CustomResponse UploadImage(ImageDTO objimage)
        {
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999) + r.Next(9999).ToString() + DateTime.Now.Millisecond;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydata = Convert.FromBase64String(objimage.binary);

                //using (var fileStream = binarydata)
                //{
                blockBlob.Properties.ContentType = "image/jpeg";
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);

                //UploadFromStream(fileStream);
                //}


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                string imageurl = blockBlob1.Uri.ToString();
                //                UsersDAL.StoreImageInDB(imageurl, objimage.userid); -- todo: rohan
                //UsersDAL.StoreImageInDB(imageurl, 1);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = imageurl;
                Result.Response = imageurl;

                return Result;
            }
            catch (Exception ex)
            {
                //lblstatus.Text = "Failed to upload image to Azure Server";
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = "Failed to add image";
            }

            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse ShareImage(ImageDTO objimage)
        {
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999) + r.Next(9999).ToString() + DateTime.Now.Millisecond;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydata = Convert.FromBase64String(objimage.binary);

                //using (var fileStream = binarydata)
                //{
                blockBlob.Properties.ContentType = "image/jpeg";
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);

                //UploadFromStream(fileStream);
                //}


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                string imageurl = blockBlob1.Uri.ToString();
                // -- todo: rohan - save in db so we can clean up later.
                // UsersDAL.StoreImageInDB(imageurl, 1);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = imageurl;
                Result.Response = imageurl;

                return Result;
            }
            catch (Exception ex)
            {
                //lblstatus.Text = "Failed to upload image to Azure Server";
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = "Failed to add image";
            }

            return Result;
        }
        [HttpPost]
        public Utility.CustomResponse GetDisplayImage(DisplayImageDTO Image)
        {
            try
            {
                var ImageURL = UsersDAL.GetDisplayImage(Image);
                if (ImageURL.PhoneID != "")
                {
                    Result.Response = ImageURL;
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = "";
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = "Failed to add image";

                //Result.Status = Utility.CustomResponseStatus.Exception;
                //Result.Message = ex.Message;
            }
            return Result;
        }
    }
}
