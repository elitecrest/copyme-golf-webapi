USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[AddUserPreferences]    Script Date: 27-11-2014 11:50:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

CREATE PROCEDURE [dbo].[DeleteUserPreferences] 
	@UserID [int] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
    Delete from UserPreference where UserId = @UserID
   
END

GO


