USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[DeleteVideo]    Script Date: 21-11-2014 14:33:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Alter PROCEDURE [dbo].[DeleteVideo] 
	@VideoID [int],
	@UserID [int]
AS
BEGIN
	
	SET NOCOUNT ON;
	
    declare @type int 
	select @type = [type] from [user] where Id = @UserID

	if(@type = 1)
	begin
     update video set [Status] = 4, UpdatedBy = @UserID  where [ID] = @VideoID 
    end
	else
	begin
	 update video set [Status] = 4, UpdatedBy = @UserID  where [ID] = @VideoID and OwnerID = @UserID
	end
END
GO


