USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[UpdateUser]    Script Date: 21-11-2014 14:52:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Alter PROCEDURE [dbo].[UpdateUser] 
	@UserID [int],
	@FirstName [varchar](50),
	@LastName [varchar](50),
	@Email [varchar](100), 
	@PhoneNumber [varchar](50),
	@PhoneID [int],
	@OrgID [int],
	@Address1 [varchar](100),
	@Address2 [varchar](100),
	@City [varchar](50),
	@State [varchar](50),
	@Country [varchar](50),
	@Zip [varchar](50),
	@Age [int],
	@Gender [int],
	@Type [int],
	@UpdatedBy [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    update [User] set 
	[FirstName] = @FirstName,
	[LastName] = @LastName,
	[Email] = @Email,
	[PhoneNumber] = @PhoneNumber,
	[PhoneID] = @PhoneID,
	[OrganizationID] = @OrgID,
	[Address1] = @Address1,
	[Address2] = @Address2,
	[City] =  @City,
	[State] = @State,
	[Zip] = @Zip,
	[Country] =  @Country,
	[Age] = @Age,
	[Gender] = @Gender,
	[UpdatedBy] = @UpdatedBy,
	[UpdateDate] = GETUTCDATE() 
	where  [ID] = @UserID

	-- todo: return failure
END
GO


