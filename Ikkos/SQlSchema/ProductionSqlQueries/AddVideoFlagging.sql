USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[AddFlagReason]    Script Date: 20-11-2014 12:27:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 

CREATE PROCEDURE  [dbo].[AddFlagReason]
	@VideoId int,
	@UserId int,
	@FlagId int
	  
AS
DECLARE @cnt int

BEGIN TRY
	 
	SET NOCOUNT ON;
	
    select @cnt = count(*) from VideoFlagging where video=@VideoId

	if(@cnt = 0)
	begin
	Insert into VideoFlagging(Video,[User],FlagId,CreatedBy,CreateDate)Values(@VideoId,@UserId,@FlagId,@UserId,Getdate())
	end

	else
	begin
	update VideoFlagging set FlagId = @FlagId, [User] = @UserId where Video = @VideoId
	end




END TRY



begin catch

end catch

GO


