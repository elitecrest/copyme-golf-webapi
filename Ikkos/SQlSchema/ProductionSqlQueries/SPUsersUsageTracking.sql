USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[AddFlagReason]    Script Date: 26-11-2014 16:21:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 
Create PROCEDURE  [dbo].[UsersUsageTracking]
	@FeatureID int ,	
	@Count int,
	@Value int,
	@User int
	  
AS
 	 BEGIN
	SET NOCOUNT ON;
	 
	Insert into UsageTracking(FeatureID,[Count],Value,[User],[status],CreatedBy,CreateDate)Values(@FeatureID,@Count,@Value,@User,1,@User,Getutcdate())
    END
 

 
 

