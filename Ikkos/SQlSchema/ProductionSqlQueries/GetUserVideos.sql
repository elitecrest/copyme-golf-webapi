USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[GetUserVideos]    Script Date: 20-11-2014 12:35:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Alter PROCEDURE [dbo].[GetUserVideos] 
	@UserID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl,v.type
    from SubCategory c, Video v where v.OwnerID = @UserID and v.[status] <> 3 and c.id = v.SubCategoryID
    
END
GO


