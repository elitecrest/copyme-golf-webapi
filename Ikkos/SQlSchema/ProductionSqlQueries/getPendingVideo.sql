USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[GetVideo]    Script Date: 21-11-2014 10:52:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetPendingVideo] 
	@VideoID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl
    from SubCategory c, Video v where v.[ID] = @VideoID  and c.[Status] = 1 and c.id = v.SubCategoryID
    
END
GO


