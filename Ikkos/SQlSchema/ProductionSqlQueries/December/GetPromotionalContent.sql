USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[GetFeaturedPromotions]    Script Date: 08-01-2015 14:31:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Alter PROCEDURE [dbo].[GetPromotionalContent] 
 
AS
BEGIN
	 
	SET NOCOUNT ON;
	
	Select p.ID,Title,p.Name,Description,ThumbNailURL,a.Name ActionType  from PromotionalContent p
	inner join ActionTypes a on a.id = p.ActionType
    
END
GO


