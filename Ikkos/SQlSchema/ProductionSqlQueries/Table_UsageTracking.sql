USE [copytest]
GO

/****** Object:  Table [dbo].[VideoUsage]    Script Date: 26-11-2014 16:18:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UsageTracking](
	[ID] [int] IDENTITY(0,1) NOT NULL,	
	[FeatureID] [int] NOT NULL,	
	[Count] [int] NOT NULL,
	[Value] [int] NOT NULL,
	[User] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO


