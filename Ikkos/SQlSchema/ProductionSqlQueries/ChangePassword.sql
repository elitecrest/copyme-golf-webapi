USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[ChangePassword]    Script Date: 21-11-2014 11:48:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[ChangePassword]  
	@userid int,
	@oldpassword varchar(256),
    @newpassword varchar(256)
AS
BEGIN
 
	SET NOCOUNT ON;
	declare @currentpassword varchar(256)
	declare @updated int
	select @currentpassword = Password from [User] where [ID] = @userid
	if(@currentpassword = @oldpassword)
		begin    
		   Update [User] set [Password] = @newpassword where [ID] = @userid
		   set @updated = 1
		end
		else
		begin
		  set @updated = 0
		end

		select @updated as Updated
END

GO


