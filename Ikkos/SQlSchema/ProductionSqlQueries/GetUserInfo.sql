USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[GetUserInfo]    Script Date: 28-11-2014 16:27:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




Alter PROCEDURE [dbo].[GetUserInfo] 
 
@UserName [varchar](100) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	
	 Select 	ID,FirstName,LastName,Email,PhoneNumber,OrganizationID,Country,City,Address1,Address2,Age,Gender,[State],[Type],Zip from 
	 [User] where UserName=@UserName
	
END
GO


