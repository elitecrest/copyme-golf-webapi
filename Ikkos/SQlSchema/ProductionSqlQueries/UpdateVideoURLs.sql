USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[UpdateVideoURLs]    Script Date: 28-11-2014 14:46:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Alter PROCEDURE [dbo].[UpdateVideoURLs]   --207,'https://ikkosvideos.blob.core.windows.net/videosstore/90420120855904.mp4','https://ikkosvideos.blob.core.windows.net/imagesstore/30759714513804.png'
	@VideoID [int],
	@VideoURL [varchar](1024),
	@ThumbNailURL [varchar](1024)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	

	if(@ThumbNailURL = '' and @VideoURL != '')
	Begin 
	   update video set [URL] = @VideoURL , [Status] = 7 where ID = @VideoID
	End
	Else
	 if(@ThumbNailURL !='' and @VideoURL = '')
	Begin 
	   update video set ThumbNailURL = @ThumbNailURL, [Status] = 7 where ID = @VideoID
	End
	else
	if(@ThumbNailURL !='' and @VideoURL != '')
	Begin
	update video set [URL] = @VideoURL, ThumbNailURL = @ThumbNailURL, [Status] = 7 where ID = @VideoID
    End
END
GO


