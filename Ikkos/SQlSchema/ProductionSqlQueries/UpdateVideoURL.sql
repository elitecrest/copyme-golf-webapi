USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[UpdateVideo]    Script Date: 26-11-2014 10:52:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 Alter PROCEDURE [dbo].[UpdateVideoThumbURLs]   
    @VideoID [int],
	@UserID [int], 
	@VideoURL [varchar](1024),
	@ThumbNailURL [varchar](1024)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	 
	If(@ThumbNailURL IS NULL or @ThumbNailURL = '')
	BEGIN
	 
	update Video set URL = @VideoURL,  UpdatedBy = @UserID, UpdateDate = GETUTCDATE() 
		where ID=@VideoID
	
    END
	ELSE
	BEGIN
		update Video set URL = @VideoURL, ThumbNailURL = @ThumbNailURL, UpdatedBy = @UserID, UpdateDate = GETUTCDATE() 
		where ID=@VideoID
	END
END
GO


