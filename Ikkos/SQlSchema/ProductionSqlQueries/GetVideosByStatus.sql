USE [copytest]
GO

/****** Object:  StoredProcedure [dbo].[GetVideosWithStatus]    Script Date: 28-11-2014 13:21:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Alter PROCEDURE [dbo].[GetVideosWithStatus] 
	@UserID [int],
	@Status [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    -- todo: optimize per user settings and context
    select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl,v.ownerId
    from SubCategory c, Video v where v.[status] = @Status and c.[Status] = 1 and c.id = v.SubCategoryID
    order by v.[ID]
    
END
GO


