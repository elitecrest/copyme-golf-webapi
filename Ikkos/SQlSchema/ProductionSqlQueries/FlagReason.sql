USE [copytest]
GO

/****** Object:  Table [dbo].[FlagReasons]    Script Date: 20-11-2014 12:26:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FlagReasons](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL
)

GO

SET ANSI_PADDING OFF
GO


