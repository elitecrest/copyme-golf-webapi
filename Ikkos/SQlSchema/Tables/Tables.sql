USE [copytest]
GO
/****** Object:  Table [dbo].[AppID]    Script Date: 05-01-2015 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppID](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[AppID] [varchar](50) NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CategoryList]    Script Date: 05-01-2015 14:53:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryList](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Segment] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[Subcategory] [nvarchar](255) NULL
)

GO
/****** Object:  Table [dbo].[Device]    Script Date: 05-01-2015 14:53:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Device](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[PhoneID] [varchar](50) NOT NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FeaturedVideo]    Script Date: 05-01-2015 14:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeaturedVideo](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Video] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 05-01-2015 14:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Feedback](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[PhoneID] [int] NOT NULL,
	[Question1] [int] NOT NULL,
	[Question2] [int] NOT NULL,
	[Question3] [int] NOT NULL,
	[Comment] [varchar](4096) NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FlagReasons]    Script Date: 05-01-2015 14:53:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlagReasons](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Friend]    Script Date: 05-01-2015 14:53:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Friend](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[FriendEmail] [varchar](100) NOT NULL,
	[FriendID] [int] NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Organization]    Script Date: 05-01-2015 14:53:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Organization](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Email] [varchar](100) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[FaxNumber] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Zip] [varchar](50) NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Points]    Script Date: 05-01-2015 14:53:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Points](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[TrackingID] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
/****** Object:  Table [dbo].[Promotion]    Script Date: 05-01-2015 14:53:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Title] [varchar](100) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](1024) NULL,
	[ThumbNailURL] [varchar](1024) NULL,
	[URL] [varchar](1024) NULL,
	[FeaturedID] [int] NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Session]    Script Date: 05-01-2015 14:53:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Session](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[DeviceID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[LoginTime] [datetime] NOT NULL,
	[LastHeartBeat] [datetime] NOT NULL,
	[LastAction] [int] NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
/****** Object:  Table [dbo].[Status]    Script Date: 05-01-2015 14:53:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Value] [varchar](50) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubCategory]    Script Date: 05-01-2015 14:53:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubCategory](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Category] [varchar](50) NOT NULL,
	[Segment] [varchar](50) NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Type]    Script Date: 05-01-2015 14:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Type](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsageTracking]    Script Date: 05-01-2015 14:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsageTracking](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[FeatureID] [int] NOT NULL,
	[Count] [int] NOT NULL,
	[Value] [int] NOT NULL,
	[User] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
/****** Object:  Table [dbo].[User]    Script Date: 05-01-2015 14:53:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[ShardID] [int] NOT NULL,
	[UserName] [varchar](100) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[Password] [varchar](256) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[PhoneID] [int] NULL,
	[OrganizationID] [int] NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Zip] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Age] [int] NULL,
	[Gender] [int] NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserPreference]    Script Date: 05-01-2015 14:53:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPreference](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[SubCategoryID] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
/****** Object:  Table [dbo].[Version]    Script Date: 05-01-2015 14:53:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Version](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ReleasedBy] [int] NULL,
	[ReleaseDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Video]    Script Date: 05-01-2015 14:53:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Video](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[ShardID] [int] NOT NULL,
	[SubCategoryID] [int] NOT NULL,
	[MovementName] [varchar](50) NOT NULL,
	[ActorName] [varchar](50) NULL,
	[OwnerID] [int] NOT NULL,
	[Format] [varchar](50) NULL,
	[Speed] [varchar](50) NULL,
	[Quality] [varchar](50) NULL,
	[Size] [int] NULL,
	[Duration] [int] NULL,
	[Orientation] [int] NULL,
	[Facing] [int] NULL,
	[AverageRating] [int] NULL,
	[URL] [varchar](1024) NULL,
	[ThumbNailURL] [varchar](1024) NULL,
	[AccessKey] [varchar](1024) NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Videobac]    Script Date: 05-01-2015 14:53:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Videobac](
	[ID] [int] NOT NULL,
	[ShardID] [int] NOT NULL,
	[SubCategoryID] [int] NOT NULL,
	[MovementName] [varchar](50) NOT NULL,
	[ActorName] [varchar](50) NULL,
	[OwnerID] [int] NOT NULL,
	[Format] [varchar](50) NULL,
	[Speed] [varchar](50) NULL,
	[Quality] [varchar](50) NULL,
	[Size] [int] NULL,
	[Duration] [int] NULL,
	[Orientation] [int] NULL,
	[Facing] [int] NULL,
	[AverageRating] [int] NULL,
	[URL] [varchar](1024) NULL,
	[ThumbNailURL] [varchar](1024) NULL,
	[AccessKey] [varchar](1024) NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoFlagging]    Script Date: 05-01-2015 14:53:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoFlagging](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Video] [int] NOT NULL,
	[User] [int] NOT NULL,
	[FlagId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
 CONSTRAINT [PrimaryKey_44d873a0-8d83-4539-b995-00eca6df8442] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[VideoRating]    Script Date: 05-01-2015 14:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoRating](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Video] [int] NOT NULL,
	[User] [int] NOT NULL,
	[Rating] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
/****** Object:  Table [dbo].[VideoUsage]    Script Date: 05-01-2015 14:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoUsage](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Video] [int] NOT NULL,
	[User] [int] NOT NULL,
	[Count] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
