USE [copytest]
GO
/****** Object:  StoredProcedure [dbo].[AddFeedback]    Script Date: 05-01-2015 14:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddFeedback] 
	@PhoneID [int],
	@Name [varchar](100),
	@Question1 [int],
	@Question2 [int],
	@Question3 [int],
	@Comment [varchar](4096)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	insert into [Feedback] ([PhoneID], [Name], [Question1], [Question2], [Question3], [Comment], [Status], [CreatedBy], [CreateDate]) 
	values (@PhoneID, @Name, @Question1, @Question2, @Question3, @Comment, 1, 1, GETUTCDATE())

END
GO
/****** Object:  StoredProcedure [dbo].[AddFlagReason]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 

CREATE PROCEDURE  [dbo].[AddFlagReason]
	@VideoId int,
	@UserId int,
	@FlagId int
	  
AS
DECLARE @cnt int

BEGIN TRY
	 
	SET NOCOUNT ON;
	
    select @cnt = count(*) from VideoFlagging where video=@VideoId

	if(@cnt = 0)
	begin
	Insert into VideoFlagging(Video,[User],FlagId,CreatedBy,CreateDate)Values(@VideoId,@UserId,@FlagId,@UserId,Getdate())
	end

	else
	begin
	update VideoFlagging set FlagId = @FlagId, [User] = @UserId where Video = @VideoId
	end




END TRY



begin catch

end catch

GO
/****** Object:  StoredProcedure [dbo].[AddRating]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

CREATE PROCEDURE  [dbo].[AddRating]
	@VideoId int,
	@UserId int,
	@Rating int
	  
AS
DECLARE @UType int

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	--get usertype from db into varaible
	

	
	SET @UType=(Select Type from [User] where ID=@UserId )

	Insert into VideoRating(Video,[User],Rating,Type,Status,CreatedBy,CreateDate)Values(@VideoId,@UserId,@Rating,@UType,1,@UserId,Getdate())
	



END TRY



begin catch

end catch
GO
/****** Object:  StoredProcedure [dbo].[AddUserPreferences]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[AddUserPreferences] 
	@UserID [int],
	@Category varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 
   declare @SubcategoryId int 
   select @SubcategoryId = Id from SubCategory where Segment = 'Sports' and Name = 'Misc' and Category = @Category
   if(@SubcategoryId > 0)
   Begin
   Insert into UserPreference (UserID,SubCategoryID,Status,CreatedBy,CreateDate) values(@UserID,@SubcategoryId,1,@UserID,getutcdate())
   End
    
END

GO
/****** Object:  StoredProcedure [dbo].[AddVideo]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddVideo] 
	@UserID [int],
	@Segment [varchar](50),
	@Category [varchar](50),
	@SubCategory [varchar](50),
	@MovementName [varchar](50),
	@ActorName [varchar](50),
	@Orientation [int],
	@Facing [int],
	@VideoURL [varchar](1024),
	@ThumbNailURL [varchar](1024),
	@Status [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
   	DECLARE @catID int = 0;
   	DECLARE @videoID int = 0;
	SELECT @catID = [ID] from SubCategory where Segment = @Segment and Category = @Category and [Name] = @SubCategory and [status] = 1
    
	if (@catID <> 0)
	BEGIN
		insert into Video (ShardID, SubCategoryID, MovementName, ActorName, OwnerID, Orientation, Facing, AverageRating, URL, ThumbNailURL, [Type], [Status], CreatedBy, CreateDate) 
		values (1, @catID, @MovementName, @ActorName, @UserID, @Orientation, @Facing, 50, @VideoURL, @ThumbNailURL, 1, @Status, @UserID, GETUTCDATE())
		select @videoID = @@IDENTITY
    END
    
	select @videoID as ID
END
GO
/****** Object:  StoredProcedure [dbo].[AddVisibility]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 

CREATE PROCEDURE  [dbo].[AddVisibility]
	@VideoId int,
	@Updatedby int,
	@Visibility int
	  
AS
 

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	--get usertype from db into varaible
	

	
 Update Video set [Type]=@Visibility , UpdatedBy=@Updatedby where ID=@VideoId

END TRY



begin catch

end catch
GO
/****** Object:  StoredProcedure [dbo].[AdminAddVideo]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AdminAddVideo] 
	@UserID [int],
	@Segment [varchar](50),
	@Category [varchar](50),
	@SubCategory [varchar](50),
	@MovementName [varchar](50),
	@ActorName [varchar](50),
	@Orientation [int],
	@Facing [int],
	@VideoURL [varchar](1024),
	@ThumbNailURL [varchar](1024),
	@Status [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
   	DECLARE @catID int = 0;
   	DECLARE @videoID int = 0;
	SELECT @catID = [ID] from SubCategory where Segment = @Segment and Category = @Category and [Name] = @SubCategory and [status] = 1
    
    -- todo: verify user is admin
	if (@catID <> 0)
	BEGIN
		insert into Video (ShardID, SubCategoryID, MovementName, ActorName, OwnerID, Orientation, Facing, AverageRating, URL, ThumbNailURL, [Type], [Status], CreatedBy, CreateDate) 
		values (1, @catID, @MovementName, @ActorName, @UserID, @Orientation, @Facing, 50, @VideoURL, @ThumbNailURL, 1, @Status, @UserID, GETUTCDATE())
		select @videoID = @@IDENTITY
    END
    
	select @videoID as ID
END
GO
/****** Object:  StoredProcedure [dbo].[AllVideos]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AllVideos] 
	@Status [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl
    from SubCategory c, Video v where v.id > 200 and v.[status] = @Status and c.[Status] = @Status and c.id = v.SubCategoryID order by id
    
END
GO
/****** Object:  StoredProcedure [dbo].[ApproveVideo]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ApproveVideo] 
	@VideoID [int],
	@status  [int],
	@updatedby [int] 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 
   update video set status = @status , updatedby = @updatedby where Id=@VideoID       

END
GO
/****** Object:  StoredProcedure [dbo].[ChangePassword]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[ChangePassword]  
	@userid int,
	@oldpassword varchar(256),
    @newpassword varchar(256)
AS
BEGIN
 
	SET NOCOUNT ON;
	declare @currentpassword varchar(256)
	declare @updated int
	select @currentpassword = Password from [User] where [ID] = @userid
	if(@currentpassword = @oldpassword)
		begin    
		   Update [User] set [Password] = @newpassword where [ID] = @userid
		   set @updated = 1
		end
		else
		begin
		  set @updated = 0
		end

		select @updated as Updated
END


GO
/****** Object:  StoredProcedure [dbo].[CheckAdminEmail]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CheckAdminEmail]   
@Email varchar(100)
AS
BEGIN
 
	SET NOCOUNT ON;
 
 select [Type] from [User] where email = @Email  
    
END
GO
/****** Object:  StoredProcedure [dbo].[CheckUserEmail]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CheckUserEmail] 
@Email varchar(100)
AS
BEGIN
 
	SET NOCOUNT ON;
 
 select count(*) count from [User] where email = @Email 
    
END
GO
/****** Object:  StoredProcedure [dbo].[CheckVideoExists]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [dbo].[CheckVideoExists]
@VideoId varchar(100)
AS
BEGIN
 
	SET NOCOUNT ON;
 
 select count(*) cnt from Video where ID = @VideoId
END
GO
/****** Object:  StoredProcedure [dbo].[CreateVideo]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CreateVideo] 
	@ShardID [int],
	@SubCategoryID [int],
	@MovementName [varchar](50),
	@ActorName [varchar](50),
	@Orientation [int],
	@Facing [int],
	@URL [varchar](1024),
	@Size [int],
	@Duration [int],
	@OwnerID [int],
	@Quality [varchar](200),
	@Speed [varchar](100),
	@Format [varchar](100),
	@Type [int],
	@ThumbNailURL [varchar](1024),
	@Status [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
   	 
	insert into Video (ShardId,SubcategoryId,MovementName,ActorName,OwnerId,[Format],Speed,Quality,Size,Duration,Orientation,Facing,URL,ThumbNailURL,[Type],[Status],CreateDate,CreatedBy) values (@ShardID,@SubCategoryID,@MovementName,@ActorName,@OwnerID,@Format,@Speed,@Quality,@Size,@Duration,@Orientation,@Facing,@URL,@ThumbNailURL,@Type,@Status,getdate(),@OwnerID)


END
GO
/****** Object:  StoredProcedure [dbo].[DeleteUserPreferences]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 

CREATE PROCEDURE [dbo].[DeleteUserPreferences] 
	@UserID [int] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
    Delete from UserPreference where UserId = @UserID
   
END


GO
/****** Object:  StoredProcedure [dbo].[DeleteVideo]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[DeleteVideo] 
	@VideoID [int],
	@UserID [int]
AS
BEGIN
	
	SET NOCOUNT ON;
	
    declare @type int 
	select @type = [type] from [user] where Id = @UserID

	if(@type = 1)
	begin
     update video set [Status] = 4, UpdatedBy = @UserID  where [ID] = @VideoID 
    end
	else
	begin
	 update video set [Status] = 4, UpdatedBy = @UserID  where [ID] = @VideoID and OwnerID = @UserID
	end
END


GO
/****** Object:  StoredProcedure [dbo].[ForgotPassword]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ForgotPassword] 
	@Email [varchar](100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select [UserName], [Password] from [User] where [Email] = @Email
END
GO
/****** Object:  StoredProcedure [dbo].[GetAllVideos]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetAllVideos] 
	@UserID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    -- todo: optimize per user settings and context
    select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, 
    v.averagerating, v.url, v.thumbnailurl, v.[type]
    from SubCategory c, Video v where v.[status] = 1 and c.[Status] = 1 and c.id = v.SubCategoryID and c.id <> 287 -- ikkos own content
    order by v.[ID]
    
END
GO
/****** Object:  StoredProcedure [dbo].[GetAllVideosBetweenRangeofIDS]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetAllVideosBetweenRangeofIDS] 
    @VideoID1 int,
	@VideoID2 int
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    -- todo: optimize per user settings and context
    select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, 
    v.averagerating, v.url, v.thumbnailurl, v.[type]
    from SubCategory c, Video v  
	where v.Id >=@VideoID1 and v.Id <=@VideoID2 and c.id = v.SubCategoryID
    order by v.[ID]
    
END

GO
/****** Object:  StoredProcedure [dbo].[GetAllVideosBySearchText]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
Create PROCEDURE [dbo].[GetAllVideosBySearchText]  
	@SearchText varchar(100)

AS
BEGIN
 
	SET NOCOUNT ON;	

 BEGIN tran
	
declare @MaxVar varchar(max)

set  @MaxVar =  ' select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, 
    v.averagerating, v.url, v.thumbnailurl, v.[type]
    from SubCategory c, Video v where v.[status] = 1 and c.[Status] = 1 and c.id = v.SubCategoryID and c.id <> 287  '

if(@SearchText != '')
set @MaxVar = @MaxVar + ' and v.movementname LIKE ''%'+ @SearchText +'%'' or v.actorname LIKE ''%'+ @SearchText +'%'''
 
set @MaxVar = @MaxVar +' order by v.[ID] desc'
 
 --print @MaxVar
exec (@MaxVar)

commit
 
END
GO
/****** Object:  StoredProcedure [dbo].[GetAllVideosSearch]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
CREATE PROCEDURE [dbo].[GetAllVideosSearch]  
	@UserID [int],
	@Segment  varchar(50) ,
	@Category  varchar(50) ,
	@SubCategory  varchar(50),
	@SearchText varchar(100),
	@Offset [int],
	@max [int]

AS
BEGIN
 
	SET NOCOUNT ON;	

 BEGIN tran
	
declare @MaxVar varchar(max)

set  @MaxVar =  ' select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, 
    v.averagerating, v.url, v.thumbnailurl, v.[type]
    from SubCategory c, Video v where v.[status] = 1 and c.[Status] = 1 and c.id = v.SubCategoryID and c.id <> 287  '

if(@Segment != 'ALL')
set  @MaxVar = @MaxVar + ' and c.Segment LIKE ''%'+ @Segment +'%'''

if(@Category != 'ALL')
set @MaxVar = @MaxVar +' and c.Category LIKE  ''%' + @Category +'%'''

if(@SubCategory != 'ALL')
set @MaxVar = @MaxVar +' and c.Name LIKE ''%' + @SubCategory +'%'''

if(@SearchText != '')
set @MaxVar = @MaxVar + ' and v.movementname LIKE ''%'+ @SearchText +'%'' or v.actorname LIKE ''%'+ @SearchText +'%'''

set @MaxVar = @MaxVar +' order by v.[ID] desc'
set @MaxVar = @MaxVar +' OFFSET '+  cast(@Offset as varchar(10))  + ' ROWS FETCH NEXT '+ cast(@max as varchar(10)) +' ROWS ONLY' 
exec (@MaxVar)

commit
 
END
GO
/****** Object:  StoredProcedure [dbo].[GetBlobThumbnailUriByVideoId]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [dbo].[GetBlobThumbnailUriByVideoId] 
@VideoID int
AS
BEGIN
 
	SET NOCOUNT ON;
 
  select ThumbNailURL from video where Id=@VideoID
    
END
GO
/****** Object:  StoredProcedure [dbo].[GetCatalog]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCatalog] 
	@LastID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select [ID], Segment, Category, Name as SubCategory from SubCategory where ID > @LastID and status = 1;
END
GO
/****** Object:  StoredProcedure [dbo].[GetCategories]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCategories] 
	@Segment [varchar](50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select distinct Category from SubCategory where Segment = @Segment and status = 1 order by Category;
END
GO
/****** Object:  StoredProcedure [dbo].[GetFeaturedPromotions]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFeaturedPromotions] 
	@Date [datetime],
	@UserID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	SELECT id, title, name, [Description], thumbnailurl, url, [Type] from Promotion where startdate <= @Date and enddate >= @Date and [status] = 1
END
GO
/****** Object:  StoredProcedure [dbo].[GetFeaturedVideo]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFeaturedVideo] 
	@Date [datetime],
	@UserID [int],
	@CategoryID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	DECLARE @ID int = 0;
	SELECT @ID = [video] from featuredvideo where startdate <= @date and enddate >= @date and [status] = 1
    
	if (@ID <> 0)
	BEGIN
		select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl
		from SubCategory c, Video v where v.[ID] = @ID and v.[status] = 1 and c.[Status] = 1 and c.id = v.SubCategoryID		
    END
END
GO
/****** Object:  StoredProcedure [dbo].[GetFlagReasons]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetFlagReasons]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    -- todo: optimize per user settings and context
   select * from FlagReasons
    
END

GO
/****** Object:  StoredProcedure [dbo].[GetOrganizations]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetOrganizations] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select [ID], Name from Organization where status = 1;
END
GO
/****** Object:  StoredProcedure [dbo].[GetPendingVideo]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[GetPendingVideo] 
	@VideoID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl
    from SubCategory c, Video v where v.[ID] = @VideoID and v.status = 7 and c.[Status] = 1 and c.id = v.SubCategoryID
    
END


GO
/****** Object:  StoredProcedure [dbo].[GetPromotions]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPromotions] 
	@Date [datetime],
	@UserID [int],
	@Count [int],
	@CategoryID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	SELECT top 10 id, title, name, [Description], thumbnailurl, url, [Type] from Promotion 
	where startdate <= @Date and enddate >= @Date and [status] = 1
	order by id desc
END
GO
/****** Object:  StoredProcedure [dbo].[GetSegments]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSegments] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select distinct Segment from SubCategory where status = 1 order by Segment;
END
GO
/****** Object:  StoredProcedure [dbo].[GetSubCategories]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSubCategories] 
	@Category [varchar](50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select distinct Name as SubCategory from SubCategory where Category = @Category and status = 1 order by Name;
END
GO
/****** Object:  StoredProcedure [dbo].[GetTypes]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTypes] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select * from Type where ID > 0 
END
GO
/****** Object:  StoredProcedure [dbo].[GetUserInfo]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[GetUserInfo]  
 
@UserName [varchar](100) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	
	 Select	ID,FirstName,LastName,Email,PhoneNumber,OrganizationID,Country,City,Address1,Address2,Age,Gender,[State],[Type],Zip from 
	 [User] where email=@UserName
	
END


GO
/****** Object:  StoredProcedure [dbo].[GetUserRoleByEmail]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUserRoleByEmail]
	@email [varchar](100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select [type] from [user] where email=@email
END
GO
/****** Object:  StoredProcedure [dbo].[GetUserVideos]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetUserVideos] 
	@UserID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl,v.[type],v.OwnerID
    from SubCategory c, Video v where v.OwnerID = @UserID and v.[status] =1 and c.id = v.SubCategoryID
    
END



GO
/****** Object:  StoredProcedure [dbo].[GetVideo]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetVideo] 
	@VideoID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl
    from SubCategory c, Video v where v.[ID] = @VideoID and v.[status] = 1 and c.[Status] = 1 and c.id = v.SubCategoryID
    
END
GO
/****** Object:  StoredProcedure [dbo].[GetVideoPath]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[GetVideoPath] 
@videoid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
     select url from video where id = @videoid
END
GO
/****** Object:  StoredProcedure [dbo].[GetVideoSubCategories]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetVideoSubCategories] 
	--@LastID [int]
AS
BEGIN
	 
	SET NOCOUNT ON;
    
	--select c.[ID], Segment, Category, Name as SubCategory, COUNT(v.ID) as videoCount from SubCategory c inner join Video v on c.id = v.subcategoryId and c.Status = 1 and c.ID<>287
	--order by Segment,Category,Name

	 select Segment, Category, Name as SubCategory, COUNT(v.ID) as videoCount
	from SubCategory c left join Video v on c.id = v.subcategoryId and c.Status = 1  and c.ID<>287
	Group by  c.id,Segment,Category,Name
	order by Segment,Category,Name

END
GO
/****** Object:  StoredProcedure [dbo].[GetVideosWithStatus]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetVideosWithStatus] 
	@UserID [int],
	@Status [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    -- todo: optimize per user settings and context
    select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl,v.ownerId
    from SubCategory c, Video v where v.[status] = @Status and c.[Status] = 1 and c.id = v.SubCategoryID
    order by v.[ID]
    
END

GO
/****** Object:  StoredProcedure [dbo].[GetVideoURL]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetVideoURL] 
	@VideoID int
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select URL from Video where ID=@VideoID
END
GO
/****** Object:  StoredProcedure [dbo].[IsValidUser]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [dbo].[IsValidUser]
@emailId varchar(100)
AS
BEGIN
 
	SET NOCOUNT ON;
 
 select ID from [user] where email = @emailId
END
GO
/****** Object:  StoredProcedure [dbo].[LoginUser]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[LoginUser] 
	@UserName [varchar](100),
	@Password [varchar](256)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	SELECT [ID], FirstName, LastName, Email, OrganizationID, Country, Age, Gender, [Type]
	from [User] where [Email] = @UserName and [Password] = @Password;
END

GO
/****** Object:  StoredProcedure [dbo].[RecommendedVideos]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[RecommendedVideos] 
 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	
	 Select top 5 ID,MovementName,OwnerID,Quality,Size,Speed,[Status],SubCategoryID,ThumbNailURL,URL,[Type],[Format],ActorName,ShardID from Video where Status=1 order by ID desc
	
END
GO
/****** Object:  StoredProcedure [dbo].[RegisterDevice]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RegisterDevice] 
	@PhoneID [varchar](50),
	@Type [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

   	DECLARE @ID int = 0;
	select @ID = [ID] from [Device] where [PhoneID] = @PhoneID
	
	if (@ID = 0) 
	BEGIN
		Insert into [Device] ([PhoneID], [Type], [Status], [CreatedBy],	[CreateDate]) 
			values (@PhoneID, @Type, 1, 0, GETUTCDATE())
		
		select @ID = @@IDENTITY
	END
	
	Select top 1 AppID, @ID as ID from AppID where Status = 1 order by ID desc
END
GO
/****** Object:  StoredProcedure [dbo].[RegisterUser]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RegisterUser] 
	@ShardID [int],
	@UserName [varchar](100),
	@FirstName [varchar](50),
	@LastName [varchar](50),
	@Email [varchar](100),
	@Password [varchar](256),
	@PhoneNumber [varchar](50),
	@PhoneID [int],
	@OrgID [int],
	@Address1 [varchar](100),
	@Address2 [varchar](100),
	@City [varchar](50),
	@State [varchar](50),
	@Country [varchar](50),
	@Zip [varchar](50),
	@Age [int],
	@Gender [int],
	@Type [int],
	@CreatedBy [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @ID int = 0;
	SELECT @ID = ID from [User] where [UserName] = @UserName and [Email] = @Email 
	
    -- Insert statements for procedure here
	if (@ID = 0)
	BEGIN
		Insert into [User] ([ShardID] ,	[UserName] ,[FirstName],[LastName] ,[Email] ,[Password] ,[PhoneNumber] ,[PhoneID] ,[OrganizationID] ,
		[Address1] ,[Address2] ,[City] ,[State] ,[Zip] ,[Country] ,[Age] ,[Gender] ,[Type] ,[Status] ,[CreatedBy] ,[CreateDate] ) 
		values (@ShardID, @UserName ,@FirstName ,@LastName ,@Email ,@Password ,@PhoneNumber ,@PhoneID ,	@OrgID ,@Address1 ,	@Address2 ,	@City ,
		@State,	@Zip ,	@Country ,	@Age ,	@Gender ,@Type, 1, @CreatedBy, GETUTCDATE())
		select @ID = @@IDENTITY
	END
	
	select @ID as ID
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateUser]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[UpdateUser] 
	@UserID [int],
	@FirstName [varchar](50),
	@LastName [varchar](50),
	@Email [varchar](100), 
	@PhoneNumber [varchar](50),
	@PhoneID [int],
	@OrgID [int],
	@Address1 [varchar](100),
	@Address2 [varchar](100),
	@City [varchar](50),
	@State [varchar](50),
	@Country [varchar](50),
	@Zip [varchar](50),
	@Age [int],
	@Gender [int],
	@Type [int],
	@UpdatedBy [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    update [User] set 
	[FirstName] = @FirstName,
	[LastName] = @LastName,
	[Email] = @Email,
	[PhoneNumber] = @PhoneNumber,
	[PhoneID] = @PhoneID,
	[OrganizationID] = @OrgID,
	[Address1] = @Address1,
	[Address2] = @Address2,
	[City] =  @City,
	[State] = @State,
	[Zip] = @Zip,
	[Country] =  @Country,
	[Age] = @Age,
	[Gender] = @Gender,
	[UpdatedBy] = @UpdatedBy,
	[UpdateDate] = GETUTCDATE() 
	where  [ID] = @UserID

	-- todo: return failure
END

GO
/****** Object:  StoredProcedure [dbo].[UpdateVideo]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROCEDURE [dbo].[UpdateVideo]
@VideoID [int],
	@UserID [int],
	@Segment [varchar](50),
	@Category [varchar](50),
	@SubCategory [varchar](50),
	@MovementName [varchar](50),
	@ActorName [varchar](50),
	@Orientation [int],
	@Facing [int],
	@VideoURL [varchar](1024),
	@ThumbNailURL [varchar](1024),@Format [varchar](50)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
   	DECLARE @catID int = 0;
	SELECT @catID = [ID] from SubCategory where Segment = @Segment and Category = @Category and [Name] = @SubCategory and [status] = 1
    
	if (@catID <> 0)
	BEGIN
		update Video set SubCategoryID = @catID, MovementName = @MovementName, ActorName = @ActorName, Orientation = @Orientation, 
		Facing = @Facing, URL = @VideoURL, ThumbNailURL = @ThumbNailURL, UpdatedBy = @UserID, UpdateDate = GETUTCDATE() ,[Format]=@Format
		where ID=@VideoID
    END
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateVideo1]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateVideo1] 
	@ID [int],
	@ShardID [int],
	@SubCategoryID [int],
	@MovementName [varchar](50),
	@ActorName [varchar](50),
	@Orientation [int],
	@Facing [int],
	@URL [varchar](1024),
	@Size [int],
	@Duration [int],
	@OwnerID [int],
	@Quality [varchar](200),
	@Speed [varchar](100),
	@Format [varchar](100),
	@Type [int],
	@ThumbNailURL [varchar](1024),
	@Status [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
   	 
	update Video set ShardId=@ShardID,SubcategoryId=@SubCategoryID,MovementName=@MovementName,ActorName=@ActorName,OwnerId=@OwnerID,[Format]=@Format,Speed=@Speed,Quality=@Quality,Size=@Size,Duration=@Duration,Orientation=@Orientation,Facing=@Facing,URL=@URL,ThumbNailURL=@ThumbNailURL,[Type]=@Type,[Status]=@Status,UpdateDate=getDate() where ID=@ID

END
GO
/****** Object:  StoredProcedure [dbo].[UpdateVideoThumbURLs]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE PROCEDURE [dbo].[UpdateVideoThumbURLs]   
    @VideoID [int],
	@UserID [int], 
	@VideoURL [varchar](1024),
	@ThumbNailURL [varchar](1024)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	 
	If(@ThumbNailURL IS NULL or @ThumbNailURL = '')
	BEGIN
	 
	update Video set URL = @VideoURL,  UpdatedBy = @UserID, UpdateDate = GETUTCDATE() 
		where ID=@VideoID
	
    END
 	If(@VideoURL IS NULL or @VideoURL = '')
	BEGIN
	 
	update Video set ThumbNailURL = @ThumbNailURL,  UpdatedBy = @UserID, UpdateDate = GETUTCDATE() 
		where ID=@VideoID
	
    END
	ELSE
	BEGIN
		update Video set URL = @VideoURL, ThumbNailURL = @ThumbNailURL, UpdatedBy = @UserID, UpdateDate = GETUTCDATE() 
		where ID=@VideoID
	END
END


GO
/****** Object:  StoredProcedure [dbo].[UpdateVideoURLs]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateVideoURLs]   --207,'https://ikkosvideos.blob.core.windows.net/videosstore/90420120855904.mp4','https://ikkosvideos.blob.core.windows.net/imagesstore/30759714513804.png'
	@VideoID [int],
	@VideoURL [varchar](1024),
	@ThumbNailURL [varchar](1024)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	

	if(@ThumbNailURL = '' and @VideoURL != '')
	Begin 
	   update video set [URL] = @VideoURL , [Status] = 7 where ID = @VideoID
	End
	Else
	 if(@ThumbNailURL !='' and @VideoURL = '')
	Begin 
	   update video set ThumbNailURL = @ThumbNailURL, [Status] = 7 where ID = @VideoID
	End
	else
	if(@ThumbNailURL !='' and @VideoURL != '')
	Begin
	update video set [URL] = @VideoURL, ThumbNailURL = @ThumbNailURL, [Status] = 7 where ID = @VideoID
    End
END

GO
/****** Object:  StoredProcedure [dbo].[UsersUsageTracking]    Script Date: 05-01-2015 14:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
Create PROCEDURE  [dbo].[UsersUsageTracking]
	@FeatureID int ,	
	@Count int,
	@Value int,
	@User int
	  
AS
 	 BEGIN
	SET NOCOUNT ON;
	 
	Insert into UsageTracking(FeatureID,[Count],Value,[User],[status],CreatedBy,CreateDate)Values(@FeatureID,@Count,@Value,@User,1,@User,Getutcdate())
    END
 

 
 


GO
