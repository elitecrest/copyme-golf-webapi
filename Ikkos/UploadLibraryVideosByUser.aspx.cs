﻿using Ikkos.Models;
using Ikkos.Models.DAL;
using Microsoft.WindowsAzure.MediaServices.Client;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ikkos
{
    public partial class UploadLibraryVideosByUser : System.Web.UI.Page
    {
        Utility.CustomResponse response = new Utility.CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                response = Handlefile();
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string jsonString = javaScriptSerializer.Serialize(response);
                Response.Write(jsonString);
            }

        }
        public static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        private Utility.CustomResponse Handlefile()
        {
            try
            {
                string baseUrl = ConfigurationManager.AppSettings["BaseURL"];
                //string baseUrl = "http://localhost:53175/";
                Stream stream = Request.InputStream;


                string videoFormat = Convert.ToString(Request.QueryString["VideoFormat"]).Trim();
                string MomentName = Convert.ToString(Request.QueryString["MomentName"]).Trim();
                int Duration = Convert.ToInt32(Request.QueryString["Duration"]);

                RecordedVideoInfoDTO video = new RecordedVideoInfoDTO();
                string videoUrl = Streamaudio(stream, videoFormat);
                //   string videoUrl = "https://newchurchblob.blob.core.windows.net:443/asset-3712445d-1500-80c5-588f-f1e5d5446923/fn1487471924.mp4?sv=2012-02-12&sr=c&si=099211b3-0f49-4dba-badf-7a9681a4d438&sig=ceYsoC%2FhRvqze5n3PIy6VNNSKLrAn5Sk9y9nu3pl6QE%3D&se=2017-02-16T07%3A02%3A39Z";
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                video.VideoUrl = videoUrl;
                video.MovementName = MomentName;
                video.CreatedBy = 0;
                video.CreatedBy = Convert.ToInt32(Request.QueryString["UserId"].Trim());
                video.Status = Convert.ToInt32(Request.QueryString["Status"].Trim());


                video.Format = videoFormat;


                video.Duration = Duration;
                //video.ThumbnailUrl =  GetVideoThumbnails(video);
                video.ThumbnailUrl = string.Empty;

                // video.ThumbNailURL = "sam";

                int res = UploadVideoToDB(video);
                /*if (res.IsSuccessStatusCode)*/
                if (res > 0)
                {
                    response.Status = Utility.CustomResponseStatus.Successful;
                    response.Response = videoUrl;
                    response.Message = CustomConstants.Video_Added_Successfully;
                }
                else
                {
                    response.Status = Utility.CustomResponseStatus.UnSuccessful;
                    response.Response = null;
                    response.Message = CustomConstants.videoNotExist;
                }
            }
            catch (Exception ex)
            {
                var Message = ex.Message;

                return null;
            }
            return response;
        }

        private string GetVideoThumbnails(RecordedVideoInfoDTO videoResp)
        {
            try
            {
                RecordedVideoInfoDTO video = new RecordedVideoInfoDTO();

                var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
                int totDuration = Convert.ToInt32(videoResp.Duration);
                float startPoint = 0;
                float dur = (float)totDuration / 6;

                var frame = startPoint + (float)Math.Round(2.0, 2);
                string thumbnailJpeGpath = HttpContext.Current.Server.MapPath("~/videoThumb" + 1 + ".jpg");
                ffMpeg.GetVideoThumbnail(videoResp.VideoUrl, thumbnailJpeGpath, frame);
                byte[] bytes = System.IO.File.ReadAllBytes(thumbnailJpeGpath);
                string base64String = Convert.ToBase64String(bytes);
                string thumbnailUrl = "";
                //if (base64String == null || base64String == string.Empty)
                //{
                //    thumbnailUrl = string.Empty;

                //}
                //else
                //{
                thumbnailUrl = GetImageURL(base64String, ".jpg", "");
                //}
                startPoint = startPoint + (float)Math.Round(2.0, 2);
                return thumbnailUrl;
            }
            catch (Exception ex)
            {
                var Message = ex.Message;

                return null;
            }
        }
        public string Streamaudio(Stream stream, string format)
        {
            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);
            var fileStream = System.IO.File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();
            var ext = format;
            Stream fileStreambanners = stream;
            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string videoUrl = UploadImage1(fileNamebanners, path, ext, fileStreambanners);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            return videoUrl;

        }

        public string UploadImage1(string fileName, string path, string ext, Stream inputStream)
        {
            string account = ConfigurationManager.AppSettings["MediaServicesAccountName"];
            string key = ConfigurationManager.AppSettings["MediaServicesAccountKey"];
            CloudMediaContext context = new CloudMediaContext(account, key);


            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);
            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));
            assetFile.Upload(path);
            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 10000;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive), AccessPermissions.Read);
            string streamingUrl = string.Empty;
            var assetFiles = streamingAsset.AssetFiles.ToList();
            var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".mp4")).FirstOrDefault();
            if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            {
                var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                var mp4Uri = new UriBuilder(locator.Path);
                mp4Uri.Path += "/" + streamingAssetFile.Name;
                streamingUrl = mp4Uri.ToString();
            }
            return streamingUrl;
            //string streamingUrl = "";

            //string singleFilePath = path;
            //var tokenCredentials = new AzureAdTokenCredentials("rlamikkos.onmicrosoft.com",
            //new AzureAdClientSymmetricKey("fc4cbb9f-263b-4145-8b11-816e902dacf8", "pdJesXlyxKNR4HamI5JOhdO130WmypmnskpsN4RQySs="),
            //AzureEnvironments.AzureCloudEnvironment);

            //var tokenProvider = new AzureAdTokenProvider(tokenCredentials);

            //CloudMediaContext _context = new CloudMediaContext(new Uri("https://ikkosports.restv2.westus.media.azure.net/api/"), tokenProvider);


            //var assetName = Path.GetFileNameWithoutExtension(singleFilePath);
            //IAsset inputAsset = _context.Assets.Create(assetName, AssetCreationOptions.None);

            //var assetFile = inputAsset.AssetFiles.Create(Path.GetFileName(singleFilePath));

            //Console.WriteLine("Upload {0}", inputAsset.Uri);

            //assetFile.Upload(singleFilePath);

            //Console.WriteLine("Done uploading {0}", assetFile.Name);
            //var streamingAssetId = inputAsset.Id; // "YOUR ASSET ID";
            //var daysForWhichStreamingUrlIsActive = 10000;
            //var streamingAsset = _context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            //var accessPolicy = _context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive), AccessPermissions.Read);
            //var assetFiles = streamingAsset.AssetFiles.ToList();
            //var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".mp4")).FirstOrDefault();
            //if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            //{
            //    var locator = _context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
            //    var mp4Uri = new UriBuilder(locator.Path);
            //    mp4Uri.Path += "/" + streamingAssetFile.Name;
            //    streamingUrl = mp4Uri.ToString();
            //}
            //return streamingUrl;

        }

        public string GetImageURL(string binary, string format, string contentType)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();
                string filename = "" + r.Next(999999) + DateTime.Now.Millisecond + r.Next(99999) + format;
                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();
                container.SetPermissions(new BlobContainerPermissions
                {
                    PublicAccess =
                        BlobContainerPublicAccessType.Blob
                });
                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
                // Create or overwrite the "myblob" blob with contents from a local file.
                byte[] binarydata = Convert.FromBase64String(binary);
                blockBlob.Properties.ContentType = contentType;
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);
                //UploadFromStream(fileStream);
                //}
                // Retrieve reference to a blob named "myblob".
                CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);
                imageurl = blockBlob1.Uri.ToString();
            }
            catch (Exception ex)
            {

            }
            return imageurl;
        }

        internal static int UploadVideoToDB(RecordedVideoInfoDTO videoDTO)
        {
            int result = 0;
            try
            {

                SqlConnection myConn = UsersDAL.ConnectTODB();
                SqlCommand cmd = new SqlCommand("[AddRecordedVideoV2]", myConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter Format = cmd.Parameters.Add("@Format", SqlDbType.VarChar, 50);
                Format.Direction = ParameterDirection.Input;
                Format.Value = videoDTO.Format;

                SqlParameter movname = cmd.Parameters.Add("@MovementName", SqlDbType.VarChar, 50);
                movname.Direction = ParameterDirection.Input;
                movname.Value = videoDTO.MovementName;

                SqlParameter Duration = cmd.Parameters.Add("@Duration", SqlDbType.Float);
                Duration.Direction = ParameterDirection.Input;
                Duration.Value = videoDTO.Duration;

                SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                VideoURL.Direction = ParameterDirection.Input;
                VideoURL.Value = videoDTO.VideoUrl;

                SqlParameter ThumbnailURL = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1024);
                ThumbnailURL.Direction = ParameterDirection.Input;
                ThumbnailURL.Value = (videoDTO.ThumbnailUrl == null) ? string.Empty : videoDTO.ThumbnailUrl;

                SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                Status.Direction = ParameterDirection.Input;
                Status.Value = videoDTO.Status;

                SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", SqlDbType.Int);
                CreatedBy.Direction = ParameterDirection.Input;
                CreatedBy.Value = videoDTO.CreatedBy;



                SqlDataReader myData = cmd.ExecuteReader();

                while (myData.Read())
                {
                    result = (int)myData["ID"];
                }
            }
            catch (Exception e)
            {
                var Message = e.Message;
                // UsersModel.AddException(Message);
                throw;
            }
            return result;
        }
        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    response = Handlefile();
        //    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //    string jsonString = javaScriptSerializer.Serialize(response);
        //    Response.Write(jsonString);
        //}
    }
}