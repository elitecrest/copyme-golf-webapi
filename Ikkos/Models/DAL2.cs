﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using PushSharp.Core;
using System.IO;
using System.Net.Http;
using Ikkos.Models.DAL;
using Microsoft.WindowsAzure.Storage;

namespace Ikkos.Models
{
    public class DAL2
    {
        internal static int AdminAddVideo_V2(VideosInfoDTO videoDTO)
        {
            try
            {
                int vid = 0;
                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AdminAddVideo_V2]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", System.Data.SqlDbType.Int);
                    UserID.Direction = System.Data.ParameterDirection.Input;
                    UserID.Value = videoDTO.OwnerID;

                    SqlParameter Segment = cmd.Parameters.Add("@Segment", System.Data.SqlDbType.VarChar, 100);
                    Segment.Direction = System.Data.ParameterDirection.Input;
                    Segment.Value = videoDTO.Segment;

                    SqlParameter Category = cmd.Parameters.Add("@Category", System.Data.SqlDbType.VarChar, 100);
                    Category.Direction = System.Data.ParameterDirection.Input;
                    Category.Value = videoDTO.Category;

                    SqlParameter SubCategory = cmd.Parameters.Add("@SubCategory", System.Data.SqlDbType.VarChar, 100);
                    SubCategory.Direction = System.Data.ParameterDirection.Input;
                    SubCategory.Value = videoDTO.SubCategory;

                    SqlParameter MovementName = cmd.Parameters.Add("@MovementName", System.Data.SqlDbType.VarChar, 100);
                    MovementName.Direction = System.Data.ParameterDirection.Input;
                    MovementName.Value = videoDTO.MovementName;

                    SqlParameter ActorName = cmd.Parameters.Add("@ActorName", System.Data.SqlDbType.VarChar, 100);
                    ActorName.Direction = System.Data.ParameterDirection.Input;
                    ActorName.Value = videoDTO.ActorName;

                    SqlParameter Orientation = cmd.Parameters.Add("@Orientation", System.Data.SqlDbType.Int);
                    Orientation.Direction = System.Data.ParameterDirection.Input;
                    Orientation.Value = videoDTO.Orientation;

                    SqlParameter Facing = cmd.Parameters.Add("@Facing", System.Data.SqlDbType.Int);
                    Facing.Direction = System.Data.ParameterDirection.Input;
                    Facing.Value = videoDTO.Facing;

                    SqlParameter ThumbNailURL = cmd.Parameters.Add("@ThumbNailURL", System.Data.SqlDbType.VarChar, 100);
                    ThumbNailURL.Direction = System.Data.ParameterDirection.Input;
                    ThumbNailURL.Value = videoDTO.ThumbNailURL;

                    SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", System.Data.SqlDbType.VarChar, 100);
                    VideoURL.Direction = System.Data.ParameterDirection.Input;
                    VideoURL.Value = videoDTO.URL;

                    SqlParameter Format = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Format.Direction = System.Data.ParameterDirection.Input;
                    Format.Value = videoDTO.Status;

                    SqlParameter VideoMode = cmd.Parameters.Add("@VideoMode", System.Data.SqlDbType.Int);
                    VideoMode.Direction = System.Data.ParameterDirection.Input;
                    VideoMode.Value = videoDTO.VideoMode;

                    SqlParameter AudioMix = cmd.Parameters.Add("@AudioMix", System.Data.SqlDbType.Int);
                    AudioMix.Direction = System.Data.ParameterDirection.Input;
                    AudioMix.Value = videoDTO.AudioMix;

                    SqlParameter ProcessedVideoURL = cmd.Parameters.Add("@ProcessedVideoURL", System.Data.SqlDbType.VarChar, 1000);
                    ProcessedVideoURL.Direction = System.Data.ParameterDirection.Input;
                    ProcessedVideoURL.Value = (videoDTO.ProcessedVideoURL == null) ? string.Empty : videoDTO.ProcessedVideoURL;

                    SqlParameter ProcessedThumbNailURL = cmd.Parameters.Add("@ProcessedThumbNailURL", System.Data.SqlDbType.VarChar, 1000);
                    ProcessedThumbNailURL.Direction = System.Data.ParameterDirection.Input;
                    ProcessedThumbNailURL.Value = (videoDTO.ProcessedThumbNailURL == null) ? string.Empty : videoDTO.ProcessedThumbNailURL;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        vid = (int)myData["ID"];
                    }

                    myConn.Close();
                }

                return vid;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static void UpdateVideo_V2(VideosInfoDTO videoDTO)
        {
            try
            {
                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateVideo_V2]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter VideoID = cmd.Parameters.Add("@VideoID", System.Data.SqlDbType.Int);
                    VideoID.Direction = System.Data.ParameterDirection.Input;
                    VideoID.Value = videoDTO.ID;

                    SqlParameter Segment = cmd.Parameters.Add("@Segment", System.Data.SqlDbType.VarChar, 100);
                    Segment.Direction = System.Data.ParameterDirection.Input;
                    Segment.Value = videoDTO.Segment;

                    SqlParameter Category = cmd.Parameters.Add("@Category", System.Data.SqlDbType.VarChar, 100);
                    Category.Direction = System.Data.ParameterDirection.Input;
                    Category.Value = videoDTO.Category;

                    SqlParameter SubCategory = cmd.Parameters.Add("@SubCategory", System.Data.SqlDbType.VarChar, 100);
                    SubCategory.Direction = System.Data.ParameterDirection.Input;
                    SubCategory.Value = videoDTO.SubCategory;

                    SqlParameter ActorName = cmd.Parameters.Add("@ActorName", System.Data.SqlDbType.VarChar, 100);
                    ActorName.Direction = System.Data.ParameterDirection.Input;
                    ActorName.Value = videoDTO.ActorName;

                    SqlParameter MovementName = cmd.Parameters.Add("@MovementName", System.Data.SqlDbType.VarChar, 100);
                    MovementName.Direction = System.Data.ParameterDirection.Input;
                    MovementName.Value = videoDTO.MovementName;

                    SqlParameter Orientation = cmd.Parameters.Add("@Orientation", System.Data.SqlDbType.Int);
                    Orientation.Direction = System.Data.ParameterDirection.Input;
                    Orientation.Value = videoDTO.Orientation;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", System.Data.SqlDbType.Int);
                    UserID.Direction = System.Data.ParameterDirection.Input;
                    UserID.Value = videoDTO.OwnerID;

                    SqlParameter Facing = cmd.Parameters.Add("@Facing", System.Data.SqlDbType.VarChar, 100);
                    Facing.Direction = System.Data.ParameterDirection.Input;
                    Facing.Value = videoDTO.Facing;

                    SqlParameter Format = cmd.Parameters.Add("@Format", System.Data.SqlDbType.VarChar, 100);
                    Format.Direction = System.Data.ParameterDirection.Input;
                    Format.Value = videoDTO.Format.ToLower();

                    SqlParameter VideoMode = cmd.Parameters.Add("@VideoMode", System.Data.SqlDbType.Int);
                    VideoMode.Direction = System.Data.ParameterDirection.Input;
                    VideoMode.Value = videoDTO.VideoMode;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static void UpdateProcessedVideo_V2(ProcessedVideoDTO ProcessedVideo)
        {
            try
            {
                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateProcessedVideo_V2]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter VideoID = cmd.Parameters.Add("@VideoID", System.Data.SqlDbType.Int);
                    VideoID.Direction = System.Data.ParameterDirection.Input;
                    VideoID.Value = ProcessedVideo.VideoID;

                    SqlParameter ProcessedVideoURL = cmd.Parameters.Add("@ProcessedVideoURL", System.Data.SqlDbType.VarChar, 1020);
                    ProcessedVideoURL.Direction = System.Data.ParameterDirection.Input;
                    ProcessedVideoURL.Value = ProcessedVideo.ProcessedVideoURL;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static int  AddUserRecordedVideo(RecordedVideoInfoDTO videoDTO)
        {    
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddRecordedVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure; 

                    SqlParameter Format = cmd.Parameters.Add("@Format", SqlDbType.VarChar, 50);
                    Format.Direction = ParameterDirection.Input;
                    Format.Value = videoDTO.Format;

                    SqlParameter MovemName = cmd.Parameters.Add("@MovementName", SqlDbType.VarChar, 50);
                    MovemName.Direction = ParameterDirection.Input;
                    MovemName.Value = videoDTO.MovementName;

                    SqlParameter Duration = cmd.Parameters.Add("@Duration", SqlDbType.Float);
                    Duration.Direction = ParameterDirection.Input;
                    Duration.Value = videoDTO.Duration;

                    SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    VideoURL.Direction = ParameterDirection.Input;
                    VideoURL.Value = videoDTO.VideoUrl;

                    SqlParameter ThumbnailURL = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1024);
                    ThumbnailURL.Direction = ParameterDirection.Input;
                    ThumbnailURL.Value = videoDTO.ThumbnailUrl; 

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = videoDTO.Status;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", SqlDbType.Int);
                    CreatedBy.Direction = ParameterDirection.Input;
                    CreatedBy.Value = videoDTO.CreatedBy;

                    SqlParameter CreatedDate = cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime);
                    CreatedDate.Direction = ParameterDirection.Input;
                    CreatedDate.Value = videoDTO.CreatedDate;  

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["ID"];
                    }
                    myData.Close();
                    myConn.Close();

                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                throw;
            }

            return id;
        }


        internal static RecordedThumbnailDTO UpdateVideoThumbnail(RecordedThumbnailDTO thumbnailDTO)
        {
            RecordedThumbnailDTO v = new RecordedThumbnailDTO();
            try
            {
                string ThumbnailURL = UploadImageFromBase64(thumbnailDTO.ThumbnailURL, thumbnailDTO.ThumbnailFormat);

                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateRecordedVideoThumbnail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter MovName = cmd.Parameters.Add("@MovementName", SqlDbType.VarChar, 50);
                    MovName.Direction = ParameterDirection.Input;
                    MovName.Value = thumbnailDTO.MovementName; 

                    SqlParameter ActorName = cmd.Parameters.Add("@ActorName", SqlDbType.VarChar, 50);
                    ActorName.Direction = ParameterDirection.Input;
                    ActorName.Value = thumbnailDTO.ActorName;

                    SqlParameter Videorate = cmd.Parameters.Add("@VideoRate", SqlDbType.Int);
                    Videorate.Direction = ParameterDirection.Input;
                    Videorate.Value = thumbnailDTO.VideoRate;

                    SqlParameter Thumbnailurl = cmd.Parameters.Add("@Thumbnailurl", SqlDbType.VarChar, 1024);
                    Thumbnailurl.Direction = ParameterDirection.Input;
                    Thumbnailurl.Value = ThumbnailURL;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", SqlDbType.Int);
                    CreatedBy.Direction = ParameterDirection.Input;
                    CreatedBy.Value = thumbnailDTO.UserId;

                    SqlParameter VideoId = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    VideoId.Direction = ParameterDirection.Input;
                    VideoId.Value = thumbnailDTO.VideoId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.MovementName = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.ActorName = (myData["ActorName"] == DBNull.Value) ? "" : (string)myData["ActorName"].ToString();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value) ? "" : (string)myData["ThumbnailURL"].ToString();
                        v.Id = (int)myData["Id"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                throw;
            }

            return v;
        }


        internal static string UploadImageFromBase64(string binary, string format)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999).ToString() + DateTime.Now.Millisecond + "." + format;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydata = Convert.FromBase64String(binary);


                blockBlob.Properties.ContentType = "image/jpeg";
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);

                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                imageurl = blockBlob1.Uri.ToString();


            }
            catch (Exception ex)
            {
                string s = ex.Message;
                throw;
            }
            return imageurl;

        }

        internal static List<RecordedVideoFullDTO> GetRecordedVideosByUserId(int userID)
        {
            List<RecordedVideoFullDTO> videos = new List<RecordedVideoFullDTO>();

            try
            {
                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllRecordedVideosByUserId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    usrid.Direction = ParameterDirection.Input;
                    usrid.Value = userID;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        RecordedVideoFullDTO v = new RecordedVideoFullDTO();
                        v.ID = (int)myData["ID"];
                        v.MovementName = (myData["MovementName"] == DBNull.Value) ? "" : (string)myData["MovementName"];
                        v.ActorName = (myData["ActorName"] == DBNull.Value) ? "" : (string)myData["ActorName"];
                        v.Format = (string)myData["Format"];                     
                        v.URL = (string)myData["VideoURL"];
                        v.ThumbnailUrl = (myData["ThumbNailURL"] == DBNull.Value) ? "" : (string)myData["ThumbNailURL"];
                        v.VideoRate = (myData["VideoRate"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["VideoRate"]);
                        v.Status = (int)myData["Status"];
                        v.CreatedBy = (int)myData["CreatedBy"];
                        v.CreatedDate = (DateTime)myData["CreateDate"];
                        v.Duration = (double)myData["Duration"];
                        //v.UpdatedBy = (myData["UpdatedBy"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["UpdatedBy"]);

                                     
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videos;
        }

        internal static int DeleteRecordedVideo(int videoId, int userid)
        {
           
            int DeletedId = 0;
            try
            {
                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteRecordedVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter videoIdparam = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    videoIdparam.Direction = ParameterDirection.Input;
                    videoIdparam.Value = videoId;

                    SqlParameter Useridparam = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    Useridparam.Direction = ParameterDirection.Input;
                    Useridparam.Value = userid;

                    SqlDataReader myData = cmd.ExecuteReader();
                   
                    while (myData.Read())
                    {
                        DeletedId = (int)myData["result"];
                    }                  
                    myData.Close();
                    myConn.Close();
                }
                return DeletedId;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

        }


        internal static void UpdatePaymentExpiryDate(string UserID, DateTime ExpiryDate)
        {
            try
            {
                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdatePaymentExpiryDate]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter UserIDPar = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserIDPar.Direction = ParameterDirection.Input;
                    UserIDPar.Value = UserID;

                    SqlParameter expiryDate = cmd.Parameters.Add("@ExpiryDate", System.Data.SqlDbType.DateTime);
                    expiryDate.Direction = System.Data.ParameterDirection.Input;
                    expiryDate.Value = ExpiryDate;


                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static int AddPayment(PaymentDTO payment)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddPayment]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = payment.UserID;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.Int);
                    UserType.Direction = ParameterDirection.Input;
                    UserType.Value = payment.UserType;

                    SqlParameter PaymentToken = cmd.Parameters.Add("@PaymentToken", SqlDbType.VarChar, 8000);
                    PaymentToken.Direction = ParameterDirection.Input;
                    PaymentToken.Value = payment.PaymentToken;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = payment.DeviceType;

                    SqlParameter PaymentType = cmd.Parameters.Add("@PaymentType", SqlDbType.VarChar, 50);
                    PaymentType.Direction = ParameterDirection.Input;
                    PaymentType.Value = payment.PaymentType;

                    SqlParameter Skuid = cmd.Parameters.Add("@SKUId", SqlDbType.VarChar, 100);
                    Skuid.Direction = ParameterDirection.Input;
                    Skuid.Value = (payment.Skuid == null) ? string.Empty : payment.Skuid;

                    SqlParameter subscriptionType = cmd.Parameters.Add("@SubscriptionType", System.Data.SqlDbType.VarChar, 20);
                    subscriptionType.Direction = System.Data.ParameterDirection.Input;
                    subscriptionType.Value = (payment.SubscriptionType == null) ? string.Empty : payment.SubscriptionType;



                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return id;
        }


        internal static PaymentDTO GetPayment(int UserID)
        {
            PaymentDTO payment = new PaymentDTO();

            try
            {
                SqlConnection myConn =  UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPayment]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter userID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    userID.Direction = ParameterDirection.Input;
                    userID.Value = UserID;  

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        payment.UserID = (int)myData["UserID"];
                        payment.UserType = (int)myData["UserType"];
                        payment.PaymentToken = (string)myData["PaymentToken"];
                        payment.DeviceType = (string)myData["DeviceType"];
                        payment.PaymentType = (string)myData["PaymentType"];
                        payment.CreatedDate = (DateTime)myData["CreatedDate"];
                        payment.SubscriptionType = (myData["SubscriptionType"] == DBNull.Value) ? string.Empty : (string)myData["SubscriptionType"];
                        payment.ExpiryDate = (myData["ExpiryDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["ExpiryDate"];
                        payment.Skuid = (myData["Skuid"] == DBNull.Value) ? string.Empty : (string)myData["Skuid"];
                        payment.Link = (string)myData["Link"];
                        payment.Id = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return payment;
        }

        internal static PromotionVideoDTO GetPromotionVideo()
        {
            PromotionVideoDTO v = new PromotionVideoDTO();

            try
            {
                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPromotionVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]);
                        v.Title = myData["Title"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Title"]);
                        v.Name = myData["Name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Name"]);
                        v.Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]);
                        v.VideoUrl = myData["URL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["URL"]);
                        v.Thumbnail = myData["ThumbNailURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ThumbNailURL"]);
                        v.Status = myData["Status"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Status"]);
                        v.CreatedBy = myData["CreatedBy"] == DBNull.Value ? 0 : Convert.ToInt32(myData["CreatedBy"]);
                        v.CreatedDate = myData["CreateDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreateDate"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

    }
}