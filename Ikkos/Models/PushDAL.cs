﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using PushSharp.Core;
using System.IO;


namespace Ikkos.Models
{
    public class PushDAL
    {
        internal static void DeviceSubscriptionChanged(object sender,
           string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            //Do something here
        }

        //this even raised when a notification is successfully sent
        internal static void NotificationSent(object sender, INotification notification)
        {
            //Do something here
        }

        //this is raised when a notification is failed due to some reason
        internal static void NotificationFailed(object sender,
        INotification notification, Exception notificationFailureException)
        {
            //Do something here
        }

        //this is fired when there is exception is raised by the channel
         static void ChannelException
            (object sender, IPushChannel channel, Exception exception)
        {
            //Do something here
        }

        //this is fired when there is exception is raised by the service
        internal static void ServiceException(object sender, Exception exception)
        {
            //Do something here
        }

        //this is raised when the particular device subscription is expired
        internal static void DeviceSubscriptionExpired(object sender,
        string expiredDeviceSubscriptionId,
            DateTime timestamp, INotification notification)
        {
            //Do something here
        }

        //this is raised when the channel is destroyed
        internal static void ChannelDestroyed(object sender)
        {
            //Do something here
        }

        //this is raised when the channel is created
        internal static void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            //Do something here
            
        }

        internal static void PushNotifications(string message, int badge)
        {
            //create the puchbroker object
            var push = new PushBroker();
            //Wire up the events for all the services that the broker registers
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push.OnChannelCreated += ChannelCreated;
            push.OnChannelDestroyed += ChannelDestroyed;

            try
            {


                var path = HttpContext.Current.Server.MapPath("~/iKKOSAdminPushCertificates.p12");

                var appleCert = File.ReadAllBytes(path);
                //IMPORTANT: If you are using a Development provisioning Profile, you must use
                // the Sandbox push notification server 
                //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as
                // 'false')
                //  If you are using an AdHoc or AppStore provisioning profile, you must use the 
                //Production push notification server
                //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 
                //'true')
                push.RegisterAppleService(new ApplePushChannelSettings(true, appleCert, "123456Aa"));
                //Extension method
                //Fluent construction of an iOS notification
                //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets
                // generated within your iOS app itself when the Application Delegate
                //  for registered for remote notifications is called, 
                // and the device token is passed back to you
                push.QueueNotification(new AppleNotification()
                                            .ForDeviceToken("546e9a4b9a3a135f8fad07d168523ea3f99189dd79d96682fa260d3d8e9192f3")//the recipient device id
                                            .WithAlert(message)//the message
                                            .WithBadge(badge)
                                            .WithSound("sound.caf")
                                            );

                push.StopAllServices(waitForQueuesToFinish: true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}