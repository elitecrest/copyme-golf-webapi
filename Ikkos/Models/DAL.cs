﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;



namespace Ikkos.Models.DAL
{
    internal static class UsersDAL
    {
        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CopyMe"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }
        //14NOV
        internal static List<UserTypes_DTO> GetUserTypes()
        {
            //using (var DBContext = new IkkosEntities())
            //{
            //    return (from type in DBContext.Types

            //            where type.ID > 0
            //            select new UserTypes_DTO
            //            {
            //                ID = type.ID,
            //                Description = type.Description
            //            }).ToList();
            //}

            List<UserTypes_DTO> Types = new List<UserTypes_DTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetTypes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        UserTypes_DTO v = new UserTypes_DTO();

                        v.ID = (int)myData["ID"];
                        v.Description = (string)myData["Description"];


                        Types.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Types;
        }


        internal static bool ValidateApp(string appid)
        {
            // todo: validate the appid and device id, for non device alls, check something.
            return true;
        }

        internal static UserInfoDTO Login(string username, string password, bool isAdmin = false)
        {
            UserInfoDTO userinfo = new UserInfoDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[LoginUser]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrname = cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 100);
                    usrname.Direction = ParameterDirection.Input;
                    usrname.Value = username;

                    SqlParameter pwd = cmd.Parameters.Add("@Password", SqlDbType.VarChar, 256);
                    pwd.Direction = ParameterDirection.Input;
                    pwd.Value = password;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        userinfo.ID = (myData["ID"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["ID"]);
                        userinfo.FirstName = (myData["FirstName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["FirstName"]);
                        userinfo.LastName = (string)myData["LastName"];
                        userinfo.Email = (string)myData["Email"];
                        userinfo.OrganizationID = (int)myData["OrganizationID"];
                        userinfo.Country = (string)myData["Country"];
                        userinfo.Age = (int)myData["Age"];
                        userinfo.Gender = (int)myData["Gender"];
                        userinfo.UserSports = "Golf,Fitness";
                        userinfo.Type = (int)myData["Type"];
                        userinfo.Month = (myData["month"] == DBNull.Value) ? 0 : (int)myData["month"];
                        userinfo.Year = (myData["year"] == DBNull.Value) ? 0 : (int)myData["year"];
                        userinfo.FacebookId = (myData["FacebookId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["FacebookId"]);
                        userinfo.ProfilePicUrl = (myData["ProfilePicURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ProfilePicURL"]);
                        userinfo.Organization = (string)myData["Name"];  //by lohit
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return userinfo;
        }

        internal static AppInfoDTO RegisterDevice(string deviceid, int type)
        {
            AppInfoDTO appinfo = new AppInfoDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[RegisterDevice]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter phid = cmd.Parameters.Add("@PhoneID", SqlDbType.VarChar, 50);
                    phid.Direction = ParameterDirection.Input;
                    phid.Value = deviceid;

                    SqlParameter dtype = cmd.Parameters.Add("@Type", SqlDbType.Int);
                    dtype.Direction = ParameterDirection.Input;
                    dtype.Value = type;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        int i = (int)myData["ID"];
                        appinfo.AppID = (string)myData["AppID"] + "_" + i.ToString();
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return appinfo;
        }

        //<summary> Used to get subcategories data from db </summary>
        internal static List<CategoryDTO> GetCatalog(int lastid)
        {
            List<CategoryDTO> cat = new List<CategoryDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCatalog]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@LastID", SqlDbType.Int);
                    usrid.Direction = ParameterDirection.Input;
                    usrid.Value = lastid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CategoryDTO v = new CategoryDTO();

                        v.CategoryID = (int)myData["ID"];
                        v.Segment = (string)myData["Segment"];
                        v.Category = (string)myData["Category"];
                        v.SubCategory = (string)myData["SubCategory"];

                        cat.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return cat;
        }

        //Added By Bindu

        //<summary> Used to get videos data depending on the search criteria from db </summary>
        internal static List<VideosInfoDTO> GetAllVideosSearch(int userId, string segment, string category, string subCategory, int offset, int max, string searchText, bool isSubscribed)
        {
            List<VideosInfoDTO> videoInfo = new List<VideosInfoDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllVideosSearch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    usrid.Direction = ParameterDirection.Input;
                    usrid.Value = userId;

                    SqlParameter Segment = cmd.Parameters.Add("@Segment", SqlDbType.VarChar, 50);
                    Segment.Direction = ParameterDirection.Input;
                    Segment.Value = segment;

                    SqlParameter Category = cmd.Parameters.Add("@Category", SqlDbType.VarChar, 50);
                    Category.Direction = ParameterDirection.Input;
                    Category.Value = category;

                    SqlParameter SubCategory = cmd.Parameters.Add("@SubCategory", SqlDbType.VarChar, 50);
                    SubCategory.Direction = ParameterDirection.Input;
                    SubCategory.Value = subCategory;

                    SqlParameter Offset = cmd.Parameters.Add("@Offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;

                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlParameter SearchText = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 100);
                    SearchText.Direction = ParameterDirection.Input;
                    SearchText.Value = (searchText == null) ? string.Empty : searchText;

                    SqlParameter IsSubscribed = cmd.Parameters.Add("@isSubscribed", SqlDbType.Bit);
                    IsSubscribed.Direction = ParameterDirection.Input;
                    IsSubscribed.Value = isSubscribed;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideosInfoDTO v = new VideosInfoDTO();

                        v.ID = (int)myData["ID"];
                        v.Segment = (string)myData["segment"];
                        v.Category = (string)myData["category"];
                        v.SubCategory = (string)myData["subcategory"];
                        v.MovementName = (string)myData["movementname"];
                        v.ActorName = (string)myData["actorname"];
                        v.Orientation = (int)myData["orientation"];
                        v.Facing = (int)myData["facing"];
                        int rate = (int)myData["averagerating"];
                        v.Rating = (float)(rate / 20.0);
                        v.URL = (string)myData["url"];
                        v.ThumbNailURL = (string)myData["thumbnailurl"];
                        v.Type = (int)myData["type"];
                        v.PlayCount = (int)myData["PlayCount"];
                        v.VideoMode = (myData["VideoMode"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["VideoMode"]);
                        v.AudioMix = (myData["AudioMix"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AudioMix"]);
                        v.ProcessedVideoURL = (myData["ProcessedVideoURL"] == DBNull.Value) ? "" : (string)myData["ProcessedVideoURL"];
                        v.ProcessedThumbNailURL = (myData["ProcessedThumbNailURL"] == DBNull.Value) ? "" : (string)myData["ProcessedThumbNailURL"];    //by lohitK
                        videoInfo.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videoInfo;
        }

        //<summary> Used to get subcategories, segments, category those have videos from db </summary>
        internal static List<CategoryDTO> GetVideoSubCategories()
        {
            List<CategoryDTO> cat = new List<CategoryDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVideoSubCategories]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    //SqlParameter usrid = cmd.Parameters.Add("@LastID", SqlDbType.Int);
                    //usrid.Direction = ParameterDirection.Input;
                    //usrid.Value = lastid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CategoryDTO v = new CategoryDTO();

                        //v.CategoryID = (int)myData["ID"];
                        v.Segment = (string)myData["Segment"];
                        v.Category = (string)myData["Category"];
                        v.SubCategory = (string)myData["SubCategory"];
                        v.VideoCount = (int)myData["videoCount"];
                        cat.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return cat;
        }
        //<summary> Used to get subcategories, segments, category those have videos from db Removing Duplicate Subcategories</summary>
        internal static List<CatalogDto> GetVideoCatalogList(int lastid)
        {
            List<CategoryDTO> cat = new List<CategoryDTO>();
            List<CatalogDto> catalogList = new List<CatalogDto>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVideoSubCategories]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@LastID", SqlDbType.Int);
                    usrid.Direction = ParameterDirection.Input;
                    usrid.Value = lastid;

                    SqlDataReader myData = cmd.ExecuteReader();



                    string prevCategory = null;

                    int cursor = 0;
                    CatalogDto catalogDto = null;
                    while (myData.Read())
                    {


                        string presCategory = (string)myData["Category"];

                        if (prevCategory == null || (!prevCategory.Equals(presCategory)))
                        {
                            prevCategory = presCategory;
                            catalogDto = new CatalogDto();
                            catalogDto.Segment = (string)myData["Segment"];
                            catalogDto.Category = (string)myData["Category"];
                            catalogDto.SubCategoriesList = new List<SubCategoryDTO>();
                            SubCategoryDTO dto = new SubCategoryDTO();
                            dto.Subcategory = (string)myData["SubCategory"];
                            catalogDto.SubCategoriesList.Add(dto);
                            catalogList.Add(catalogDto);
                        }
                        else
                        {
                            List<SubCategoryDTO> subCategoryList = catalogDto.SubCategoriesList;
                            SubCategoryDTO dto = new SubCategoryDTO();
                            dto.Subcategory = (string)myData["SubCategory"];

                            int length = subCategoryList.Count;
                            SubCategoryDTO lastDto = subCategoryList[length - 1];

                            if (!lastDto.Subcategory.Equals(dto.Subcategory))
                            {
                                subCategoryList.Add(dto);
                            }

                        }


                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return catalogList;
        }

        //<summary> Used to get videos data depending on the search criteria for movement name and athlete name from db </summary>
        internal static List<VideosInfoDTO> GetVideoBySearchText(string searchText)
        {
            List<VideosInfoDTO> videoInfo = new List<VideosInfoDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllVideosBySearchText]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter SearchText = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 100);
                    SearchText.Direction = ParameterDirection.Input;
                    SearchText.Value = searchText;



                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideosInfoDTO v = new VideosInfoDTO();

                        v.ID = (int)myData["ID"];
                        v.Segment = (string)myData["segment"];
                        v.Category = (string)myData["category"];
                        v.SubCategory = (string)myData["subcategory"];
                        v.MovementName = (string)myData["movementname"];
                        v.ActorName = (string)myData["actorname"];
                        v.Orientation = (int)myData["orientation"];
                        v.Facing = (int)myData["facing"];
                        v.AverageRating = (int)myData["averagerating"];
                        v.URL = (string)myData["url"];
                        v.ThumbNailURL = (string)myData["thumbnailurl"];
                        v.Type = (int)myData["type"];
                        v.PlayCount = (int)myData["PlayCount"];
                        v.VideoMode = (myData["VideoMode"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["VideoMode"]);
                        v.AudioMix = (myData["AudioMix"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AudioMix"]);
                        v.ProcessedVideoURL = (myData["ProcessedVideoURL"] == DBNull.Value) ? "" : (string)myData["ProcessedVideoURL"];
                        v.ProcessedThumbNailURL = (myData["ProcessedThumbNailURL"] == DBNull.Value) ? "" : (string)myData["ProcessedThumbNailURL"];    //BY LOHIT
                        videoInfo.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videoInfo;
        }

        //End Of Code by Bindu

        //<summary> Used to get subcategories data from db </summary>
        internal static List<CategoryDTO> GetCatalogWithContent(int lastid)
        {
            List<CategoryDTO> cat = new List<CategoryDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCatalog]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@LastID", SqlDbType.Int);
                    usrid.Direction = ParameterDirection.Input;
                    usrid.Value = lastid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CategoryDTO v = new CategoryDTO();

                        v.CategoryID = (int)myData["ID"];
                        v.Segment = (string)myData["Segment"];
                        v.Category = (string)myData["Category"];
                        v.SubCategory = (string)myData["SubCategory"];

                        cat.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return cat;
        }

        internal static List<OrganizationDTO> GetOrganizations()
        {
            List<OrganizationDTO> cat = new List<OrganizationDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetOrganizations]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        OrganizationDTO v = new OrganizationDTO();

                        v.ID = (int)myData["ID"];
                        v.Name = (string)myData["Name"];

                        cat.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return cat;
        }
        //14Nov
        internal static bool CheckUserByEmail(string emailId)
        {
            //using (var DBContext = new IkkosEntities())
            //{
            //    return (from user in DBContext.Users
            //            where user.Email == emailId
            //            select user.ID).Any();
            //}
            bool chkUser = false;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[CheckUserEmail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter email = cmd.Parameters.Add("@Email", SqlDbType.VarChar, 100);
                    email.Direction = ParameterDirection.Input;
                    email.Value = emailId;

                    SqlDataReader myData = cmd.ExecuteReader();
                    int cnt = 0;
                    while (myData.Read())
                    {
                        cnt = (int)myData["count"];
                    }
                    if (cnt > 0)
                    {
                        chkUser = true;
                    }

                    myData.Close();
                    myConn.Close();
                }
                return chkUser;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static int AddUser(UserInfoDTO userDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[RegisterUser]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Shardid = cmd.Parameters.Add("@ShardID", SqlDbType.Int);
                    Shardid.Direction = ParameterDirection.Input;
                    Shardid.Value = userDTO.ShardID;

                    SqlParameter UserName = cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 100);
                    UserName.Direction = ParameterDirection.Input;
                    UserName.Value = userDTO.UserName;

                    SqlParameter FirstName = cmd.Parameters.Add("@FirstName", SqlDbType.VarChar, 50);
                    FirstName.Direction = ParameterDirection.Input;
                    FirstName.Value = userDTO.FirstName;

                    SqlParameter LastName = cmd.Parameters.Add("@LastName", SqlDbType.VarChar, 50);
                    LastName.Direction = ParameterDirection.Input;
                    LastName.Value = userDTO.LastName;

                    SqlParameter Email = cmd.Parameters.Add("@Email", SqlDbType.VarChar, 100);
                    Email.Direction = ParameterDirection.Input;
                    Email.Value = (userDTO.Email == null) ? string.Empty : userDTO.Email;

                    SqlParameter Password = cmd.Parameters.Add("@Password", SqlDbType.VarChar, 256);
                    Password.Direction = ParameterDirection.Input;
                    Password.Value = userDTO.Password;

                    SqlParameter PhoneNumber = cmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar, 50);
                    PhoneNumber.Direction = ParameterDirection.Input;
                    PhoneNumber.Value = (userDTO.PhoneNumber == null) ? string.Empty : userDTO.PhoneNumber;

                    SqlParameter PhoneID = cmd.Parameters.Add("@PhoneID", SqlDbType.Int);
                    PhoneID.Direction = ParameterDirection.Input;
                    PhoneID.Value = userDTO.PhoneID;

                    SqlParameter OrganizationID = cmd.Parameters.Add("@OrgID", SqlDbType.Int);
                    OrganizationID.Direction = ParameterDirection.Input;
                    OrganizationID.Value = userDTO.OrganizationID;

                    SqlParameter Address1 = cmd.Parameters.Add("@Address1", SqlDbType.VarChar, 100);
                    Address1.Direction = ParameterDirection.Input;
                    Address1.Value = (userDTO.Address1 == null) ? string.Empty : userDTO.Address1;

                    SqlParameter Address2 = cmd.Parameters.Add("@Address2", SqlDbType.VarChar, 100);
                    Address2.Direction = ParameterDirection.Input;
                    Address2.Value = (userDTO.Address2 == null) ? string.Empty : userDTO.Address2;

                    SqlParameter City = cmd.Parameters.Add("@City", SqlDbType.VarChar, 50);
                    City.Direction = ParameterDirection.Input;
                    City.Value = (userDTO.City == null) ? string.Empty : userDTO.City;

                    SqlParameter State = cmd.Parameters.Add("@State", SqlDbType.VarChar, 50);
                    State.Direction = ParameterDirection.Input;
                    State.Value = (userDTO.State == null) ? string.Empty : userDTO.State;

                    SqlParameter Country = cmd.Parameters.Add("@Country", SqlDbType.VarChar, 50);
                    Country.Direction = ParameterDirection.Input;
                    Country.Value = (userDTO.Country == null) ? "USA" : userDTO.Country;

                    SqlParameter Zip = cmd.Parameters.Add("@Zip", SqlDbType.VarChar, 50);
                    Zip.Direction = ParameterDirection.Input;
                    Zip.Value = (userDTO.Zip == null) ? string.Empty : userDTO.Zip;

                    SqlParameter Age = cmd.Parameters.Add("@Age", SqlDbType.Int);
                    Age.Direction = ParameterDirection.Input;
                    Age.Value = userDTO.Age;

                    SqlParameter Gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    Gender.Direction = ParameterDirection.Input;
                    Gender.Value = userDTO.Gender;

                    SqlParameter Type = cmd.Parameters.Add("@Type", SqlDbType.Int);
                    Type.Direction = ParameterDirection.Input;
                    Type.Value = userDTO.Type;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", SqlDbType.Int);
                    CreatedBy.Direction = ParameterDirection.Input;
                    CreatedBy.Value = userDTO.CreatedBy;

                    SqlParameter month = cmd.Parameters.Add("@month", SqlDbType.Int);
                    month.Direction = ParameterDirection.Input;
                    month.Value = userDTO.Month;

                    SqlParameter year = cmd.Parameters.Add("@year", SqlDbType.Int);
                    year.Direction = ParameterDirection.Input;
                    year.Value = userDTO.Year;

                    //SqlParameter facebookId = cmd.Parameters.Add("@FacebookId", System.Data.SqlDbType.VarChar, 500);
                    //facebookId.Direction = System.Data.ParameterDirection.Input;
                    //facebookId.Value = userDTO.FacebookId;

                    //SqlParameter profilePicUrl = cmd.Parameters.Add("@ProfilePicURL", System.Data.SqlDbType.VarChar, 1000);
                    //profilePicUrl.Direction = System.Data.ParameterDirection.Input;
                    //profilePicUrl.Value = userDTO.ProfilePicUrl;

                    SqlDataReader myData = cmd.ExecuteReader();

                    //  cmd.ExecuteNonQuery();
                    while (myData.Read())
                    {
                        var retval = myData["ID"];
                        id = Convert.ToInt32(retval);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static void UpdateUser(UserInfoDTO userDTO)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateUser]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = userDTO.ID;


                    SqlParameter FirstName = cmd.Parameters.Add("@FirstName", SqlDbType.VarChar, 50);
                    FirstName.Direction = ParameterDirection.Input;
                    FirstName.Value = userDTO.FirstName;

                    SqlParameter LastName = cmd.Parameters.Add("@LastName", SqlDbType.VarChar, 50);
                    LastName.Direction = ParameterDirection.Input;
                    LastName.Value = userDTO.LastName;

                    SqlParameter Email = cmd.Parameters.Add("@Email", SqlDbType.VarChar, 100);
                    Email.Direction = ParameterDirection.Input;
                    Email.Value = (userDTO.Email == null) ? string.Empty : userDTO.Email;

                    //SqlParameter Password = cmd.Parameters.Add("@Password", SqlDbType.VarChar, 256);
                    //Password.Direction = ParameterDirection.Input;
                    //Password.Value = userDTO.Password;

                    SqlParameter PhoneNumber = cmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar, 50);
                    PhoneNumber.Direction = ParameterDirection.Input;
                    PhoneNumber.Value = (userDTO.PhoneNumber == null) ? string.Empty : userDTO.PhoneNumber;

                    SqlParameter PhoneID = cmd.Parameters.Add("@PhoneID", SqlDbType.Int);
                    PhoneID.Direction = ParameterDirection.Input;
                    PhoneID.Value = userDTO.PhoneID;

                    SqlParameter OrganizationID = cmd.Parameters.Add("@OrgID", SqlDbType.Int);
                    OrganizationID.Direction = ParameterDirection.Input;
                    OrganizationID.Value = userDTO.OrganizationID;

                    SqlParameter Address1 = cmd.Parameters.Add("@Address1", SqlDbType.VarChar, 100);
                    Address1.Direction = ParameterDirection.Input;
                    Address1.Value = (userDTO.Address1 == null) ? string.Empty : userDTO.Address1;

                    SqlParameter Address2 = cmd.Parameters.Add("@Address2", SqlDbType.VarChar, 100);
                    Address2.Direction = ParameterDirection.Input;
                    Address2.Value = (userDTO.Address2 == null) ? string.Empty : userDTO.Address2;

                    SqlParameter City = cmd.Parameters.Add("@City", SqlDbType.VarChar, 50);
                    City.Direction = ParameterDirection.Input;
                    City.Value = (userDTO.City == null) ? string.Empty : userDTO.City;

                    SqlParameter State = cmd.Parameters.Add("@State", SqlDbType.VarChar, 50);
                    State.Direction = ParameterDirection.Input;
                    State.Value = (userDTO.State == null) ? string.Empty : userDTO.State;

                    SqlParameter Country = cmd.Parameters.Add("@Country", SqlDbType.VarChar, 50);
                    Country.Direction = ParameterDirection.Input;
                    Country.Value = (userDTO.Country == null) ? "USA" : userDTO.Country;

                    SqlParameter Zip = cmd.Parameters.Add("@Zip", SqlDbType.VarChar, 50);
                    Zip.Direction = ParameterDirection.Input;
                    Zip.Value = (userDTO.Zip == null) ? string.Empty : userDTO.Zip;

                    SqlParameter Age = cmd.Parameters.Add("@Age", SqlDbType.Int);
                    Age.Direction = ParameterDirection.Input;
                    Age.Value = userDTO.Age;

                    SqlParameter Gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    Gender.Direction = ParameterDirection.Input;
                    Gender.Value = userDTO.Gender;

                    SqlParameter Type = cmd.Parameters.Add("@Type", SqlDbType.Int);
                    Type.Direction = ParameterDirection.Input;
                    Type.Value = userDTO.Type;

                    SqlParameter UpdatedBy = cmd.Parameters.Add("@UpdatedBy", SqlDbType.Int);
                    UpdatedBy.Direction = ParameterDirection.Input;
                    UpdatedBy.Value = userDTO.UpdatedBy;

                    //SqlParameter swim_exp = cmd.Parameters.Add("@swim_exp", SqlDbType.Int);
                    //swim_exp.Direction = ParameterDirection.Input;
                    //swim_exp.Value = userDTO.ExpYearsInSwimming;

                    //SqlParameter working_with_coach = cmd.Parameters.Add("@working_with_coach", SqlDbType.Bit);
                    //working_with_coach.Direction = ParameterDirection.Input;
                    //working_with_coach.Value = userDTO.WorkingWithCoach;

                    //SqlParameter subcategoryid = cmd.Parameters.Add("@subcategoryid", SqlDbType.Int);
                    //subcategoryid.Direction = ParameterDirection.Input;
                    //subcategoryid.Value = userDTO.SubCategoryId;

                    //SqlParameter level = cmd.Parameters.Add("@level", SqlDbType.VarChar, 50);
                    //level.Direction = ParameterDirection.Input;
                    //level.Value = userDTO.Level;

                    SqlParameter month = cmd.Parameters.Add("@month", SqlDbType.Int);
                    month.Direction = ParameterDirection.Input;
                    month.Value = userDTO.Month;

                    SqlParameter year = cmd.Parameters.Add("@year", SqlDbType.Int);
                    year.Direction = ParameterDirection.Input;
                    year.Value = userDTO.Year;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static UserInfoDTO ForgotPassword(string email)
        {
            UserInfoDTO userinfo = new UserInfoDTO();
            userinfo.Email = email;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[ForgotPassword]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Email = cmd.Parameters.Add("@Email", SqlDbType.VarChar, 100);
                    Email.Direction = ParameterDirection.Input;
                    Email.Value = email;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        userinfo.UserName = (string)myData["UserName"];
                        userinfo.Password = (string)myData["Password"];
                    }
                    myData.Close();
                    myConn.Close();
                }
                return userinfo;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static UserInfoDTO GetUserDetails(string username)
        {
            UserInfoDTO userinfo = new UserInfoDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserInfo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserName = cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 100);
                    UserName.Direction = ParameterDirection.Input;
                    UserName.Value = username;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        userinfo.ID = (int)myData["ID"];
                        userinfo.FirstName = Convert.ToString(myData["FirstName"]);
                        userinfo.LastName = Convert.ToString(myData["LastName"]);
                        userinfo.Email = Convert.ToString(myData["Email"]);
                        userinfo.PhoneNumber = Convert.ToString(myData["PhoneNumber"]);
                        userinfo.OrganizationID = Convert.ToInt32(myData["OrganizationID"]);
                        userinfo.Country = Convert.ToString(myData["Country"]);
                        userinfo.City = Convert.ToString(myData["City"]);
                        userinfo.Address1 = Convert.ToString(myData["Address1"]);
                        userinfo.Address2 = Convert.ToString(myData["Address2"]);
                        userinfo.Age = Convert.ToInt32(myData["Age"]);
                        userinfo.Gender = Convert.ToInt32(myData["Gender"]);
                        userinfo.State = Convert.ToString(myData["State"]);
                        userinfo.UserSports = "Golf,Fitness";
                        userinfo.Type = Convert.ToInt32(myData["Type"]);
                        userinfo.Zip = Convert.ToString(myData["Zip"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
                return userinfo;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        //internal static void GetVideoPath(int videoid, ref string videopath)
        //{
        //    using (var DBContext = new IkkosEntities())
        //    {
        //        if(DBContext.VideosInfoes.Where(x=>x.VideoId==videoid).Any())
        //        {
        //            videopath = DBContext.VideosInfoes.Where(x => x.VideoId == videoid).First().VideoUrl;
        //        }
        //    }
        //}

        internal static string GetVideoPath(int videoid)
        {
            //using (var DBContext = new IkkosEntities())
            //{
            //    if(DBContext.VideosInfoes.Where(x=>x.VideoId==videoid).Any())
            //    {
            //        videopath = DBContext.VideosInfoes.Where(x => x.VideoId == videoid).First().VideoUrl;
            //    }
            //}
            string videoPath = string.Empty;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVideoPath]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Videoid = cmd.Parameters.Add("@videoid", SqlDbType.Int);
                    Videoid.Direction = ParameterDirection.Input;
                    Videoid.Value = videoid;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        videoPath = (string)myData["Description"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videoPath;

        }

        //internal static void ApproveVideo(int videoid,int status,string updatedby)
        //{

        //    using (var DBContext = new IkkosEntities())
        //    {

        //        if (DBContext.Videos.Where(x => x.ID == videoid).Any())
        //        {

        //            Video vid = DBContext.Videos.Where(x => x.ID == videoid).First();
        //            vid.Status = status;
        //            vid.UpdatedBy = GetUseridwithEmail(updatedby);
        //            DBContext.SaveChanges();
        //        }
        //    }
        //}

        internal static void ApproveVideo(int videoid, int status, string updatedby)
        {

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[ApproveVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter VideoID = cmd.Parameters.Add("@VideoID", SqlDbType.Int);
                    VideoID.Direction = ParameterDirection.Input;
                    VideoID.Value = videoid;


                    SqlParameter statuspar = cmd.Parameters.Add("@status", SqlDbType.Int);
                    statuspar.Direction = ParameterDirection.Input;
                    statuspar.Value = status;

                    SqlParameter updatedbypar = cmd.Parameters.Add("@updatedby", SqlDbType.Int);
                    updatedbypar.Direction = ParameterDirection.Input;
                    updatedbypar.Value = GetUseridwithEmail(updatedby);


                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }
        // adding imagepath to db
        internal static void StoreImageInDB(string uri, int userid)
        {
            //using (var DBContext = new IkkosEntities())
            //{

            //    USER_IMAGES img = new USER_IMAGES();
            //    img.USERID = userid;
            //    img.IMAGE_BLOB_PATH = uri;
            //    img.STATUS = 1;
            //    DBContext.USER_IMAGES.AddObject(img);
            //    DBContext.SaveChanges();

            //}

        }
        //14Nov
        internal static bool CheckAdminByEmail(string emailId)
        {
            //using (var DBContext = new IkkosEntities())
            //{
            //    return (from user in DBContext.Users
            //            where user.Email == emailId & user.Type==1
            //            select user.ID).Any();
            //}

            bool chkAdmin = false;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[CheckAdminEmail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter email = cmd.Parameters.Add("@Email", SqlDbType.VarChar, 100);
                    email.Direction = ParameterDirection.Input;
                    email.Value = emailId;

                    SqlDataReader myData = cmd.ExecuteReader();
                    int Type = 0;
                    while (myData.Read())
                    {
                        Type = (int)myData["Type"];
                    }
                    if (Type == 1)
                    {
                        chkAdmin = true;
                    }

                    myData.Close();
                    myConn.Close();
                }
                return chkAdmin;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static string GetBlobUriByVideoId(long videoid)
        {

            string videourl = "";
            try
            {

                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[GetVideoURL]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter @VideoID = cmd.Parameters.Add("@VideoID", SqlDbType.Int);
                    @VideoID.Direction = ParameterDirection.Input;
                    @VideoID.Value = videoid;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        videourl = (string)myData["URL"];

                    }
                    myData.Close();
                    myConn.Close();
                }

            }
            catch (Exception ex)
            {
                string s = ex.Message;
                throw;

            }
            return videourl;

        }

        internal static string GetBlobThumbnailUriByVideoId(long videoid)
        {
            //using (var DBContext = new IkkosEntities())
            //{
            //    return (from video in DBContext.Videos where video.ID == videoid select video.ThumbNailURL).First();
            //}

            string ThumbNailURL = "";
            try
            {

                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[GetBlobThumbnailUriByVideoId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter @VideoID = cmd.Parameters.Add("@VideoID", SqlDbType.Int);
                    @VideoID.Direction = ParameterDirection.Input;
                    @VideoID.Value = videoid;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ThumbNailURL = (string)myData["ThumbNailURL"];

                    }
                    myData.Close();
                    myConn.Close();
                }

            }
            catch (Exception ex)
            {
                string s = ex.Message;
                throw;

            }
            return ThumbNailURL;
        }

        internal static string GetBlobThumbnailurlByVideoId(long videoid)
        {
            //using (var DBContext = new IkkosEntities())
            //{
            //    return (from video in DBContext.Videos where video.ID == videoid select video.ThumbNailURL).First();
            //}
            string ThumbNailURL = "";
            try
            {

                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[GetBlobThumbnailUriByVideoId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter @VideoID = cmd.Parameters.Add("@VideoID", SqlDbType.Int);
                    @VideoID.Direction = ParameterDirection.Input;
                    @VideoID.Value = videoid;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ThumbNailURL = (string)myData["ThumbNailURL"];

                    }
                    myData.Close();
                    myConn.Close();
                }

            }
            catch (Exception ex)
            {
                string s = ex.Message;
                throw;

            }
            return ThumbNailURL;
        }

        internal static List<VideosInfoDTO> AllVideos(int userID)
        {
            List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    usrid.Direction = ParameterDirection.Input;
                    usrid.Value = userID;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideosInfoDTO v = new VideosInfoDTO();
                        v.ID = (int)myData["ID"];
                        v.Segment = (string)myData["segment"];
                        v.Category = (string)myData["category"];
                        v.SubCategory = (string)myData["subcategory"];
                        v.MovementName = (string)myData["movementname"];
                        v.ActorName = (string)myData["actorname"];
                        v.Orientation = (int)myData["orientation"];
                        v.Facing = (int)myData["facing"];
                        int rate = (int)myData["averagerating"];
                        v.Rating = (float)(rate / 20.0);
                        v.URL = (string)myData["url"];
                        v.ThumbNailURL = (string)myData["thumbnailurl"];
                        v.PlayCount = (int)myData["PlayCount"];
                        int t = (int)myData["type"];
                        if (t == 13)
                            v.Paid = 1;
                        else
                            v.Paid = 0;
                        v.VideoMode = (myData["VideoMode"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["VideoMode"]);
                        v.AudioMix = (myData["AudioMix"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AudioMix"]);
                        v.ProcessedVideoURL = (myData["ProcessedVideoURL"] == DBNull.Value) ? "" : (string)myData["ProcessedVideoURL"];
                        v.ProcessedThumbNailURL = (myData["ProcessedThumbNailURL"] == DBNull.Value) ? "" : (string)myData["ProcessedThumbNailURL"];    //by lohit   
                        int VideoId = (int)myData["videoId"];
                        v.isPurchased = (VideoId == 0) ? false : true;


                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videos;
        }

        internal static List<VideosInfoDTO> AllVideosByStatus(int status)
        {
            List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVideosWithStatus]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    usrid.Direction = ParameterDirection.Input;
                    usrid.Value = 1;

                    SqlParameter sts = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    sts.Direction = ParameterDirection.Input;
                    sts.Value = status;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideosInfoDTO v = new VideosInfoDTO();
                        v.ID = (int)myData["ID"];
                        v.Segment = (string)myData["segment"];
                        v.Category = (string)myData["category"];
                        v.SubCategory = (string)myData["subcategory"];
                        v.MovementName = (string)myData["movementname"];
                        v.ActorName = (string)myData["actorname"];
                        v.Orientation = (int)myData["orientation"];
                        v.Facing = (int)myData["facing"];
                        int rate = (int)myData["averagerating"];
                        v.Rating = (float)(rate / 20.0);
                        v.URL = (string)myData["url"];
                        v.Status = status;
                        v.OwnerID = (int)myData["ownerId"];
                        v.ThumbNailURL = (string)myData["thumbnailurl"];
                        v.PlayCount = (int)myData["PlayCount"];
                        v.VideoMode = (myData["VideoMode"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["VideoMode"]);
                        v.AudioMix = (myData["AudioMix"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AudioMix"]);
                        v.ProcessedVideoURL = (myData["ProcessedVideoURL"] == DBNull.Value) ? "" : (string)myData["ProcessedVideoURL"];
                        v.ProcessedThumbNailURL = (myData["ProcessedThumbNailURL"] == DBNull.Value) ? "" : (string)myData["ProcessedThumbNailURL"];   //by lohit
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videos;
        }

        internal static List<VideosInfoDTO> GetRecommendedVideos()
        {
            List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {

                    DataTable t = new DataTable();
                    using (SqlDataAdapter a = new SqlDataAdapter("[RecommendedVideos]", myConn))
                    {
                        a.SelectCommand.CommandType = CommandType.StoredProcedure;

                        a.Fill(t);
                        myConn.Close();
                    }
                    //now we need to parse datarows and createvideoinfodto objects based on data in dt
                    foreach (DataRow row in t.Rows)
                    {

                        VideosInfoDTO videoinfo = new VideosInfoDTO();
                        if ((int)row["ID"] > 1)
                        {
                            videoinfo.ID = (int)row["ID"];

                            if (row["MovementName"] != null)
                                videoinfo.MovementName = (string)row["MovementName"];

                            if (!DBNull.Value.Equals(row["OwnerID"]))
                                videoinfo.OwnerID = (int)row["OwnerID"];
                            if (!DBNull.Value.Equals(row["Quality"]))
                                videoinfo.Quality = (string)row["Quality"];
                            if (!DBNull.Value.Equals(row["Size"]))
                                videoinfo.Size = (int)row["Size"];
                            if (!DBNull.Value.Equals(row["Speed"]))
                                videoinfo.Speed = (string)row["Speed"];
                            if (!DBNull.Value.Equals(row["Status"]))
                                videoinfo.Status = (int)row["Status"];
                            if (!DBNull.Value.Equals(row["SubCategoryID"]))
                                videoinfo.SubCategoryID = (int)row["SubCategoryID"];
                            if (!DBNull.Value.Equals(row["ThumbNailURL"]))
                                videoinfo.ThumbNailURL = (string)row["ThumbNailURL"];
                            if (!DBNull.Value.Equals(row["URL"]))
                                videoinfo.URL = (string)row["URL"];
                            if (!DBNull.Value.Equals(row["Type"]))
                                videoinfo.Type = (int)row["Type"];
                            if (!DBNull.Value.Equals(row["Format"]))
                                videoinfo.Format = (string)row["Format"];
                            if (!DBNull.Value.Equals(row["ActorName"]))
                                videoinfo.ActorName = (string)row["ActorName"];
                            if (!DBNull.Value.Equals(row["ShardID"]))
                                videoinfo.ShardID = (int)row["ShardID"];
                            if (!DBNull.Value.Equals(row["PlayCount"]))
                                videoinfo.PlayCount = (int)row["PlayCount"];
                            videos.Add(videoinfo);
                        }

                    }
                }
                int aa = 10;
                return videos;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static List<VideosInfoDTO> GetRecommendedVideosApp(int userid)
        {
            List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {

                    DataTable t = new DataTable();
                    using (SqlDataAdapter a = new SqlDataAdapter("[GetRecommendedVideosApp]", myConn))
                    {
                        a.SelectCommand.CommandType = CommandType.StoredProcedure;

                        SqlParameter usrid = a.SelectCommand.Parameters.Add("@UserID", SqlDbType.Int);
                        usrid.Direction = ParameterDirection.Input;
                        usrid.Value = 1;

                        a.Fill(t);
                        myConn.Close();
                    }
                    //now we need to parse datarows and createvideoinfodto objects based on data in dt
                    foreach (DataRow row in t.Rows)
                    {

                        VideosInfoDTO videoinfo = new VideosInfoDTO();
                        if ((int)row["ID"] > 1)
                        {
                            videoinfo.ID = (int)row["ID"];

                            if (row["MovementName"] != null)
                                videoinfo.MovementName = (string)row["MovementName"];

                            if (!DBNull.Value.Equals(row["OwnerID"]))
                                videoinfo.OwnerID = (int)row["OwnerID"];
                            if (!DBNull.Value.Equals(row["Quality"]))
                                videoinfo.Quality = (string)row["Quality"];
                            if (!DBNull.Value.Equals(row["Size"]))
                                videoinfo.Size = (int)row["Size"];
                            if (!DBNull.Value.Equals(row["Speed"]))
                                videoinfo.Speed = (string)row["Speed"];
                            if (!DBNull.Value.Equals(row["Status"]))
                                videoinfo.Status = (int)row["Status"];
                            if (!DBNull.Value.Equals(row["SubCategoryID"]))
                                videoinfo.SubCategoryID = (int)row["SubCategoryID"];
                            if (!DBNull.Value.Equals(row["ThumbNailURL"]))
                                videoinfo.ThumbNailURL = (string)row["ThumbNailURL"];
                            if (!DBNull.Value.Equals(row["URL"]))
                                videoinfo.URL = (string)row["URL"];
                            if (!DBNull.Value.Equals(row["Type"]))
                                videoinfo.Type = (int)row["Type"];
                            if (!DBNull.Value.Equals(row["Format"]))
                                videoinfo.Format = (string)row["Format"];
                            if (!DBNull.Value.Equals(row["ActorName"]))
                                videoinfo.ActorName = (string)row["ActorName"];
                            if (!DBNull.Value.Equals(row["ShardID"]))
                                videoinfo.ShardID = (int)row["ShardID"];
                            if (!DBNull.Value.Equals(row["PlayCount"]))
                                videoinfo.PlayCount = (int)row["PlayCount"];
                            if (!DBNull.Value.Equals(row["Name"]))
                                videoinfo.SubCategory = (string)row["Name"];
                            if (!DBNull.Value.Equals(row["Category"]))
                                videoinfo.Category = (string)row["Category"];
                            if (!DBNull.Value.Equals(row["Segment"]))
                                videoinfo.Segment = (string)row["Segment"];
                            videos.Add(videoinfo);
                        }

                    }
                }
                int aa = 10;
                return videos;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static PromotionalContentDTO GetVideoOfTheDay(int userid, int catid, int DeviceId)
        {
            PromotionalContentDTO promoContent = new PromotionalContentDTO();

            try
            {



                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFeaturedVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    usrid.Direction = ParameterDirection.Input;
                    usrid.Value = userid;

                    SqlParameter cid = cmd.Parameters.Add("@CategoryID", SqlDbType.Int);
                    cid.Direction = ParameterDirection.Input;
                    cid.Value = catid;

                    SqlParameter dt = cmd.Parameters.Add("@Date", SqlDbType.DateTime);
                    dt.Direction = ParameterDirection.Input;
                    dt.Value = DateTime.Now;

                    SqlParameter deviceId = cmd.Parameters.Add("@deviceId", SqlDbType.Int);
                    deviceId.Direction = ParameterDirection.Input;
                    deviceId.Value = DeviceId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        //v.ID = (int)myData["ID"];
                        //v.Segment = (string)myData["segment"];
                        //v.Category = (string)myData["category"];
                        //v.SubCategory = (string)myData["subcategory"];
                        //v.MovementName = (string)myData["movementname"];
                        //v.ActorName = (string)myData["actorname"];
                        //v.Orientation = (int)myData["orientation"];
                        //v.Facing = (int)myData["facing"];
                        //int rate = (int)myData["averagerating"];
                        //v.Rating = (float)(rate/20.0);
                        //v.URL = (string)myData["url"];
                        //v.ThumbNailURL = (string)myData["thumbnailurl"];
                        //v.PlayCount = (int)myData["PlayCount"];
                        promoContent.Id = (int)myData["ID"];
                        promoContent.Title = (string)myData["Title"];
                        promoContent.Name = (string)myData["Name"];
                        promoContent.Description = (string)myData["Description"];
                        promoContent.ThumbNailURL = (string)myData["ThumbNailURL"];
                        promoContent.URL = (string)myData["URL"];
                        promoContent.ActionType = (string)myData["ActionType"];
                        promoContent.EventStartDate = (DateTime)myData["EventStartDate"];
                        promoContent.EventEndDate = (DateTime)myData["EventEndDate"];
                        promoContent.ActionTypeID = (int)myData["Type"];
                        promoContent.Status = (int)myData["Status"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return promoContent;
        }

        internal static VideosInfoDTO HowTo(int userid, int videoid)
        {
            // todo: check this.
            if (videoid == 1)
                videoid = 102;
            else if (videoid == 2)
                videoid = 103;
            else
                videoid = 104;

            return GetVideo(videoid);
        }

        //internal static List<VideosInfoDTO> GetAllUserVideos(int ownerid)
        //{
        //    using (var DBContext = new IkkosEntities())
        //    {
        //        // videos.Status == 1 &&
        //        return (from videos in DBContext.Videos  where  videos.OwnerID==ownerid
        //                select new VideosInfoDTO
        //                {
        //                    ActorName = videos.ActorName,
        //                    //   IAPProductId = videos.IAPProductId,
        //                    MovementName = videos.MovementName,
        //                    OwnerID = videos.OwnerID,
        //                    Quality = videos.Quality,
        //                    Speed = videos.Speed,Size=videos.Size,Duration=videos.Duration,
        //                    //  SportsCategory = i.VideosInfo.SportsCategory,
        //                    //   SportsId = videos.SportsId,
        //                    //SportsCategory = new SportsCategoryDTO
        //                    //{
        //                    //    SportsId = videos.SportsCategory.SportsId,
        //                    //    SportsName = videos.SportsCategory.SportsName
        //                    //},
        //                    SubCategoryID=videos.SubCategoryID,
        //                    Category = "Golf",
        //                    SubCategory = "Putter",
        //                    ThumbNailURL=videos.ThumbNailURL,
        //                    Format = videos.Format,
        //                    ID = videos.ID,
        //                    URL = videos.URL

        //                }).ToList();
        //    }
        //}

        internal static VideosInfoDTO GetVideo(long VideoId)
        {
            VideosInfoDTO video = new VideosInfoDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@VideoID", SqlDbType.Int);
                    usrid.Direction = ParameterDirection.Input;
                    usrid.Value = VideoId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        //  VideosInfoDTO v = new VideosInfoDTO();
                        video.ID = (int)myData["ID"];
                        video.Segment = (string)myData["segment"];
                        video.Category = (string)myData["category"];
                        video.SubCategory = (string)myData["subcategory"];
                        video.MovementName = (string)myData["movementname"];
                        video.ActorName = (string)myData["actorname"];
                        video.Orientation = (int)myData["orientation"];
                        video.Facing = (int)myData["facing"];
                        int rate = (int)myData["averagerating"];
                        video.Rating = (float)(rate / 20.0);
                        video.URL = (string)myData["url"];
                        video.ThumbNailURL = (string)myData["thumbnailurl"];
                        video.PlayCount = (int)myData["PlayCount"];
                        video.VideoMode = (myData["VideoMode"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["VideoMode"]);
                        video.AudioMix = (myData["AudioMix"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AudioMix"]);
                        video.ProcessedVideoURL = (myData["ProcessedVideoURL"] == DBNull.Value) ? "" : (string)myData["ProcessedVideoURL"];
                        video.ProcessedThumbNailURL = (myData["ProcessedThumbNailURL"] == DBNull.Value) ? "" : (string)myData["ProcessedThumbNailURL"];   //by lohit 
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return video;
        }

        internal static int GetUseridwithEmail(string email)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserInfo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserName = cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 100);
                    UserName.Direction = ParameterDirection.Input;
                    UserName.Value = email;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["ID"];
                    }
                    myData.Close();
                    myConn.Close();
                }
                return id;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static long IsValidUser(string emailId)
        {
            long userid = 0;
            //using (var DBContext = new IkkosEntities())
            //{
            //    try
            //    {
            //       userid = DBContext.UserInfoes.Where(x => x.EmailId == emailId).First().UserId; 
            //    }
            //    catch (Exception ex)
            //    {
            //    }

            //    return userid;
            //}


            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[IsValidUser]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Email = cmd.Parameters.Add("@emailId", SqlDbType.VarChar, 100);
                    Email.Direction = ParameterDirection.Input;
                    Email.Value = emailId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        userid = (myData["ID"] == null) ? 0 : (long)myData["ID"];
                    }
                    myData.Close();
                    myConn.Close();
                }
                return userid;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        //<summary> this method add rating info to db</summary>
        internal static void AddRating(VideosRatingDTO ratingDTO)
        {
            UserInfoDTO userinfo = new UserInfoDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddRating]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter videoid = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    videoid.Direction = ParameterDirection.Input;
                    videoid.Value = ratingDTO.VideoId;

                    SqlParameter userid = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    userid.Direction = ParameterDirection.Input;
                    userid.Value = ratingDTO.UserId;

                    SqlParameter rating = cmd.Parameters.Add("@Rating", SqlDbType.Int);
                    userid.Direction = ParameterDirection.Input;
                    rating.Value = ratingDTO.Rating * 20;

                    cmd.ExecuteNonQuery();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        //<summary>This Method addes Video Visibility </summary> <parameters>visibility ids 7-friend,10-public,11-private</parameters>
        internal static void AddVideoVisibility(Visibility_DTO visibilitydto)
        {
            UserInfoDTO userinfo = new UserInfoDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddVisibility]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter videoid = cmd.Parameters.Add("@VideoId", SqlDbType.Int, 100);
                    videoid.Direction = ParameterDirection.Input;
                    videoid.Value = visibilitydto.VideoId;

                    SqlParameter updatedby = cmd.Parameters.Add("@Updatedby", SqlDbType.Int, 100);
                    updatedby.Direction = ParameterDirection.Input;
                    updatedby.Value = visibilitydto.UpdatedBy;

                    SqlParameter visibility = cmd.Parameters.Add("@Visibility", SqlDbType.Int, 100);
                    visibility.Direction = ParameterDirection.Input;
                    visibility.Value = visibilitydto.VisibilityId;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static void AddFeedback(FeedbackDTO fb)
        {
            int phoneid = 1;
            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddFeedback]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter phone = cmd.Parameters.Add("@PhoneID", SqlDbType.Int);
                    phone.Direction = ParameterDirection.Input;
                    phone.Value = phoneid;

                    SqlParameter user = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    user.Direction = ParameterDirection.Input;
                    user.Value = fb.name;

                    SqlParameter q1 = cmd.Parameters.Add("@Question1", SqlDbType.Int);
                    q1.Direction = ParameterDirection.Input;
                    q1.Value = fb.question1;

                    SqlParameter q2 = cmd.Parameters.Add("@Question2", SqlDbType.Int);
                    q2.Direction = ParameterDirection.Input;
                    q2.Value = fb.question2;

                    SqlParameter q3 = cmd.Parameters.Add("@Question3", SqlDbType.Int);
                    q3.Direction = ParameterDirection.Input;
                    q3.Value = fb.question3;

                    SqlParameter notes = cmd.Parameters.Add("@Comment", SqlDbType.VarChar, 4096);
                    notes.Direction = ParameterDirection.Input;
                    notes.Value = fb.comment;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static int AddVideo(VideosInfoDTO videoDTO)
        {
            try
            {
                int vid = 0;
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = videoDTO.OwnerID;

                    SqlParameter Segment = cmd.Parameters.Add("@Segment", SqlDbType.VarChar, 100);
                    Segment.Direction = ParameterDirection.Input;
                    Segment.Value = videoDTO.Segment;

                    SqlParameter Category = cmd.Parameters.Add("@Category", SqlDbType.VarChar, 100);
                    Category.Direction = ParameterDirection.Input;
                    Category.Value = videoDTO.Category;

                    SqlParameter SubCategory = cmd.Parameters.Add("@SubCategory", SqlDbType.VarChar, 100);
                    SubCategory.Direction = ParameterDirection.Input;
                    SubCategory.Value = videoDTO.SubCategory;

                    SqlParameter MovementName = cmd.Parameters.Add("@MovementName", SqlDbType.VarChar, 100);
                    MovementName.Direction = ParameterDirection.Input;
                    MovementName.Value = videoDTO.MovementName;

                    SqlParameter ActorName = cmd.Parameters.Add("@ActorName", SqlDbType.VarChar, 100);
                    ActorName.Direction = ParameterDirection.Input;
                    ActorName.Value = videoDTO.ActorName;

                    SqlParameter Orientation = cmd.Parameters.Add("@Orientation", SqlDbType.Int);
                    Orientation.Direction = ParameterDirection.Input;
                    Orientation.Value = videoDTO.Orientation;

                    SqlParameter Facing = cmd.Parameters.Add("@Facing", SqlDbType.Int);
                    Facing.Direction = ParameterDirection.Input;
                    Facing.Value = videoDTO.Facing;

                    SqlParameter ThumbNailURL = cmd.Parameters.Add("@ThumbNailURL", SqlDbType.VarChar, 100);
                    ThumbNailURL.Direction = ParameterDirection.Input;
                    ThumbNailURL.Value = videoDTO.ThumbNailURL;

                    SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 100);
                    VideoURL.Direction = ParameterDirection.Input;
                    VideoURL.Value = videoDTO.URL;

                    SqlParameter Format = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    Format.Direction = ParameterDirection.Input;
                    Format.Value = videoDTO.Status;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        vid = (int)myData["ID"];
                    }

                    myConn.Close();
                }

                return vid;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static int AdminAddVideo(VideosInfoDTO videoDTO)
        {
            try
            {
                int vid = 0;
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AdminAddVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = videoDTO.OwnerID;

                    SqlParameter Segment = cmd.Parameters.Add("@Segment", SqlDbType.VarChar, 100);
                    Segment.Direction = ParameterDirection.Input;
                    Segment.Value = videoDTO.Segment;

                    SqlParameter Category = cmd.Parameters.Add("@Category", SqlDbType.VarChar, 100);
                    Category.Direction = ParameterDirection.Input;
                    Category.Value = videoDTO.Category;

                    SqlParameter SubCategory = cmd.Parameters.Add("@SubCategory", SqlDbType.VarChar, 100);
                    SubCategory.Direction = ParameterDirection.Input;
                    SubCategory.Value = videoDTO.SubCategory;

                    SqlParameter MovementName = cmd.Parameters.Add("@MovementName", SqlDbType.VarChar, 100);
                    MovementName.Direction = ParameterDirection.Input;
                    MovementName.Value = videoDTO.MovementName;

                    SqlParameter ActorName = cmd.Parameters.Add("@ActorName", SqlDbType.VarChar, 100);
                    ActorName.Direction = ParameterDirection.Input;
                    ActorName.Value = videoDTO.ActorName;

                    SqlParameter Orientation = cmd.Parameters.Add("@Orientation", SqlDbType.Int);
                    Orientation.Direction = ParameterDirection.Input;
                    Orientation.Value = videoDTO.Orientation;

                    SqlParameter Facing = cmd.Parameters.Add("@Facing", SqlDbType.Int);
                    Facing.Direction = ParameterDirection.Input;
                    Facing.Value = videoDTO.Facing;

                    SqlParameter ThumbNailURL = cmd.Parameters.Add("@ThumbNailURL", SqlDbType.VarChar, 100);
                    ThumbNailURL.Direction = ParameterDirection.Input;
                    ThumbNailURL.Value = videoDTO.ThumbNailURL;

                    SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 100);
                    VideoURL.Direction = ParameterDirection.Input;
                    VideoURL.Value = videoDTO.URL;

                    SqlParameter Format = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    Format.Direction = ParameterDirection.Input;
                    Format.Value = videoDTO.Status;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        vid = (int)myData["ID"];
                    }

                    myConn.Close();
                }

                return vid;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static void StreamVideo(VideoFilesDTO videoDTO)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateVideoURLs]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter VideoID = cmd.Parameters.Add("@VideoID", SqlDbType.Int);
                    VideoID.Direction = ParameterDirection.Input;
                    VideoID.Value = videoDTO.VideoID;

                    SqlParameter ThumbNailURL = cmd.Parameters.Add("@ThumbNailURL", SqlDbType.VarChar, 1024);
                    ThumbNailURL.Direction = ParameterDirection.Input;
                    ThumbNailURL.Value = (videoDTO.ThumbNailURL == null) ? string.Empty : videoDTO.ThumbNailURL;

                    SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    VideoURL.Direction = ParameterDirection.Input;
                    VideoURL.Value = (videoDTO.URL == null) ? string.Empty : videoDTO.URL;

                    cmd.ExecuteNonQuery();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static bool CheckUser(string email)
        {
            //using (var DBContext = new IkkosEntities())
            //{
            //    return (from a in DBContext.Users where a.Email == email select a).Any();
            //}
            return true;
        }

        //internal static int GetUserRoleByEmail(string email)
        //{
        //    int role = 0;
        //    try
        //    {
        //        using (var DBContext = new IkkosEntities())
        //        {
        //            role = DBContext.Users.Where(x => x.Email == email).First().Type;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        role=0;
        //    }

        //    return role;
        //}
        internal static int GetUserRoleByEmail(string email)
        {
            int role = 0;
            try
            {

                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserRoleByEmail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter email1 = cmd.Parameters.Add("@email", SqlDbType.VarChar, 100);
                    email1.Direction = ParameterDirection.Input;
                    email1.Value = email;



                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        role = (int)myData["type"];
                    }

                    myConn.Close();
                }

            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return role;
        }
        internal static bool GetUserRole(long? userid)
        {
            bool isadmin = false;
            try
            {
                //using (var DBContext = new IkkosEntities())
                //{
                //    isadmin = DBContext.Users.Where(x => x.ID == userid && x.Type==1).Any();

                //}
            }
            catch (Exception ex)
            {
                isadmin = false;
            }

            return isadmin;
        }

        internal static object PackageFeatures()
        {
            //using (var DBContext = new IkkosEntities())
            //{
            //    return (from pack in DBContext.Packages
            //            select new
            //            {
            //                PackID = pack.PackID,
            //                PackName = pack.PackName,
            //                PackCost = pack.PackCost,
            //                Features = (from a in DBContext.PackFeatures
            //                            where a.PackID == pack.PackID
            //                            select new
            //                            {
            //                                FeatureID = a.Feature.FeatureID,
            //                                FeatureName = a.Feature.FeatureName,
            //                            }
            //                                ),
            //            }).ToList();
            //}

            return null;
        }

        internal static List<string> Segments()
        {
            List<string> segments = new List<string>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSegments]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        segments.Add((string)myData["Segment"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return segments;
        }

        internal static List<string> Categories(string segmentname)
        {
            List<string> categories = new List<string>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCategories]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter VideoID = cmd.Parameters.Add("@Segment", SqlDbType.VarChar, 50);
                    VideoID.Direction = ParameterDirection.Input;
                    VideoID.Value = segmentname;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        categories.Add((string)myData["Category"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return categories;
        }

        internal static List<string> Subcategories(string categoryname)
        {
            List<string> subcategories = new List<string>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSubCategories]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter VideoID = cmd.Parameters.Add("@Category", SqlDbType.VarChar, 50);
                    VideoID.Direction = ParameterDirection.Input;
                    VideoID.Value = categoryname;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        subcategories.Add((string)myData["SubCategory"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return subcategories;
        }

        internal static bool CheckVideoExists(long VideoId)
        {
            //using (var DBContext = new IkkosEntities())
            //{
            //    return (from a in DBContext.Videos where a.ID == VideoId select a).Any();
            //}
            bool chkVideo = false;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[CheckVideoExists]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter videoId = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    videoId.Direction = ParameterDirection.Input;
                    videoId.Value = VideoId;

                    SqlDataReader myData = cmd.ExecuteReader();
                    int cnt = 0;
                    while (myData.Read())
                    {
                        cnt = (int)myData["cnt"];
                    }
                    if (cnt > 0)
                    {
                        chkVideo = true;
                    }

                    myData.Close();
                    myConn.Close();
                }
                return chkVideo;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

        }

        internal static void UpdateVideo(VideosInfoDTO videoDTO)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter VideoID = cmd.Parameters.Add("@VideoID", SqlDbType.Int);
                    VideoID.Direction = ParameterDirection.Input;
                    VideoID.Value = videoDTO.ID;

                    SqlParameter Segment = cmd.Parameters.Add("@Segment", SqlDbType.VarChar, 100);
                    Segment.Direction = ParameterDirection.Input;
                    Segment.Value = videoDTO.Segment;

                    SqlParameter Category = cmd.Parameters.Add("@Category", SqlDbType.VarChar, 100);
                    Category.Direction = ParameterDirection.Input;
                    Category.Value = videoDTO.Category;

                    SqlParameter SubCategory = cmd.Parameters.Add("@SubCategory", SqlDbType.VarChar, 100);
                    SubCategory.Direction = ParameterDirection.Input;
                    SubCategory.Value = videoDTO.SubCategory;

                    SqlParameter ActorName = cmd.Parameters.Add("@ActorName", SqlDbType.VarChar, 100);
                    ActorName.Direction = ParameterDirection.Input;
                    ActorName.Value = videoDTO.ActorName;

                    SqlParameter MovementName = cmd.Parameters.Add("@MovementName", SqlDbType.VarChar, 100);
                    MovementName.Direction = ParameterDirection.Input;
                    MovementName.Value = videoDTO.MovementName;

                    SqlParameter Orientation = cmd.Parameters.Add("@Orientation", SqlDbType.Int);
                    Orientation.Direction = ParameterDirection.Input;
                    Orientation.Value = videoDTO.Orientation;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = videoDTO.OwnerID;

                    SqlParameter Facing = cmd.Parameters.Add("@Facing", SqlDbType.VarChar, 100);
                    Facing.Direction = ParameterDirection.Input;
                    Facing.Value = videoDTO.Facing;

                    SqlParameter Format = cmd.Parameters.Add("@Format", SqlDbType.VarChar, 100);
                    Format.Direction = ParameterDirection.Input;
                    Format.Value = videoDTO.Format.ToLower();

                    SqlParameter ThumbNailURL = cmd.Parameters.Add("@ThumbNailURL", SqlDbType.VarChar, 1024);
                    ThumbNailURL.Direction = ParameterDirection.Input;
                    ThumbNailURL.Value = videoDTO.ThumbNailURL;

                    SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    VideoURL.Direction = ParameterDirection.Input;
                    VideoURL.Value = videoDTO.URL;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static void DeleteVideo(long videoId, int userid)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter ID = cmd.Parameters.Add("@VideoID", SqlDbType.Int);
                    ID.Direction = ParameterDirection.Input;
                    ID.Value = videoId;

                    SqlParameter userId = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    userId.Direction = ParameterDirection.Input;
                    userId.Value = userid;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static int ChangePassword(int UserId, string oldpassword, string newpassword)
        {
            int updated = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[ChangePassword]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@userid", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = UserId;

                    SqlParameter OldPassword = cmd.Parameters.Add("@oldpassword", SqlDbType.VarChar, 256);
                    OldPassword.Direction = ParameterDirection.Input;
                    OldPassword.Value = oldpassword;

                    SqlParameter NewPassword = cmd.Parameters.Add("@newpassword", SqlDbType.VarChar, 256);
                    NewPassword.Direction = ParameterDirection.Input;
                    NewPassword.Value = newpassword;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        updated = (int)myData["Updated"];
                    }


                    myData.Close();
                    myConn.Close();


                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return updated;
        }

        internal static List<VideosFlagReasonDTO> GetFlagReasons()
        {
            List<VideosFlagReasonDTO> FlagReasons = new List<VideosFlagReasonDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFlagReasons]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideosFlagReasonDTO v = new VideosFlagReasonDTO();
                        v.VideoFlagId = (int)myData["ID"];
                        v.Description = (string)myData["Description"];
                        FlagReasons.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return FlagReasons;
        }



        //<summary> this method add flagreasons info to db</summary>
        internal static void AddVideoFlag(VideosFlagReasonDTO FlagReasonDTO)
        {
            UserInfoDTO userinfo = new UserInfoDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddFlagReason]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter videoid = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    videoid.Direction = ParameterDirection.Input;
                    videoid.Value = FlagReasonDTO.VideoId;

                    SqlParameter userid = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    userid.Direction = ParameterDirection.Input;
                    userid.Value = FlagReasonDTO.UserId;

                    SqlParameter flagID = cmd.Parameters.Add("@FlagId", SqlDbType.Int);
                    flagID.Direction = ParameterDirection.Input;
                    flagID.Value = FlagReasonDTO.VideoFlagId;

                    cmd.ExecuteNonQuery();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }


        internal static List<VideosInfoDTO> AllUserVideos(int userID)
        {
            List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    usrid.Direction = ParameterDirection.Input;
                    usrid.Value = userID;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideosInfoDTO v = new VideosInfoDTO();
                        v.ID = (int)myData["ID"];
                        v.Segment = (string)myData["segment"];
                        v.Category = (string)myData["category"];
                        v.SubCategory = (string)myData["subcategory"];
                        v.MovementName = (string)myData["movementname"];
                        v.ActorName = (string)myData["actorname"];
                        v.Orientation = (int)myData["orientation"];
                        v.Facing = (int)myData["facing"];
                        int rate = (int)myData["averagerating"];
                        v.Rating = (float)(rate / 20.0);
                        v.URL = (string)myData["url"];
                        v.ThumbNailURL = (string)myData["thumbnailurl"];
                        int t = (int)myData["type"];
                        if (t == 13)
                            v.Paid = 1;
                        else
                            v.Paid = 0;
                        v.OwnerID = (int)myData["OwnerID"];
                        v.PlayCount = (int)myData["PlayCount"];
                        v.VideoMode = (myData["VideoMode"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["VideoMode"]);
                        v.AudioMix = (myData["AudioMix"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AudioMix"]);
                        v.ProcessedVideoURL = (myData["ProcessedVideoURL"] == DBNull.Value) ? "" : (string)myData["ProcessedVideoURL"];
                        v.ProcessedThumbNailURL = (myData["ProcessedThumbNailURL"] == DBNull.Value) ? "" : (string)myData["ProcessedThumbNailURL"];    //by lohit
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videos;
        }



        internal static VideosInfoDTO GetPendingVideo(long VideoId)
        {
            VideosInfoDTO video = new VideosInfoDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPendingVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@VideoID", SqlDbType.Int);
                    usrid.Direction = ParameterDirection.Input;
                    usrid.Value = VideoId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        //  VideosInfoDTO v = new VideosInfoDTO();
                        video.ID = (int)myData["ID"];
                        video.Segment = (string)myData["segment"];
                        video.Category = (string)myData["category"];
                        video.SubCategory = (string)myData["subcategory"];
                        video.MovementName = (string)myData["movementname"];
                        video.ActorName = (string)myData["actorname"];
                        video.Orientation = (int)myData["orientation"];
                        video.Facing = (int)myData["facing"];
                        int rate = (int)myData["averagerating"];
                        video.Rating = (float)(rate / 20.0);
                        video.URL = (string)myData["url"];
                        video.ThumbNailURL = (string)myData["thumbnailurl"];
                        video.PlayCount = (int)myData["PlayCount"];
                        video.VideoMode = (myData["VideoMode"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["VideoMode"]);
                        video.AudioMix = (myData["AudioMix"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AudioMix"]);
                        video.ProcessedVideoURL = (myData["ProcessedVideoURL"] == DBNull.Value) ? "" : (string)myData["ProcessedVideoURL"];
                        video.ProcessedThumbNailURL = (myData["ProcessedThumbNailURL"] == DBNull.Value) ? "" : (string)myData["ProcessedThumbNailURL"];    //BY LOHIT

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return video;
        }

        internal static void AddUserPreferences(string Category, int userId)
        {

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddUserPreferences]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = userId;

                    SqlParameter category = cmd.Parameters.Add("@Category", SqlDbType.VarChar, 50);
                    category.Direction = ParameterDirection.Input;
                    category.Value = Category;


                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }


        }

        internal static void UpdateVideoUrl(long VideoID, int UserId, string VideoURL, string ThumbNailURL)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateVideoThumbURLs]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter videoID = cmd.Parameters.Add("@VideoID", SqlDbType.Int);
                    videoID.Direction = ParameterDirection.Input;
                    videoID.Value = VideoID;

                    SqlParameter userId = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    userId.Direction = ParameterDirection.Input;
                    userId.Value = UserId;

                    SqlParameter thumbNailURL = cmd.Parameters.Add("@ThumbNailURL", SqlDbType.VarChar, 1024);
                    thumbNailURL.Direction = ParameterDirection.Input;
                    thumbNailURL.Value = (ThumbNailURL == null) ? string.Empty : ThumbNailURL;

                    SqlParameter videoURL = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    videoURL.Direction = ParameterDirection.Input;
                    videoURL.Value = VideoURL;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }


        internal static void AddUsageTracking(UsageDTO usageDTO)
        {

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UsersUsageTracking]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter featureID = cmd.Parameters.Add("@FeatureID", SqlDbType.Int);
                    featureID.Direction = ParameterDirection.Input;
                    featureID.Value = usageDTO.FeatureId;

                    SqlParameter count = cmd.Parameters.Add("@Count", SqlDbType.Int);
                    count.Direction = ParameterDirection.Input;
                    count.Value = usageDTO.Count;

                    SqlParameter value = cmd.Parameters.Add("@Value", SqlDbType.Int);
                    value.Direction = ParameterDirection.Input;
                    value.Value = usageDTO.Value;

                    SqlParameter user = cmd.Parameters.Add("@User", SqlDbType.Int);
                    user.Direction = ParameterDirection.Input;
                    user.Value = usageDTO.UserId;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }


        internal static void DeleteUserPreferences(int userid)
        {

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteUserPreferences]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter userId = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    userId.Direction = ParameterDirection.Input;
                    userId.Value = userid;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static void fnAddUserPreferences(ExpertDTO expertDTO, int id)
        {
            string userPreference = expertDTO.UserPreference;
            string[] strUserPreference = userPreference.Split(',');
            for (int i = 0; i < strUserPreference.Length; i++)
            {
                UsersDAL.AddUserPreferences(strUserPreference[i].ToString(), id);
            }
        }

        internal static int AddExpertUser(ExpertDTO expertDTO)
        {
            int userid = 0;
            SqlConnection conn = ConnectTODB();
            SqlTransaction transaction;
            transaction = conn.BeginTransaction();
            SqlCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            try
            {
                userid = AddUser(expertDTO);
                fnAddUserPreferences(expertDTO, userid);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
            }

            return userid;
        }


        internal static void UpdateExpertUser(ExpertDTO expertDTO)
        {
            SqlConnection conn = ConnectTODB();
            SqlTransaction transaction;
            transaction = conn.BeginTransaction();
            SqlCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            try
            {
                UpdateUser(expertDTO);
                DeleteUserPreferences(GetUseridwithEmail(expertDTO.Email));
                fnAddUserPreferences(expertDTO, expertDTO.ID);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
            }

        }


        internal static List<VideosInfoDTO> GetVideosInRange(int videoID1, int videoID2)
        {
            List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllVideosBetweenRangeofIDS]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter VideoID1 = cmd.Parameters.Add("@VideoID1", SqlDbType.Int);
                    VideoID1.Direction = ParameterDirection.Input;
                    VideoID1.Value = videoID1;

                    SqlParameter VideoID2 = cmd.Parameters.Add("@VideoID2", SqlDbType.Int);
                    VideoID2.Direction = ParameterDirection.Input;
                    VideoID2.Value = videoID2;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideosInfoDTO v = new VideosInfoDTO();
                        v.ID = (int)myData["ID"];
                        v.Segment = (string)myData["segment"];
                        v.Category = (string)myData["category"];
                        v.SubCategory = (string)myData["subcategory"];
                        v.MovementName = (string)myData["movementname"];
                        v.ActorName = (string)myData["actorname"];
                        v.Orientation = (int)myData["orientation"];
                        v.Facing = (int)myData["facing"];
                        int rate = (int)myData["averagerating"];
                        v.Rating = (float)(rate / 20.0);
                        v.URL = (string)myData["url"];
                        v.ThumbNailURL = (string)myData["thumbnailurl"];
                        v.PlayCount = (int)myData["PlayCount"];
                        int t = (int)myData["type"];
                        if (t == 13)
                            v.Paid = 1;
                        else
                            v.Paid = 0;
                        v.VideoMode = (myData["VideoMode"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["VideoMode"]);
                        v.AudioMix = (myData["AudioMix"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AudioMix"]);
                        v.ProcessedVideoURL = (myData["ProcessedVideoURL"] == DBNull.Value) ? "" : (string)myData["ProcessedVideoURL"];
                        v.ProcessedThumbNailURL = (myData["ProcessedThumbNailURL"] == DBNull.Value) ? "" : (string)myData["ProcessedThumbNailURL"];    //by lohit
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videos;
        }


        internal static List<PromotionalContentDTO> GetPromotionalContent(DateTime Date)
        {
            List<PromotionalContentDTO> PromotionalContent = new List<PromotionalContentDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPromotions]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter date = cmd.Parameters.Add("@Date", SqlDbType.DateTime);
                    date.Direction = ParameterDirection.Input;
                    date.Value = Date;


                    SqlDataReader myData = cmd.ExecuteReader();


                    while (myData.Read())
                    {
                        PromotionalContentDTO promoContent = new PromotionalContentDTO();
                        promoContent.Id = (int)myData["ID"];
                        promoContent.Title = (string)myData["Title"];
                        promoContent.Name = (string)myData["Name"];
                        promoContent.Description = (string)myData["Description"];
                        promoContent.ThumbNailURL = (string)myData["ThumbNailURL"];
                        promoContent.URL = (string)myData["URL"];
                        promoContent.ActionType = (string)myData["ActionType"];
                        promoContent.EventStartDate = (DateTime)myData["EventStartDate"];
                        promoContent.EventEndDate = (DateTime)myData["EventEndDate"];
                        promoContent.ActionTypeID = (int)myData["Type"];
                        promoContent.Status = (int)myData["Status"];
                        promoContent.StartDate = (DateTime)myData["StartDate"];
                        promoContent.EndDate = (DateTime)myData["EndDate"];


                        PromotionalContent.Add(promoContent);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return PromotionalContent;
        }

        internal static List<PromotionalContentDTO> GetPromotionalContentByType(int type)
        {
            List<PromotionalContentDTO> PromotionalContent = new List<PromotionalContentDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPromotionalContentByType]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Type = cmd.Parameters.Add("@Type", SqlDbType.Int);
                    Type.Direction = ParameterDirection.Input;
                    Type.Value = type;

                    SqlDataReader myData = cmd.ExecuteReader();


                    while (myData.Read())
                    {
                        PromotionalContentDTO promoContent = new PromotionalContentDTO();
                        promoContent.Id = (int)myData["ID"];
                        promoContent.Title = (string)myData["Title"];
                        promoContent.Name = (string)myData["Name"];
                        promoContent.Description = (string)myData["Description"];
                        promoContent.ThumbNailURL = (string)myData["ThumbNailURL"];
                        promoContent.URL = (string)myData["URL"];
                        promoContent.ActionType = (string)myData["ActionType"];
                        promoContent.EventStartDate = (DateTime)myData["EventStartDate"];
                        promoContent.EventEndDate = (DateTime)myData["EventEndDate"];
                        promoContent.ActionTypeID = (int)myData["Type"];
                        promoContent.Status = (int)myData["Status"];
                        promoContent.StartDate = (DateTime)myData["StartDate"];
                        promoContent.EndDate = (DateTime)myData["EndDate"];
                        // promoContent.UpdateDate = (DateTime)myData["UpdateDate"];

                        PromotionalContent.Add(promoContent);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return PromotionalContent;
        }

        internal static List<PromotionalContentDTO> GetPromotionalContentByID(int Id)
        {
            List<PromotionalContentDTO> PromotionalContent = new List<PromotionalContentDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPromotionalContentById]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter idPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    idPar.Direction = ParameterDirection.Input;
                    idPar.Value = Id;


                    SqlDataReader myData = cmd.ExecuteReader();


                    while (myData.Read())
                    {
                        PromotionalContentDTO promoContent = new PromotionalContentDTO();
                        promoContent.Id = (int)myData["ID"];
                        promoContent.Title = (string)myData["Title"];
                        promoContent.Name = (string)myData["Name"];
                        promoContent.Description = (string)myData["Description"];
                        promoContent.ThumbNailURL = (string)myData["ThumbNailURL"];
                        promoContent.URL = (string)myData["URL"];
                        promoContent.ActionType = (string)myData["ActionType"];
                        promoContent.EventStartDate = (DateTime)myData["EventStartDate"];
                        promoContent.EventEndDate = (DateTime)myData["EventEndDate"];
                        promoContent.ActionTypeID = (int)myData["Type"];
                        promoContent.Status = (int)myData["Status"];
                        promoContent.StartDate = (DateTime)myData["StartDate"];
                        promoContent.EndDate = (DateTime)myData["EndDate"];
                        promoContent.UpdateDate = (DateTime)myData["UpdateDate"];

                        PromotionalContent.Add(promoContent);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return PromotionalContent;
        }

        internal static PromotionalContentDTO AddPromotionalContent(PromotionalContentDTO promotionalContentDTO)
        {
            PromotionalContentDTO promoContent = new PromotionalContentDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddPromotionalContent]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter title = cmd.Parameters.Add("@Title", SqlDbType.VarChar, 100);
                    title.Direction = ParameterDirection.Input;
                    title.Value = promotionalContentDTO.Title;

                    SqlParameter name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    name.Direction = ParameterDirection.Input;
                    name.Value = promotionalContentDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1024);
                    description.Direction = ParameterDirection.Input;
                    description.Value = promotionalContentDTO.Description;

                    SqlParameter thumbNailURL = cmd.Parameters.Add("@ThumbNailURL", SqlDbType.VarChar, 1024);
                    thumbNailURL.Direction = ParameterDirection.Input;
                    thumbNailURL.Value = promotionalContentDTO.ThumbNailURL;

                    SqlParameter url = cmd.Parameters.Add("@URL", SqlDbType.VarChar, 1024);
                    url.Direction = ParameterDirection.Input;
                    url.Value = promotionalContentDTO.URL;

                    SqlParameter actionType = cmd.Parameters.Add("@Type", SqlDbType.Int);
                    actionType.Direction = ParameterDirection.Input;
                    actionType.Value = promotionalContentDTO.ActionTypeID;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    status.Direction = ParameterDirection.Input;
                    status.Value = promotionalContentDTO.Status;

                    SqlParameter startDate = cmd.Parameters.Add("@StartDate", SqlDbType.DateTime);
                    startDate.Direction = ParameterDirection.Input;
                    startDate.Value = promotionalContentDTO.StartDate;

                    SqlParameter endDate = cmd.Parameters.Add("@EndDate", SqlDbType.DateTime);
                    endDate.Direction = ParameterDirection.Input;
                    endDate.Value = promotionalContentDTO.EndDate;

                    SqlParameter eventStartDate = cmd.Parameters.Add("@EventStartDate", SqlDbType.DateTime);
                    eventStartDate.Direction = ParameterDirection.Input;
                    eventStartDate.Value = promotionalContentDTO.EventStartDate;

                    SqlParameter eventEndDate = cmd.Parameters.Add("@EventEndDate", SqlDbType.DateTime);
                    eventEndDate.Direction = ParameterDirection.Input;
                    eventEndDate.Value = promotionalContentDTO.EventEndDate;

                    SqlParameter userid = cmd.Parameters.Add("@userid", SqlDbType.Int);
                    userid.Direction = ParameterDirection.Input;
                    userid.Value = promotionalContentDTO.UserID;

                    SqlDataReader myData = cmd.ExecuteReader();


                    while (myData.Read())
                    {

                        promoContent.Id = (int)myData["ID"];
                        promoContent.Title = (string)myData["Title"];
                        promoContent.Name = (string)myData["Name"];
                        promoContent.Description = (string)myData["Description"];
                        promoContent.ThumbNailURL = (string)myData["ThumbNailURL"];
                        promoContent.URL = (string)myData["URL"];
                        promoContent.ActionType = (string)myData["ActionType"];
                        promoContent.EventStartDate = (DateTime)myData["EventStartDate"];
                        promoContent.EventEndDate = (DateTime)myData["EventEndDate"];
                        promoContent.ActionTypeID = (int)myData["Type"];
                        promoContent.Status = (int)myData["Status"];
                        promoContent.StartDate = (DateTime)myData["StartDate"];
                        promoContent.EndDate = (DateTime)myData["EndDate"];

                    }
                    myData.Close();
                    myConn.Close();


                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return promoContent;
        }

        internal static PromotionalContentDTO UpdatePromotionalContent(PromotionalContentDTO promotionalContentDTO)
        {
            PromotionalContentDTO promoContent = new PromotionalContentDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdatePromotionalContent]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter title = cmd.Parameters.Add("@Title", SqlDbType.VarChar, 100);
                    title.Direction = ParameterDirection.Input;
                    title.Value = promotionalContentDTO.Title;

                    SqlParameter name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    name.Direction = ParameterDirection.Input;
                    name.Value = promotionalContentDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1024);
                    description.Direction = ParameterDirection.Input;
                    description.Value = promotionalContentDTO.Description;

                    SqlParameter thumbNailURL = cmd.Parameters.Add("@ThumbNailURL", SqlDbType.VarChar, 1024);
                    thumbNailURL.Direction = ParameterDirection.Input;
                    thumbNailURL.Value = promotionalContentDTO.ThumbNailURL;

                    SqlParameter url = cmd.Parameters.Add("@URL", SqlDbType.VarChar, 1024);
                    url.Direction = ParameterDirection.Input;
                    url.Value = promotionalContentDTO.URL;

                    SqlParameter actionType = cmd.Parameters.Add("@Type", SqlDbType.Int);
                    actionType.Direction = ParameterDirection.Input;
                    actionType.Value = promotionalContentDTO.ActionTypeID;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    status.Direction = ParameterDirection.Input;
                    status.Value = promotionalContentDTO.Status;

                    SqlParameter startDate = cmd.Parameters.Add("@StartDate", SqlDbType.DateTime);
                    startDate.Direction = ParameterDirection.Input;
                    startDate.Value = promotionalContentDTO.StartDate;

                    SqlParameter endDate = cmd.Parameters.Add("@EndDate", SqlDbType.DateTime);
                    endDate.Direction = ParameterDirection.Input;
                    endDate.Value = promotionalContentDTO.EndDate;

                    SqlParameter eventStartDate = cmd.Parameters.Add("@EventStartDate", SqlDbType.DateTime);
                    eventStartDate.Direction = ParameterDirection.Input;
                    eventStartDate.Value = promotionalContentDTO.EventStartDate;

                    SqlParameter eventEndDate = cmd.Parameters.Add("@EventEndDate", SqlDbType.DateTime);
                    eventEndDate.Direction = ParameterDirection.Input;
                    eventEndDate.Value = promotionalContentDTO.EventEndDate;

                    SqlParameter userid = cmd.Parameters.Add("@userid", SqlDbType.Int);
                    userid.Direction = ParameterDirection.Input;
                    userid.Value = promotionalContentDTO.UserID;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Input;
                    Id.Value = promotionalContentDTO.Id;

                    SqlDataReader myData = cmd.ExecuteReader();


                    while (myData.Read())
                    {

                        promoContent.Id = (int)myData["ID"];
                        promoContent.Title = (string)myData["Title"];
                        promoContent.Name = (string)myData["Name"];
                        promoContent.Description = (string)myData["Description"];
                        promoContent.ThumbNailURL = (string)myData["ThumbNailURL"];
                        promoContent.URL = (string)myData["URL"];
                        promoContent.ActionType = (string)myData["ActionType"];
                        promoContent.EventStartDate = (DateTime)myData["EventStartDate"];
                        promoContent.EventEndDate = (DateTime)myData["EventEndDate"];
                        promoContent.ActionTypeID = (int)myData["Type"];
                        promoContent.Status = (int)myData["Status"];
                        promoContent.StartDate = (DateTime)myData["StartDate"];
                        promoContent.EndDate = (DateTime)myData["EndDate"];

                    }
                    myData.Close();
                    myConn.Close();


                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return promoContent;
        }

        internal static void UpdatePromotionalStatus(string PromotionId, int TypeId)
        {


            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdatePromotionStatus]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter promotionId = cmd.Parameters.Add("@PromotionId", SqlDbType.VarChar, 100);
                    promotionId.Direction = ParameterDirection.Input;
                    promotionId.Value = PromotionId;

                    SqlParameter type = cmd.Parameters.Add("@type", SqlDbType.Int);
                    type.Direction = ParameterDirection.Input;
                    type.Value = TypeId;

                    cmd.ExecuteNonQuery();

                    myConn.Close();


                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

        }

        internal static void DeletePromotionalContent(int promotionalID, int UserId)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();


                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeletePromotionalContent]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter ID = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    ID.Direction = ParameterDirection.Input;
                    ID.Value = promotionalID;

                    SqlParameter userId = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    userId.Direction = ParameterDirection.Input;
                    userId.Value = UserId;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static int AddPayment(PaymentDto paymentDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddPayment]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter userId = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    userId.Direction = ParameterDirection.Input;
                    userId.Value = paymentDto.UserID;

                    SqlParameter paymentToken = cmd.Parameters.Add("@PaymentToken", SqlDbType.VarChar, 4000);
                    paymentToken.Direction = ParameterDirection.Input;
                    paymentToken.Value = paymentDto.PaymentToken;

                    SqlParameter deviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.Int);
                    deviceType.Direction = ParameterDirection.Input;
                    deviceType.Value = paymentDto.DeviceType;

                    SqlParameter paymentType = cmd.Parameters.Add("@PaymentType", SqlDbType.VarChar, 50);
                    paymentType.Direction = ParameterDirection.Input;
                    paymentType.Value = paymentDto.PaymentType;

                    SqlParameter videoId = cmd.Parameters.Add("@VideoId", SqlDbType.VarChar, 100);
                    videoId.Direction = ParameterDirection.Input;
                    videoId.Value = (paymentDto.VideoId == null) ? 0 : paymentDto.VideoId;

                    SqlParameter planType = cmd.Parameters.Add("@PlanType", SqlDbType.Int);
                    planType.Direction = ParameterDirection.Input;
                    planType.Value = (paymentDto.PlanType == null) ? 0 : paymentDto.PlanType;


                    SqlDataReader myData = cmd.ExecuteReader();

                    //  cmd.ExecuteNonQuery();
                    while (myData.Read())
                    {
                        var retval = myData["ID"];
                        id = Convert.ToInt32(retval);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static PaymentDto GetPayment(int userId)
        {
            PaymentDto payments = new PaymentDto();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPayment]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter userIdpar = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    userIdpar.Direction = ParameterDirection.Input;
                    userIdpar.Value = userId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        payments.Id = (int)myData["ID"];
                        payments.VideoId = (int)myData["VideoId"];
                        payments.PaymentToken = (string)myData["PaymentToken"];
                        payments.DeviceType = (int)myData["DeviceType"];
                        payments.PaymentType = (string)myData["PaymentType"];
                        payments.ExpiryDate = (DateTime)myData["ExpiryDate"];
                        payments.PlanType = (int)myData["PlanType"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return payments;
        }


        internal static List<VideosInfoDTO> GetPurchaseVideos(int userId)
        {
            List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPurchaseVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    usrid.Direction = ParameterDirection.Input;
                    usrid.Value = userId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideosInfoDTO v = new VideosInfoDTO();
                        v.ID = (int)myData["ID"];
                        v.Segment = (string)myData["segment"];
                        v.Category = (string)myData["category"];
                        v.SubCategory = (string)myData["subcategory"];
                        v.MovementName = (string)myData["movementname"];
                        v.ActorName = (string)myData["actorname"];
                        v.Orientation = (int)myData["orientation"];
                        v.Facing = (int)myData["facing"];
                        int rate = (int)myData["averagerating"];
                        v.Rating = (float)(rate / 20.0);
                        v.URL = (string)myData["url"];
                        v.OwnerID = (int)myData["ownerId"];
                        v.ThumbNailURL = (string)myData["thumbnailurl"];
                        v.PlayCount = (int)myData["PlayCount"];
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videos;
        }


        #region Push Notification
        static void DeviceSubscriptionChanged(object sender,
              string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            //Do something here
        }

        //this even raised when a notification is successfully sent
        static void NotificationSent(object sender, INotification notification)
        {
            //Do something here
        }

        //this is raised when a notification is failed due to some reason
        static void NotificationFailed(object sender,
        INotification notification, Exception notificationFailureException)
        {
            //Do something here
        }

        //this is fired when there is exception is raised by the channel
        static void ChannelException
            (object sender, IPushChannel channel, Exception exception)
        {
            //Do something here
        }

        //this is fired when there is exception is raised by the service
        static void ServiceException(object sender, Exception exception)
        {
            //Do something here
        }

        //this is raised when the particular device subscription is expired
        static void DeviceSubscriptionExpired(object sender,
        string expiredDeviceSubscriptionId,
            DateTime timestamp, INotification notification)
        {
            //Do something here
        }

        //this is raised when the channel is destroyed
        static void ChannelDestroyed(object sender)
        {
            //Do something here
        }

        //this is raised when the channel is created
        static void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            //Do something here
        }

        internal static void AddPushNotifications(string message, int badge)
        {
            //create the puchbroker object
            var push = new PushBroker();
            //Wire up the events for all the services that the broker registers
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push.OnChannelCreated += ChannelCreated;
            push.OnChannelDestroyed += ChannelDestroyed;

            try
            {


                var path = HttpContext.Current.Server.MapPath("~/iKKOSAdminPushCertificates.p12");

                var appleCert = System.IO.File.ReadAllBytes(path);
                //IMPORTANT: If you are using a Development provisioning Profile, you must use
                // the Sandbox push notification server 
                //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as
                // 'false')
                //  If you are using an AdHoc or AppStore provisioning profile, you must use the 
                //Production push notification server
                //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 
                //'true')

                var settings = new PushServiceSettings();
                settings.AutoScaleChannels = false;
                settings.Channels = 1;
                settings.MaxAutoScaleChannels = 1;

                push.RegisterAppleService(new ApplePushChannelSettings(true, appleCert, "123456Aa"));
                //Extension method
                //Fluent construction of an iOS notification
                //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets
                // generated within your iOS app itself when the Application Delegate
                //  for registered for remote notifications is called, 
                // and the device token is passed back to you
                push.QueueNotification(new AppleNotification()
                                            .ForDeviceToken("546e9a4b9a3a135f8fad07d168523ea3f99189dd79d96682fa260d3d8e9192f3")//the recipient device id
                                            .WithAlert(message)//the message
                                            .WithBadge(badge)
                                            );

                push.StopAllServices(waitForQueuesToFinish: true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Training
        //internal static List<BestTimeDTO> GetUserPernalizationData(int userid, int strokeTypeid)
        //{
        //    List<BestTimeDTO> bestTimeDTO = new List<BestTimeDTO>();
        //    try
        //    {
        //        SqlConnection myConn = ConnectTODB();

        //        if (null != myConn)
        //        {
        //            SqlCommand cmd = new SqlCommand("[GetUserPernalizationData]", myConn);
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
        //            UserID.Direction = ParameterDirection.Input;
        //            UserID.Value = userid;

        //            SqlParameter Stroke_Type_Id = cmd.Parameters.Add("@Stroke_Type_Id", SqlDbType.Int);
        //            Stroke_Type_Id.Direction = ParameterDirection.Input;
        //            Stroke_Type_Id.Value = strokeTypeid;

        //            SqlDataReader myData = cmd.ExecuteReader();

        //            while (myData.Read())
        //            {
        //                BestTimeDTO v = new BestTimeDTO();
        //                v.BestTime1 = myData["BestTime1"].ToString();
        //                v.BestTime2 = myData["BestTime2"].ToString();
        //                //v.age = (int) myData["age"];
        //                //v.gender = (int)myData["gender"];
        //                bestTimeDTO.Add(v);
        //            }

        //            myData.Close();
        //            myConn.Close();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        string s = e.Message;
        //        throw;
        //    }

        //    return bestTimeDTO;
        //}


        internal static int AddUserPersonalization(PersonalizationDTO personalizationDTO)
        {
            int Id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddUserPersonalization]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Event_Type_Id = cmd.Parameters.Add("@Event_Type_Id", SqlDbType.Int);
                    Event_Type_Id.Direction = ParameterDirection.Input;
                    Event_Type_Id.Value = personalizationDTO.EventTypeId;

                    SqlParameter BestTime1 = cmd.Parameters.Add("@BestTime1", SqlDbType.Int);
                    BestTime1.Direction = ParameterDirection.Input;
                    BestTime1.Value = personalizationDTO.BestTime1;

                    SqlParameter BestTime2 = cmd.Parameters.Add("@BestTime2", SqlDbType.Int);
                    BestTime2.Direction = ParameterDirection.Input;
                    BestTime2.Value = 0;

                    //SqlParameter Stroke1 = cmd.Parameters.Add("@Stroke1", SqlDbType.VarChar, 50);
                    //Stroke1.Direction = ParameterDirection.Input;
                    //Stroke1.Value = personalizationDTO.Stroke1;

                    //SqlParameter Stroke2 = cmd.Parameters.Add("@Stroke2", SqlDbType.VarChar, 50);
                    //Stroke2.Direction = ParameterDirection.Input;
                    //Stroke2.Value = personalizationDTO.Stroke2;

                    SqlParameter working_with_coach = cmd.Parameters.Add("@working_with_coach", SqlDbType.Bit);
                    working_with_coach.Direction = ParameterDirection.Input;
                    working_with_coach.Value = personalizationDTO.WorkingWithCoach;

                    SqlParameter user_id = cmd.Parameters.Add("@user_id", SqlDbType.Int);
                    user_id.Direction = ParameterDirection.Input;
                    user_id.Value = personalizationDTO.UserId;

                    UserInfoDTO userinfo = GetUserAgeGender(personalizationDTO.UserId);

                    SqlParameter age = cmd.Parameters.Add("@age", SqlDbType.Int);
                    age.Direction = ParameterDirection.Input;
                    age.Value = userinfo.Age;

                    SqlParameter gender = cmd.Parameters.Add("@gender", SqlDbType.Int);
                    gender.Direction = ParameterDirection.Input;
                    gender.Value = userinfo.Gender;


                    Id = Convert.ToInt32(cmd.ExecuteScalar());


                    myConn.Close();

                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Id;
        }


        //internal static int UpdateUserPersonalization(PersonalizationDTO personalizationDTO)
        //{
        //    int Id = 0;
        //    try
        //    {
        //        SqlConnection myConn = ConnectTODB();

        //        if (null != myConn)
        //        {
        //            SqlCommand cmd = new SqlCommand("[UpdateUserPersonalization]", myConn);
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            SqlParameter Event_Type_Id = cmd.Parameters.Add("@Event_Type_Id", SqlDbType.Int);
        //            Event_Type_Id.Direction = ParameterDirection.Input;
        //            Event_Type_Id.Value = personalizationDTO.EventTypeId;

        //            SqlParameter BestTime1 = cmd.Parameters.Add("@BestTime1", SqlDbType.Int);
        //            BestTime1.Direction = ParameterDirection.Input;
        //            BestTime1.Value = personalizationDTO.BestTime1;

        //            SqlParameter BestTime2 = cmd.Parameters.Add("@BestTime2", SqlDbType.Int);
        //            BestTime2.Direction = ParameterDirection.Input;
        //            BestTime2.Value = 0;

        //            //SqlParameter Stroke1 = cmd.Parameters.Add("@Stroke1", SqlDbType.VarChar, 50);
        //            //Stroke1.Direction = ParameterDirection.Input;
        //            //Stroke1.Value = personalizationDTO.Stroke1;

        //            //SqlParameter Stroke2 = cmd.Parameters.Add("@Stroke2", SqlDbType.VarChar, 50);
        //            //Stroke2.Direction = ParameterDirection.Input;
        //            //Stroke2.Value = personalizationDTO.Stroke2;

        //            SqlParameter working_with_coach = cmd.Parameters.Add("@working_with_coach", SqlDbType.Bit);
        //            working_with_coach.Direction = ParameterDirection.Input;
        //            working_with_coach.Value = personalizationDTO.WorkingWithCoach;


        //            UserInfoDTO userinfo = GetUserAgeGender(personalizationDTO.UserId);

        //            SqlParameter age = cmd.Parameters.Add("@age", SqlDbType.Int);
        //            age.Direction = ParameterDirection.Input;
        //            age.Value = userinfo.Age;

        //            SqlParameter gender = cmd.Parameters.Add("@gender", SqlDbType.Int);
        //            gender.Direction = ParameterDirection.Input;
        //            gender.Value = userinfo.Gender;


        //            SqlDataReader myData = cmd.ExecuteReader();

        //            while (myData.Read())
        //            {

        //                Id = (int)myData["TrainingTemplateID"];
        //                //v.Text = myData["Text"].ToString();
        //                //v.UserId = (int)myData["user_id"];
        //                //v.ShowQuestion = (bool)myData["ShowQuestion"];
        //                //v.Question = myData["Text"].ToString();
        //                //v.ShowRecord = (bool)myData["ShowRecord"];
        //                //v.RecordedVideoId = (int)myData["recorded_video_id"];
        //                //v.ShowResults = (bool)myData["ShowResults"];
        //                //v.started = (bool)myData["started"];
        //                //v.QuestionAns = myData["QuestionAns"].ToString();
        //                //v.IdealVideoId = (int)myData["IdealVideoId"];

        //            }

        //            myConn.Close();
        //            myData.Close();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        string s = e.Message;
        //        throw;
        //    }

        //    return Id;
        //}

        internal static void UpdateUserTrainingStartDate(UserTrainingPlanDTO userTrainingPlanDTO)
        {

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateUserTrainingStartDate]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter TrainingPlanId = cmd.Parameters.Add("@TrainingPlanId", SqlDbType.Int);
                    TrainingPlanId.Direction = ParameterDirection.Input;
                    TrainingPlanId.Value = userTrainingPlanDTO.TrainingPlanId;

                    SqlParameter Date = cmd.Parameters.Add("@Date", SqlDbType.DateTime);
                    Date.Direction = ParameterDirection.Input;
                    Date.Value = userTrainingPlanDTO.StartDate;

                    SqlParameter user_id = cmd.Parameters.Add("@user_id", SqlDbType.Int);
                    user_id.Direction = ParameterDirection.Input;
                    user_id.Value = userTrainingPlanDTO.UserId;

                    cmd.ExecuteNonQuery();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }


        }
        internal static void UpdateQuestionAnswer(UserTrainingPlanDTO userTrainingPlanDTO)
        {

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateQuestionAnswer]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter training_plan_id = cmd.Parameters.Add("@training_plan_id", SqlDbType.Int);
                    training_plan_id.Direction = ParameterDirection.Input;
                    training_plan_id.Value = userTrainingPlanDTO.TrainingPlanId;

                    SqlParameter question_id = cmd.Parameters.Add("@question", SqlDbType.VarChar, 2000);
                    question_id.Direction = ParameterDirection.Input;
                    question_id.Value = userTrainingPlanDTO.Question;

                    SqlParameter answer = cmd.Parameters.Add("@answer", SqlDbType.VarChar, 50);
                    answer.Direction = ParameterDirection.Input;
                    answer.Value = userTrainingPlanDTO.QuestionAns;

                    SqlParameter user_id = cmd.Parameters.Add("@user_id", SqlDbType.Int);
                    user_id.Direction = ParameterDirection.Input;
                    user_id.Value = userTrainingPlanDTO.UserId;

                    cmd.ExecuteNonQuery();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }


        }

        internal static int AddUserTrainingPlan(UserTrainingPlanDTO userTrainingPlanDTO)
        {
            UserTrainingPlanDTO v = new UserTrainingPlanDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddUserTrainingPlan]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter training_plan_id = cmd.Parameters.Add("@training_plan_id", SqlDbType.Int);
                    training_plan_id.Direction = ParameterDirection.Input;
                    training_plan_id.Value = userTrainingPlanDTO.TrainingPlanId;

                    SqlParameter user_id = cmd.Parameters.Add("@user_id", SqlDbType.Int);
                    user_id.Direction = ParameterDirection.Input;
                    user_id.Value = userTrainingPlanDTO.UserId;

                    SqlParameter start_date = cmd.Parameters.Add("@start_date", SqlDbType.DateTime);
                    start_date.Direction = ParameterDirection.Input;
                    start_date.Value = userTrainingPlanDTO.StartDate;

                    SqlParameter Best_current_time1 = cmd.Parameters.Add("@Best_current_time1", SqlDbType.Time);
                    Best_current_time1.Direction = ParameterDirection.Input;
                    Best_current_time1.Value = userTrainingPlanDTO.BestCurrentTime1;

                    SqlParameter Best_current_time2 = cmd.Parameters.Add("@Best_current_time2", SqlDbType.Time);
                    Best_current_time2.Direction = ParameterDirection.Input;
                    Best_current_time2.Value = userTrainingPlanDTO.BestCurrentTime2;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v.Id;
        }


        internal static int UpdateUserTrainingPlan(UserTrainingPlanDTO userTrainingPlanDTO)
        {
            UserTrainingPlanDTO v = new UserTrainingPlanDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateUserTrainingPlan]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter training_plan_id = cmd.Parameters.Add("@training_plan_id", SqlDbType.Int);
                    training_plan_id.Direction = ParameterDirection.Input;
                    training_plan_id.Value = userTrainingPlanDTO.TrainingPlanId;

                    SqlParameter user_id = cmd.Parameters.Add("@user_id", SqlDbType.Int);
                    user_id.Direction = ParameterDirection.Input;
                    user_id.Value = userTrainingPlanDTO.UserId;

                    SqlParameter start_date = cmd.Parameters.Add("@start_date", SqlDbType.DateTime);
                    start_date.Direction = ParameterDirection.Input;
                    start_date.Value = userTrainingPlanDTO.StartDate;

                    SqlParameter Best_current_time1 = cmd.Parameters.Add("@Best_current_time1", SqlDbType.Time);
                    Best_current_time1.Direction = ParameterDirection.Input;
                    Best_current_time1.Value = userTrainingPlanDTO.BestCurrentTime1;

                    SqlParameter Best_current_time2 = cmd.Parameters.Add("@Best_current_time2", SqlDbType.Time);
                    Best_current_time2.Direction = ParameterDirection.Input;
                    Best_current_time2.Value = userTrainingPlanDTO.BestCurrentTime2;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Input;
                    Id.Value = userTrainingPlanDTO.Id;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v.Id;
        }

        internal static int AddTrainingPlan(TrainingPlanDTO trainingPlanDTO)
        {
            TrainingPlanDTO v = new TrainingPlanDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddTrainingPlan]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter age = cmd.Parameters.Add("@age", SqlDbType.Int);
                    age.Direction = ParameterDirection.Input;
                    age.Value = trainingPlanDTO.Age;

                    SqlParameter gender = cmd.Parameters.Add("@gender", SqlDbType.Int);
                    gender.Direction = ParameterDirection.Input;
                    gender.Value = trainingPlanDTO.Gender;

                    SqlParameter Event_Type_Name = cmd.Parameters.Add("@Event_Type_Id", SqlDbType.Int);
                    Event_Type_Name.Direction = ParameterDirection.Input;
                    Event_Type_Name.Value = trainingPlanDTO.EventId;

                    SqlParameter Besttime1 = cmd.Parameters.Add("@Besttime1", SqlDbType.Int);
                    Besttime1.Direction = ParameterDirection.Input;
                    Besttime1.Value = trainingPlanDTO.BestTime1;

                    SqlParameter Besttime2 = cmd.Parameters.Add("@Besttime2", SqlDbType.Int);
                    Besttime2.Direction = ParameterDirection.Input;
                    Besttime2.Value = trainingPlanDTO.BestTime2;

                    SqlParameter Text = cmd.Parameters.Add("@Text", SqlDbType.VarChar, 4000);
                    Text.Direction = ParameterDirection.Input;
                    Text.Value = trainingPlanDTO.Text;

                    SqlParameter user_id = cmd.Parameters.Add("@userid", SqlDbType.Int);
                    user_id.Direction = ParameterDirection.Input;
                    user_id.Value = trainingPlanDTO.UserId;

                    SqlParameter default1 = cmd.Parameters.Add("@default", SqlDbType.Int);
                    default1.Direction = ParameterDirection.Input;
                    default1.Value = trainingPlanDTO.Default;

                    SqlParameter Dayid = cmd.Parameters.Add("@Dayid", SqlDbType.Int);
                    Dayid.Direction = ParameterDirection.Input;
                    Dayid.Value = trainingPlanDTO.Day;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v.Id;
        }


        internal static int UpdateTrainingPlan(TrainingPlanDTO trainingPlanDTO)
        {
            TrainingPlanDTO v = new TrainingPlanDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateTrainingPlan]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    //SqlParameter age = cmd.Parameters.Add("@age", SqlDbType.Int);
                    //age.Direction = ParameterDirection.Input;
                    //age.Value = trainingPlanDTO.Age;

                    //SqlParameter gender = cmd.Parameters.Add("@gender", SqlDbType.Int);
                    //gender.Direction = ParameterDirection.Input;
                    //gender.Value = trainingPlanDTO.Gender;

                    //SqlParameter Event_Type_Name = cmd.Parameters.Add("@Event_Type_Name", SqlDbType.VarChar, 50);
                    //Event_Type_Name.Direction = ParameterDirection.Input;
                    //Event_Type_Name.Value = trainingPlanDTO.EventName;

                    //SqlParameter Besttime1 = cmd.Parameters.Add("@Besttime1", SqlDbType.Int);
                    //Besttime1.Direction = ParameterDirection.Input;
                    //Besttime1.Value = trainingPlanDTO.BestTime1;

                    //SqlParameter Besttime2 = cmd.Parameters.Add("@Besttime2", SqlDbType.Int);
                    //Besttime2.Direction = ParameterDirection.Input;
                    //Besttime2.Value = trainingPlanDTO.BestTime2;

                    SqlParameter Text = cmd.Parameters.Add("@Text", SqlDbType.VarChar, 4000);
                    Text.Direction = ParameterDirection.Input;
                    Text.Value = trainingPlanDTO.Text;

                    //SqlParameter user_id = cmd.Parameters.Add("@userid", SqlDbType.Int);
                    //user_id.Direction = ParameterDirection.Input;
                    //user_id.Value = trainingPlanDTO.UserId;

                    //SqlParameter default1 = cmd.Parameters.Add("@default", SqlDbType.Int);
                    //default1.Direction = ParameterDirection.Input;
                    //default1.Value = trainingPlanDTO.Default;

                    //SqlParameter Dayid = cmd.Parameters.Add("@Dayid", SqlDbType.Int);
                    //Dayid.Direction = ParameterDirection.Input;
                    //Dayid.Value = trainingPlanDTO.Day;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Input;
                    Id.Value = trainingPlanDTO.Id;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v.Id;
        }


        internal static int AddTrainingVideo(TrainingVideoDTO trainingVideoDTO)
        {
            TrainingVideoDTO v = new TrainingVideoDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddTrainingVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter PlanId = cmd.Parameters.Add("@PlanId", SqlDbType.Int);
                    PlanId.Direction = ParameterDirection.Input;
                    PlanId.Value = trainingVideoDTO.PlanId;

                    SqlParameter VideoId = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    VideoId.Direction = ParameterDirection.Input;
                    VideoId.Value = trainingVideoDTO.VideoId;

                    SqlParameter UserId = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    UserId.Direction = ParameterDirection.Input;
                    UserId.Value = trainingVideoDTO.UserId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v.Id;
        }

        //internal static int UpdateTrainingVideo(TrainingVideoDTO trainingVideoDTO)
        //{
        //    TrainingVideoDTO v = new TrainingVideoDTO();
        //    try
        //    {
        //        SqlConnection myConn = ConnectTODB();

        //        if (null != myConn)
        //        {
        //            SqlCommand cmd = new SqlCommand("[UpdateTrainingPlan]", myConn);
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            SqlParameter PlanId = cmd.Parameters.Add("@PlanId", SqlDbType.Int);
        //            PlanId.Direction = ParameterDirection.Input;
        //            PlanId.Value = trainingVideoDTO.PlanId;

        //            SqlParameter VideoId = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
        //            VideoId.Direction = ParameterDirection.Input;
        //            VideoId.Value = trainingVideoDTO.VideoId;

        //            SqlParameter UserId = cmd.Parameters.Add("@UserId", SqlDbType.Int);
        //            UserId.Direction = ParameterDirection.Input;
        //            UserId.Value = trainingVideoDTO.UserId;

        //            SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
        //            Id.Direction = ParameterDirection.Input;
        //            Id.Value = trainingVideoDTO.Id;

        //            SqlDataReader myData = cmd.ExecuteReader();

        //            while (myData.Read())
        //            {

        //                v.Id = (int)myData["ID"];

        //            }

        //            myConn.Close();
        //            myData.Close();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        string s = e.Message;
        //        throw;
        //    }

        //    return v.Id;
        //}


        internal static void DeleteTrainingVideos(int PlanId)
        {

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteTrainingVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter planId = cmd.Parameters.Add("@PlanId", SqlDbType.Int);
                    planId.Direction = ParameterDirection.Input;
                    planId.Value = PlanId;

                    cmd.ExecuteNonQuery();


                    myConn.Close();

                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }


        }
        //internal static int UploadRecordedVideo(TrainingRecordedVideosDTO trainingRecordedVideosDTO)
        //{
        //    int recordedvideoid = 0;
        //    int traningRecordVideoId = 0;
        //    SqlConnection conn = ConnectTODB();
        //    SqlTransaction transaction;
        //    transaction = conn.BeginTransaction();
        //    SqlCommand command = conn.CreateCommand();
        //    command.Transaction = transaction;
        //    try
        //    {

        //        recordedvideoid = AdminAddVideo(trainingRecordedVideosDTO);
        //        trainingRecordedVideosDTO.RecordedVideoId = recordedvideoid;
        //        trainingRecordedVideosDTO.TrainingVideoId = 3010; //Get the video Id depending on the segment , category, SUbacategory sent by user -- TODO 
        //        traningRecordVideoId =  AddRecordedVideos(trainingRecordedVideosDTO);
        //        transaction.Commit();
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.Rollback();
        //    }

        //    return traningRecordVideoId;
        //}


        internal static int AddRecordedVideos(TrainingRecordedVideosDTO trainingRecordedVideosDTO)
        {
            try
            {
                int vid = 0;
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddTrainingRecordedVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@user_id", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = trainingRecordedVideosDTO.UserId;

                    SqlParameter training_video_id = cmd.Parameters.Add("@training_video_id", SqlDbType.Int);
                    training_video_id.Direction = ParameterDirection.Input;
                    training_video_id.Value = trainingRecordedVideosDTO.TrainingVideoId;

                    SqlParameter recorded_video_id = cmd.Parameters.Add("@recorded_video_id", SqlDbType.Int);
                    recorded_video_id.Direction = ParameterDirection.Input;
                    recorded_video_id.Value = trainingRecordedVideosDTO.RecordedVideoId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        vid = (int)myData["ID"];
                    }

                    myConn.Close();
                }

                return vid;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static UserTrainingPlanDTO GetUserTrainingPlanDetails(int TrainingTemplateID, int UserId)
        {
            UserTrainingPlanDTO v = new UserTrainingPlanDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserTrainingPlanDetails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter trainingTemplateId = cmd.Parameters.Add("@trainingTemplateId", SqlDbType.Int);
                    trainingTemplateId.Direction = ParameterDirection.Input;
                    trainingTemplateId.Value = TrainingTemplateID;


                    SqlParameter userid = cmd.Parameters.Add("@Userid", SqlDbType.Int);
                    userid.Direction = ParameterDirection.Input;
                    userid.Value = UserId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {


                        v.TrainingPlanId = (int)myData["Id"];
                        v.Text = myData["Text"].ToString();
                        v.UserId = (int)myData["user_id"];
                        v.ShowQuestion = (bool)myData["ShowQuestion"];
                        v.Question = myData["Text"].ToString();
                        v.ShowRecord = (bool)myData["ShowRecord"];
                        v.RecordedVideoId = (int)myData["recorded_video_id"];
                        v.ShowResults = (bool)myData["ShowResults"];
                        v.started = (bool)myData["started"];
                        v.QuestionAns = myData["QuestionAns"].ToString();
                        v.IdealVideoId = (int)myData["IdealVideoId"];
                        v.StartDate = (DateTime)myData["start_date"];
                        v.BestCurrentTime1 = (TimeSpan)myData["Best_current_time1"];
                        v.BestCurrentTime2 = (TimeSpan)myData["Best_current_time2"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }


        internal static List<VideosInfoDTO> GetTrainingPlanVideos(int PlanId)
        {
            List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetTrainingPlanVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter TrainingPlanId = cmd.Parameters.Add("@TrainingPlanId", SqlDbType.Int);
                    TrainingPlanId.Direction = ParameterDirection.Input;
                    TrainingPlanId.Value = PlanId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideosInfoDTO v = new VideosInfoDTO();
                        v.ID = (int)myData["ID"];
                        v.Segment = (string)myData["segment"];
                        v.Category = (string)myData["category"];
                        v.SubCategory = (string)myData["subcategory"];
                        v.MovementName = (string)myData["movementname"];
                        v.ActorName = (string)myData["actorname"];
                        v.Orientation = (int)myData["orientation"];
                        v.Facing = (int)myData["facing"];
                        int rate = (int)myData["averagerating"];
                        v.Rating = (float)(rate / 20.0);
                        v.URL = (string)myData["url"];
                        v.OwnerID = (int)myData["ownerId"];
                        v.ThumbNailURL = (string)myData["thumbnailurl"];
                        v.PlayCount = (int)myData["PlayCount"];

                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videos;
        }

        internal static List<EventTypeDTO> GetEventTypes(int age, int gender)
        {
            List<EventTypeDTO> eventTypes = new List<EventTypeDTO>();
            List<string> Strokes = new List<string>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetEventTypes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter agepar = cmd.Parameters.Add("@age", SqlDbType.Int);
                    agepar.Direction = ParameterDirection.Input;
                    agepar.Value = age;

                    SqlParameter genderpar = cmd.Parameters.Add("@gender", SqlDbType.Int);
                    genderpar.Direction = ParameterDirection.Input;
                    genderpar.Value = gender;

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    DataTable dtEvents = ds.Tables[0];

                    foreach (DataRow dr in dtEvents.Rows)
                    {
                        EventTypeDTO v = new EventTypeDTO();
                        v.Id = (int)dr[0];
                        v.Value = dr[1].ToString();
                        v.Subcategory = dr[2].ToString();
                        eventTypes.Add(v);
                    }

                    da.Dispose();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return eventTypes;
        }

        internal static List<BestTimesValuesDTO> GetBestTimes(int eventId, int age, int gender)
        {
            List<BestTimesValuesDTO> BestTimes = new List<BestTimesValuesDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBestTimes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter eventid = cmd.Parameters.Add("@eventid", SqlDbType.Int);
                    eventid.Direction = ParameterDirection.Input;
                    eventid.Value = eventId;

                    SqlParameter agepar = cmd.Parameters.Add("@age", SqlDbType.Int);
                    agepar.Direction = ParameterDirection.Input;
                    agepar.Value = age;

                    SqlParameter genderpar = cmd.Parameters.Add("@gender", SqlDbType.Int);
                    genderpar.Direction = ParameterDirection.Input;
                    genderpar.Value = gender;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        BestTimesValuesDTO v = new BestTimesValuesDTO();
                        v.BestTimeId = (int)myData["ID"];
                        v.Stroke = myData["Stroke"].ToString();
                        v.BestTimeValue = myData["Value"].ToString();
                        BestTimes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return BestTimes;
        }

        //internal static List<List<BestTimesValuesDTO>> GetBestTimesValues(PlanDTO planDTO)
        //{
        //    List<List<BestTimesValuesDTO>> BestTimes = new List<List<BestTimesValuesDTO>>();
        //    List<BestTimesValuesDTO> IndBestTimes_100Y = new List<BestTimesValuesDTO>();
        //    List<BestTimesValuesDTO> IndBestTimes_200Y = new List<BestTimesValuesDTO>();

        //    try
        //    {
        //        SqlConnection myConn = ConnectTODB();

        //        if (null != myConn)
        //        {
        //            SqlCommand cmd = new SqlCommand("[GetBestTimes]", myConn);
        //            cmd.CommandType = CommandType.StoredProcedure;


        //            SqlParameter Stroke_Type_Id = cmd.Parameters.Add("@Stroke_Type_Id", SqlDbType.VarChar, 100);
        //            Stroke_Type_Id.Direction = ParameterDirection.Input;
        //            Stroke_Type_Id.Value = GetStringFromArray(planDTO.Strokes.ToArray());


        //            SqlParameter Age = cmd.Parameters.Add("@age", SqlDbType.VarChar, 100);
        //            Age.Direction = ParameterDirection.Input;
        //            Age.Value = GetStringFromArrayStr(planDTO.AgeGroup.ToArray());


        //            SqlParameter Gender = cmd.Parameters.Add("@gender", SqlDbType.VarChar, 100);
        //            Gender.Direction = ParameterDirection.Input;
        //            Gender.Value = GetStringFromArray(planDTO.Gender.ToArray());

        //            SqlDataAdapter da = new SqlDataAdapter(cmd);
        //            DataSet ds = new DataSet();
        //            da.Fill(ds);

        //            DataTable dt100y = ds.Tables[0];
        //            DataTable dt200y = ds.Tables[1];



        //            //push each table data into list 
        //            foreach (DataRow dr in dt100y.Rows)
        //            {
        //                BestTimesValuesDTO objBestTime = new BestTimesValuesDTO { BestTimeId = (int)dr[0], BestTimeValue = dr[1].ToString() };
        //                IndBestTimes_100Y.Add(objBestTime);
        //            }
        //            BestTimes.Add(IndBestTimes_100Y);

        //            foreach (DataRow dr in dt200y.Rows)
        //            {
        //                BestTimesValuesDTO objBestTime = new BestTimesValuesDTO { BestTimeId = (int)dr[0], BestTimeValue = dr[1].ToString() };
        //                IndBestTimes_200Y.Add(objBestTime);
        //            }
        //            BestTimes.Add(IndBestTimes_200Y);





        //            //BestTimeDTO v = new BestTimeDTO();
        //            //v.BestTime_100Y = myData["100y_Best_Time"].ToString();
        //            //v.BestTime_100Y_Id = myData["100y_Best_Time_Id"].ToString();
        //            //v.BestTime_200Y = myData["200y_Best_Time"].ToString();
        //            //v.BestTime_200Y_Id = myData["200y_Best_Time_Id"].ToString();
        //            //BestTimes.Add(v);

        //            myConn.Close();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        string s = e.Message;
        //        throw;
        //    }

        //    return BestTimes;
        //}


        internal static string GetStringFromArray(int[] arr)
        {
            string str = string.Empty;
            for (int i = 0; i < arr.Length; i++)
            {
                if (str == string.Empty)
                {

                    str = arr[0].ToString();
                }
                else
                {
                    str = str + "," + arr[i].ToString();
                }


            }
            return str;
        }

        internal static string GetStringFromArrayStr(string[] arr)
        {
            string str = string.Empty;
            for (int i = 0; i < arr.Length; i++)
            {
                if (str == string.Empty)
                {

                    str = arr[0].ToString();
                }
                else
                {
                    str = str + "," + arr[i].ToString();
                }


            }
            return str;
        }
        internal static string GetStringFromArrayList(BestTimesValuesDTO[] arr)
        {
            string str = string.Empty;
            for (int i = 0; i < arr.Length; i++)
            {
                if (str == string.Empty)
                {

                    str = arr[0].BestTimeId.ToString();
                }
                else
                {
                    str = str + "," + arr[i].BestTimeId.ToString();
                }


            }
            return str;
        }

        internal static PlanDTO GetTrainingPlan(int Planid)
        {
            PlanDTO planDTO = new PlanDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetTrainingPlan]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter PlanId = cmd.Parameters.Add("@PlanId", SqlDbType.Int);
                    PlanId.Direction = ParameterDirection.Input;
                    PlanId.Value = Planid;


                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    DataTable dtPlanDet = ds.Tables[0];
                    DataTable dtVideoIds = ds.Tables[1];

                    List<int> VideoIds = new List<int>();

                    foreach (DataRow dr in dtVideoIds.Rows)
                    {
                        VideoIds.Add((int)dr["VideoId"]);
                    }

                    foreach (DataRow dr in dtPlanDet.Rows)
                    {
                        planDTO.Id = (int)dr["PlanId"];
                        planDTO.Text = dr["Text"].ToString();
                        planDTO.VideoIDs = VideoIds;
                        planDTO.Isdefault = (int)dr["default"];
                    }

                    da.Dispose();



                    //trainingPlanDTO.BestTime1 = myData["BestTime1"].ToString();
                    //trainingPlanDTO.BestTime2 = myData["BestTime2"].ToString();
                    //trainingPlanDTO.Stroke1 = myData["Stroke1"].ToString();
                    //trainingPlanDTO.Stroke2 = myData["Stroke2"].ToString();
                    //trainingPlanDTO.Gender = myData["gender"].ToString();
                    //trainingPlanDTO.Age = myData["age"].ToString();
                    //trainingPlanDTO.EventName = myData["EventName"].ToString();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return planDTO;
        }

        internal static List<PlanDaysDTO> GetTrainingPlanDays(TrainingPlanDTO planDTO)
        {
            List<PlanDaysDTO> planDaysDTO = new List<PlanDaysDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetTrainingPlanDays]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter EventName = cmd.Parameters.Add("@Event_Type_Id", SqlDbType.Int);
                    EventName.Direction = ParameterDirection.Input;
                    EventName.Value = planDTO.EventId;// GetStringFromArray(planDTO.Strokes.ToArray());


                    SqlParameter age = cmd.Parameters.Add("@age", SqlDbType.VarChar, 10);
                    age.Direction = ParameterDirection.Input;
                    age.Value = planDTO.Age;


                    SqlParameter gender = cmd.Parameters.Add("@gender", SqlDbType.VarChar, 10);
                    gender.Direction = ParameterDirection.Input;
                    gender.Value = planDTO.Gender;

                    SqlParameter BestTime1 = cmd.Parameters.Add("@BestTime1", SqlDbType.Int);
                    BestTime1.Direction = ParameterDirection.Input;
                    BestTime1.Value = planDTO.BestTime1;

                    SqlParameter BestTime2 = cmd.Parameters.Add("@BestTime2", SqlDbType.Int);
                    BestTime2.Direction = ParameterDirection.Input;
                    BestTime2.Value = planDTO.BestTime2;

                    //SqlParameter Gender = cmd.Parameters.Add("@gender", SqlDbType.VarChar, 100);
                    //Gender.Direction = ParameterDirection.Input;
                    //Gender.Value = GetStringFromArray(planDTO.Gender.ToArray());
                    //List<BestTimesValuesDTO> BestTimes_100y = planDTO.BestTimesValues[0];
                    //List<BestTimesValuesDTO> BestTimes_200y = planDTO.BestTimesValues[1];

                    //SqlParameter best_times_100y = cmd.Parameters.Add("@best_times_100y", SqlDbType.VarChar, 100);
                    //best_times_100y.Direction = ParameterDirection.Input;
                    //best_times_100y.Value = GetStringFromArrayList(BestTimes_100y.ToArray());

                    //SqlParameter best_times_200y = cmd.Parameters.Add("@best_times_200y", SqlDbType.VarChar, 100);
                    //best_times_200y.Direction = ParameterDirection.Input;
                    //best_times_200y.Value = GetStringFromArrayList(BestTimes_200y.ToArray());

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        PlanDaysDTO v = new PlanDaysDTO();
                        v.PlanId = (int)myData["ID"];
                        v.DayId = (int)myData["Day"];
                        planDaysDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return planDaysDTO;
        }

        internal static int GetIdealVideoId(string subcat)
        {
            int VideoId = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetIdealVideoId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter SubCategory = cmd.Parameters.Add("@SubCategory", SqlDbType.VarChar, 50);
                    SubCategory.Direction = ParameterDirection.Input;
                    SubCategory.Value = subcat;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoId = (int)myData["Id"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return VideoId;
        }
        internal static List<VideoURLDTO> GetRelavantVideosForSubcategory(string Subcategory)
        {
            List<VideoURLDTO> videos = new List<VideoURLDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVideosBySubcategory]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter eventName = cmd.Parameters.Add("@Subcategory", SqlDbType.VarChar, 50);
                    eventName.Direction = ParameterDirection.Input;
                    eventName.Value = Subcategory;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoURLDTO v = new VideoURLDTO();
                        v.Id = (int)myData["ID"];
                        //v.Segment = (string)myData["segment"];
                        //v.Category = (string)myData["category"];
                        //v.SubCategory = (string)myData["subcategory"];
                        v.MovementName = (string)myData["movementname"];
                        v.ActorName = (string)myData["actorname"];
                        //v.Orientation = (int)myData["orientation"];
                        //v.Facing = (int)myData["facing"];
                        //int rate = (int)myData["averagerating"];
                        //v.Rating = (float)(rate / 20.0);
                        v.VideoURL = (string)myData["url"];
                        //v.OwnerID = (int)myData["ownerId"];
                        //v.ThumbNailURL = (string)myData["thumbnailurl"];

                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videos;
        }


        internal static void UpdateUserTrainingPlanRecordId(TrainingRecordedVideosDTO trainingRecordedVideosDTO)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateTrainingPlanRecordedVideoId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter record_id = cmd.Parameters.Add("@record_id", SqlDbType.Int);
                    record_id.Direction = ParameterDirection.Input;
                    record_id.Value = trainingRecordedVideosDTO.RecordedVideoId;

                    SqlParameter user_id = cmd.Parameters.Add("@user_id", SqlDbType.Int);
                    user_id.Direction = ParameterDirection.Input;
                    user_id.Value = trainingRecordedVideosDTO.UserId;

                    SqlParameter training_plan_id = cmd.Parameters.Add("@training_plan_id", SqlDbType.Int);
                    training_plan_id.Direction = ParameterDirection.Input;
                    training_plan_id.Value = trainingRecordedVideosDTO.TrainingPlanId;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }


        internal static UserInfoDTO GetUserAgeGender(int UserId)
        {
            UserInfoDTO userinfo = new UserInfoDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAgeGenderForUser]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter userid = cmd.Parameters.Add("@Userid", SqlDbType.Int);
                    userid.Direction = ParameterDirection.Input;
                    userid.Value = UserId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        userinfo.Age = Convert.ToInt32(myData["age"]);
                        userinfo.Gender = Convert.ToInt32(myData["gender"]);

                    }
                    myData.Close();
                    myConn.Close();
                }
                return userinfo;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static int AddException(ExceptionDTO exceptionDTO)
        {
            TrainingVideoDTO v = new TrainingVideoDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddExceptionLogs]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter message = cmd.Parameters.Add("@Message", SqlDbType.VarChar, 4000);
                    message.Direction = ParameterDirection.Input;
                    message.Value = exceptionDTO.Message;

                    SqlParameter stacktrace = cmd.Parameters.Add("@Stacktrace", SqlDbType.VarChar, 4000);
                    stacktrace.Direction = ParameterDirection.Input;
                    stacktrace.Value = exceptionDTO.Stacktrace;

                    SqlParameter userid = cmd.Parameters.Add("@userid", SqlDbType.Int);
                    userid.Direction = ParameterDirection.Input;
                    userid.Value = exceptionDTO.userid;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v.Id;
        }
        #endregion

        #region YouTubeVideoDownLoad
        internal static int AddYouTubeUrl(string youTubeUrl)
        {

            int result = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddYouTubeUrl]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter url = cmd.Parameters.Add("@youtubeUrl", SqlDbType.VarChar, 500);
                    url.Direction = ParameterDirection.Input;
                    url.Value = youTubeUrl;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        result = Convert.ToInt32(myData["ID"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return result;
        }

        internal static void UpdateStorageVideo(int videoId, string videoURL, string videoName)
        {
            int result = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateStorageUrl]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter id = cmd.Parameters.Add("@ID", SqlDbType.Int);
                    id.Direction = ParameterDirection.Input;
                    id.Value = videoId;

                    SqlParameter url = cmd.Parameters.Add("@StorageUrl", SqlDbType.VarChar, 500);
                    url.Direction = ParameterDirection.Input;
                    url.Value = videoURL;

                    SqlParameter video = cmd.Parameters.Add("@VideoName", SqlDbType.VarChar, 50);
                    video.Direction = ParameterDirection.Input;
                    video.Value = videoName;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        result = Convert.ToInt32(myData["ID"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

        }

        internal static YouTubeUrl GetStorageVideoUrl(int videoId, string youTubeUrl)
        {
            string result = string.Empty;
            YouTubeUrl storageUrl = new YouTubeUrl();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetStorageUrl]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter id = cmd.Parameters.Add("@ID", SqlDbType.Int);
                    id.Direction = ParameterDirection.Input;
                    id.Value = videoId;

                    SqlParameter url = cmd.Parameters.Add("@youtubeUrl", SqlDbType.VarChar, 500);
                    url.Direction = ParameterDirection.Input;
                    url.Value = youTubeUrl;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        storageUrl.VideoId = (myData["ID"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["ID"]);
                        storageUrl.StorageUrl = (myData["StorageUrl"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["StorageUrl"]);
                        storageUrl.VideoName = (myData["videoId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["videoId"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return storageUrl;
        }


        internal static string GetVideoUrl()
        {
            string result = string.Empty;
           
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSampleVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    //SqlParameter id = cmd.Parameters.Add("@ID", SqlDbType.Int);
                    //id.Direction = ParameterDirection.Input;
                    //id.Value = videoId;

                    //SqlParameter url = cmd.Parameters.Add("@youtubeUrl", SqlDbType.VarChar, 500);
                    //url.Direction = ParameterDirection.Input;
                    //url.Value = youTubeUrl;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        result = (myData["VideoUrl"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VideoUrl"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return result;
        }

        #endregion

        #region WordPress
        internal static void AddCustomers(Customer Customer)
        {

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCustomersFromWP]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter email = cmd.Parameters.Add("@email", SqlDbType.VarChar, 50);
                    email.Direction = ParameterDirection.Input;
                    email.Value = Customer.email;

                    SqlParameter first_name = cmd.Parameters.Add("@first_name", SqlDbType.VarChar, 50);
                    first_name.Direction = ParameterDirection.Input;
                    first_name.Value = Customer.first_name;

                    SqlParameter last_name = cmd.Parameters.Add("@last_name", SqlDbType.VarChar, 50);
                    last_name.Direction = ParameterDirection.Input;
                    last_name.Value = Customer.last_name;

                    SqlParameter username = cmd.Parameters.Add("@username", SqlDbType.VarChar, 50);
                    username.Direction = ParameterDirection.Input;
                    username.Value = Customer.username;

                    SqlParameter customer_id = cmd.Parameters.Add("@customer_id", SqlDbType.Int);
                    customer_id.Direction = ParameterDirection.Input;
                    customer_id.Value = Customer.id;

                    cmd.ExecuteNonQuery();
                    myConn.Close();


                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

        }


        internal static string GetEmailByUserId(int UserId)
        {

            string email = string.Empty;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserEmailByID]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter userid = cmd.Parameters.Add("@userid", SqlDbType.Int);
                    userid.Direction = ParameterDirection.Input;
                    userid.Value = UserId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        email = myData["email"].ToString();


                    }
                    myData.Close();
                    myConn.Close();
                }
                return email;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }
        #endregion

        //public static bool ValidateServerCertificate(object sender,X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        //{
        //    if (sslPolicyErrors == SslPolicyErrors.None)
        //        return true;

        //    Console.WriteLine("Certificate error: {0}", sslPolicyErrors);

        //    // Do not allow this client to communicate with unauthenticated servers. 
        //    return false;
        //}


        internal static UsageTrackingDto UsersTracking()
        {
            UsageTrackingDto usage = new UsageTrackingDto();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[getuserscount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        usage.OneTimeUsersCount = (myData["UsersCount"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["UsersCount"]);
                        usage.UserVideosCount = (myData["VideosCount"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["VideosCount"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return usage;
        }


        internal static DisplayImageDTO GetDisplayImage(DisplayImageDTO Image)
        {
            DisplayImageDTO displayImage = new DisplayImageDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddDisplayImageStatus]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter PhoneID = cmd.Parameters.Add("@PhoneID", System.Data.SqlDbType.VarChar, 50);
                    PhoneID.Direction = System.Data.ParameterDirection.Input;
                    PhoneID.Value = Image.PhoneID;

                    SqlParameter Type = cmd.Parameters.Add("@Type", System.Data.SqlDbType.Int);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = Image.Type;

                    SqlParameter IsImgShown = cmd.Parameters.Add("@IsImageShown", System.Data.SqlDbType.Bit);
                    IsImgShown.Direction = System.Data.ParameterDirection.Input;
                    IsImgShown.Value = Image.IsImageShown;


                    SqlDataReader myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {
                        displayImage.ImageURL = (string)myData["DisplayImageURL"];
                        displayImage.IsImageShown = (myData["IsImageShown"] == DBNull.Value) ? false : Convert.ToBoolean(myData["IsImageShown"]);
                        
                        //displayImage.IsImageShown = false;


                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return displayImage;
        }
    }
}
