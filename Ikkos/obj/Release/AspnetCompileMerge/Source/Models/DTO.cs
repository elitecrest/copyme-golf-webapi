﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ikkos.Models
{
    public partial class ImageDTO
    {
        public int userid { get; set; }
        public string binary { get; set; }
    }

    public partial class FeedbackDTO
    {
        public string name { get; set; }
        public int question1 { get; set; }
        public int question2 { get; set; }
        public int question3 { get; set; }
        public string comment { get; set; }
    }

    public partial class UserInfoDTO
    {
        public UserInfoDTO()
        {
            this.ID = 0;
            this.Age = 0;
            this.ShardID = 0;
            this.CreatedBy = 0;
        }

        public int ID { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public int PhoneID { get; set; }
        public int OrganizationID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string UserSports { get; set; }
        public int Age { get; set; }
        public int Gender { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public int ShardID { get; set; }
        public int ExpYearsInSwimming { get; set; }
        public Boolean WorkingWithCoach { get; set; }
        public int SubCategoryId { get; set; }
        public string Level { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string FacebookId { get; set; }
        public string ProfilePicUrl { get; set; }
        public string Organization { get; set; }   //by lohit
    }

    public partial class AppInfoDTO
    {
        public AppInfoDTO()
        {
            this.AppID = "";
        }

        public string AppID { get; set; }
    }

    public partial class CategoryDTO
    {
        public CategoryDTO()
        {
            this.VideoCount = 0;
        }
        public int CategoryID { get; set; }
        public string Segment { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public int VideoCount { get; set; }
    }

    public partial class OrganizationDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public partial class UserSportDTO
    {
        public long UserSportsId { get; set; }
        public long UserId { get; set; }
        public int SportsId { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public UserInfoDTO UserInfo { get; set; }
    }

    public partial class VideosInfoDTO
    {
        public VideosInfoDTO()
        {
            this.ShardID = 0;
            this.SubCategoryID = 0;
            this.MovementName = "";
            this.URL = "";
            this.ThumbNailURL = "";
            this.Type = 1;
            this.Status = 1;
            this.Format = "";
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = 0;
            this.OwnerID = 0;
            this.Orientation = 0;
            this.Paid = 0;
        }

        public int ID { get; set; }
        public int SubCategoryID { get; set; }
        public string Segment { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string MovementName { get; set; }
        public string ActorName { get; set; }
        public int OwnerID { get; set; }
        public string Format { get; set; }
        public string Speed { get; set; }
        public string Quality { get; set; }
        public int? Size { get; set; }
        public int? Duration { get; set; }
        public string URL { get; set; }
        public string ThumbNailURL { get; set; }
        public string AccessKey { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public string ThumbnailString { get; set; }
        public string Base64String { get; set; }
        public string ThumbnailFormat { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public float Rating { get; set; }
        public int Orientation { get; set; }
        public int ShardID { get; set; }
        public int Facing { get; set; }
        public int AverageRating { get; set; }
        public int Paid { get; set; }
        public int PlayCount { get; set; }
        public bool isPurchased { get; set; }
        public int VideoMode { get; set; }
        public int AudioMix { get; set; }
        public string ProcessedVideoURL { get; set; }
        public string ProcessedThumbNailURL { get; set; }   //added by lohit
    }

    public partial class VideoFilesDTO
    {
        public VideoFilesDTO()
        {
            this.URL = "";
            this.ThumbNailURL = "";
        }

        public int VideoID { get; set; }
        public string VideoFormat { get; set; }
        public string ThumbnailFormat { get; set; }
        public string URL { get; set; }
        public string ThumbNailURL { get; set; }
        public string VideoBase64String { get; set; }
        public string ThumbnailBase64 { get; set; }
    }

    public partial class VideosRatingDTO
    {
        public int VideoRatingId { get; set; }
        public int VideoId { get; set; }
        public int UserId { get; set; }
        public float Rating { get; set; }
        public DateTime RatingDate { get; set; }
    }

    public partial class Visibility_DTO
    {
        public int VideoId { get; set; }
        public int VisibilityId { get; set; }
        public int UpdatedBy { get; set; }
    }

    public partial class UserTypes_DTO
    {
        public int ID { get; set; }
        public string Description { get; set; }
    }

    public partial class SubCategoryDTO
    {
        public int SubcategoryID { get; set; }
        public string Subcategory { get; set; }
    }

    public partial class CatalogDto
    {
        public string Segment { get; set; }
        public string Category { get; set; }
        public List<SubCategoryDTO> SubCategoriesList { get; set; }
    }

    public partial class VideosFlagReasonDTO
    {
        public int VideoFlagId { get; set; }
        public int VideoId { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }

    }

    public partial class UsageDTO
    {
        public int FeatureId { get; set; }
        public int Count { get; set; }
        public int Value { get; set; }
        public int UserId { get; set; }

    }

    public partial class ThumbNailDTO
    {
        public long VideoID { get; set; }
        public int UserId { get; set; }
        public string ThumbNailBase64 { get; set; }
        public string ThumbNailFormat { get; set; }
    }

    public partial class ExpertDTO : UserInfoDTO
    {
        public string UserPreference { get; set; }
    }

    public partial class PromotionalContentDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ThumbNailURL { get; set; }
        public string ThumbNailURLFormat { get; set; }
        public string URL { get; set; }
        public string URLFormat { get; set; }
        public string ActionType { get; set; }
        public int ActionTypeID { get; set; }
        public int Status { get; set; }
        public DateTime? EventStartDate { get; set; }
        public DateTime? EventEndDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int UserID { get; set; }
    }
    public partial class PromotionalContentStatus
    {
        public string PromotionalIds { get; set; }
        public int TypeId { get; set; }
    }
    public partial class BestTimeDTO
    {
        public int Id { get; set; }
        public string BestTime1 { get; set; }
        public string BestTime2 { get; set; }
        public int EventTypeId { get; set; }
        public int gender { get; set; }
        public int age { get; set; }
    }


    public partial class PersonalizationDTO
    {
        public int EventTypeId { get; set; }
        public int BestTime1 { get; set; }
        public int BestTime2 { get; set; }
        //public string Stroke1 { get; set; }
        //public string Stroke2 { get; set; }
        public bool WorkingWithCoach { get; set; }
        public int UserId { get; set; }
    }

    public partial class UserTrainingPlanDTO : TrainingPlanDTO
    {
        public int UTPId { get; set; }
        public int TrainingPlanId { get; set; }
        public DateTime StartDate { get; set; }
        public TimeSpan BestCurrentTime1 { get; set; }
        public TimeSpan BestCurrentTime2 { get; set; }
        public bool ShowQuestion { get; set; }
        public string Question { get; set; }
        public bool ShowRecord { get; set; }
        public int RecordedVideoId { get; set; }
        public bool ShowResults { get; set; }
        public string QuestionAns { get; set; }
        public bool started { get; set; }
        public int IdealVideoId { get; set; }
    }

    public partial class TrainingPlanDTO
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int BestTime1 { get; set; }
        public int BestTime2 { get; set; }
        public int Gender { get; set; }
        public int Age { get; set; }
        public int EventId { get; set; }
        public int UserId { get; set; }
        public int Default { get; set; }
        public int Day { get; set; }
        public List<int> VideoId { get; set; }
    }

    public partial class TrainingVideoDTO
    {
        public int Id { get; set; }
        public int PlanId { get; set; }
        public int VideoId { get; set; }
        public int UserId { get; set; }
    }

    public partial class TrainingRecordedVideosDTO : VideosInfoDTO
    {
        public int Id { get; set; }
        public int RecordedVideoId { get; set; }
        public int TrainingVideoId { get; set; }
        public int TrainingPlanId { get; set; }
        public int UserId { get; set; }
    }

    public partial class ExceptionDTO
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string Stacktrace { get; set; }
        public int userid { get; set; }
    }

    public partial class StrokeDTO
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }

    public partial class EventTypeDTO
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Subcategory { get; set; }
    }

    public partial class PlanDTO
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public List<int> VideoIDs { get; set; }
        public int Isdefault { get; set; }
    }

    public partial class PlanDaysDTO
    {
        public int PlanId { get; set; }
        public int DayId { get; set; }
    }

    public partial class BestTimesValuesDTO //List of 2 [ 0-100,1-200 ]
    {
        public int BestTimeId { get; set; }
        public string Stroke { get; set; }
        public string BestTimeValue { get; set; }
    }

    public partial class VideoURLDTO
    {
        public int Id { get; set; }
        public string VideoURL { get; set; }
        public string MovementName { get; set; }
        public string ActorName { get; set; }
    }



    public class BillingAddress
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
    }

    public class ShippingAddress
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
    }

    public class Customer
    {
        public int id { get; set; }
        public string created_at { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string username { get; set; }
        public string role { get; set; }
        public string last_order_id { get; set; }
        public string last_order_date { get; set; }
        public int orders_count { get; set; }
        public string total_spent { get; set; }
        public string avatar_url { get; set; }
        public BillingAddress billing_address { get; set; }
        public ShippingAddress shipping_address { get; set; }
    }

    public class CustomerDTO
    {
        public Customer customer { get; set; }
    }


    public class CustomersDTO
    {
        public List<Customer> customers { get; set; }
    }

    public class File
    {
        public string name { get; set; }
        public string file { get; set; }
    }

    public class Download
    {
        public string download_url { get; set; }
        public string download_id { get; set; }
        public int product_id { get; set; }
        public string download_name { get; set; }
        public int order_id { get; set; }
        public string order_key { get; set; }
        public string downloads_remaining { get; set; }
        public string access_expires { get; set; }
        public File file { get; set; }
    }

    public class DownloadDTO
    {
        public  List<Download> downloads { get; set; }
    }

    public partial class PaymentDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string PaymentToken { get; set; }
        public int DeviceType { get; set; }
        public string PaymentType { get; set; }
        public int VideoId { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int PlanType { get; set; }
 
    }

    public partial class YouTubeUrl
    {
        public int VideoId { get; set; }
        public string StorageUrl { get; set; }
        public string VideoName { get; set; }
    }

    public partial class UsageTrackingDto
    {
        public int OneTimeUsersCount { get; set; }
        public int UserVideosCount { get; set; }
    }
    public partial class ProcessedVideoDTO
    {
        public int VideoID { get; set; }
        public string ProcessedVideoURL { get; set; }
    }
}
