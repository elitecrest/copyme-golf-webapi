﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using PushSharp.Core;
using System.IO;
using System.Net.Http;
using Ikkos.Models.DAL;


namespace Ikkos.Models
{
    public class DAL2
    {
        internal static int AdminAddVideo_V2(VideosInfoDTO videoDTO)
        {
            try
            {
                int vid = 0;
                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AdminAddVideo_V2]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", System.Data.SqlDbType.Int);
                    UserID.Direction = System.Data.ParameterDirection.Input;
                    UserID.Value = videoDTO.OwnerID;

                    SqlParameter Segment = cmd.Parameters.Add("@Segment", System.Data.SqlDbType.VarChar, 100);
                    Segment.Direction = System.Data.ParameterDirection.Input;
                    Segment.Value = videoDTO.Segment;

                    SqlParameter Category = cmd.Parameters.Add("@Category", System.Data.SqlDbType.VarChar, 100);
                    Category.Direction = System.Data.ParameterDirection.Input;
                    Category.Value = videoDTO.Category;

                    SqlParameter SubCategory = cmd.Parameters.Add("@SubCategory", System.Data.SqlDbType.VarChar, 100);
                    SubCategory.Direction = System.Data.ParameterDirection.Input;
                    SubCategory.Value = videoDTO.SubCategory;

                    SqlParameter MovementName = cmd.Parameters.Add("@MovementName", System.Data.SqlDbType.VarChar, 100);
                    MovementName.Direction = System.Data.ParameterDirection.Input;
                    MovementName.Value = videoDTO.MovementName;

                    SqlParameter ActorName = cmd.Parameters.Add("@ActorName", System.Data.SqlDbType.VarChar, 100);
                    ActorName.Direction = System.Data.ParameterDirection.Input;
                    ActorName.Value = videoDTO.ActorName;

                    SqlParameter Orientation = cmd.Parameters.Add("@Orientation", System.Data.SqlDbType.Int);
                    Orientation.Direction = System.Data.ParameterDirection.Input;
                    Orientation.Value = videoDTO.Orientation;

                    SqlParameter Facing = cmd.Parameters.Add("@Facing", System.Data.SqlDbType.Int);
                    Facing.Direction = System.Data.ParameterDirection.Input;
                    Facing.Value = videoDTO.Facing;

                    SqlParameter ThumbNailURL = cmd.Parameters.Add("@ThumbNailURL", System.Data.SqlDbType.VarChar, 100);
                    ThumbNailURL.Direction = System.Data.ParameterDirection.Input;
                    ThumbNailURL.Value = videoDTO.ThumbNailURL;

                    SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", System.Data.SqlDbType.VarChar, 100);
                    VideoURL.Direction = System.Data.ParameterDirection.Input;
                    VideoURL.Value = videoDTO.URL;

                    SqlParameter Format = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Format.Direction = System.Data.ParameterDirection.Input;
                    Format.Value = videoDTO.Status;

                    SqlParameter VideoMode = cmd.Parameters.Add("@VideoMode", System.Data.SqlDbType.Int);
                    VideoMode.Direction = System.Data.ParameterDirection.Input;
                    VideoMode.Value = videoDTO.VideoMode;

                    SqlParameter AudioMix = cmd.Parameters.Add("@AudioMix", System.Data.SqlDbType.Int);
                    AudioMix.Direction = System.Data.ParameterDirection.Input;
                    AudioMix.Value = videoDTO.AudioMix;

                    SqlParameter ProcessedVideoURL = cmd.Parameters.Add("@ProcessedVideoURL", System.Data.SqlDbType.VarChar, 1000);
                    ProcessedVideoURL.Direction = System.Data.ParameterDirection.Input;
                    ProcessedVideoURL.Value = (videoDTO.ProcessedVideoURL == null) ? string.Empty : videoDTO.ProcessedVideoURL;

                    SqlParameter ProcessedThumbNailURL = cmd.Parameters.Add("@ProcessedThumbNailURL", System.Data.SqlDbType.VarChar, 1000);
                    ProcessedThumbNailURL.Direction = System.Data.ParameterDirection.Input;
                    ProcessedThumbNailURL.Value = (videoDTO.ProcessedThumbNailURL == null) ? string.Empty : videoDTO.ProcessedThumbNailURL;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        vid = (int)myData["ID"];
                    }

                    myConn.Close();
                }

                return vid;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static void UpdateVideo_V2(VideosInfoDTO videoDTO)
        {
            try
            {
                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateVideo_V2]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter VideoID = cmd.Parameters.Add("@VideoID", System.Data.SqlDbType.Int);
                    VideoID.Direction = System.Data.ParameterDirection.Input;
                    VideoID.Value = videoDTO.ID;

                    SqlParameter Segment = cmd.Parameters.Add("@Segment", System.Data.SqlDbType.VarChar, 100);
                    Segment.Direction = System.Data.ParameterDirection.Input;
                    Segment.Value = videoDTO.Segment;

                    SqlParameter Category = cmd.Parameters.Add("@Category", System.Data.SqlDbType.VarChar, 100);
                    Category.Direction = System.Data.ParameterDirection.Input;
                    Category.Value = videoDTO.Category;

                    SqlParameter SubCategory = cmd.Parameters.Add("@SubCategory", System.Data.SqlDbType.VarChar, 100);
                    SubCategory.Direction = System.Data.ParameterDirection.Input;
                    SubCategory.Value = videoDTO.SubCategory;

                    SqlParameter ActorName = cmd.Parameters.Add("@ActorName", System.Data.SqlDbType.VarChar, 100);
                    ActorName.Direction = System.Data.ParameterDirection.Input;
                    ActorName.Value = videoDTO.ActorName;

                    SqlParameter MovementName = cmd.Parameters.Add("@MovementName", System.Data.SqlDbType.VarChar, 100);
                    MovementName.Direction = System.Data.ParameterDirection.Input;
                    MovementName.Value = videoDTO.MovementName;

                    SqlParameter Orientation = cmd.Parameters.Add("@Orientation", System.Data.SqlDbType.Int);
                    Orientation.Direction = System.Data.ParameterDirection.Input;
                    Orientation.Value = videoDTO.Orientation;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", System.Data.SqlDbType.Int);
                    UserID.Direction = System.Data.ParameterDirection.Input;
                    UserID.Value = videoDTO.OwnerID;

                    SqlParameter Facing = cmd.Parameters.Add("@Facing", System.Data.SqlDbType.VarChar, 100);
                    Facing.Direction = System.Data.ParameterDirection.Input;
                    Facing.Value = videoDTO.Facing;

                    SqlParameter Format = cmd.Parameters.Add("@Format", System.Data.SqlDbType.VarChar, 100);
                    Format.Direction = System.Data.ParameterDirection.Input;
                    Format.Value = videoDTO.Format.ToLower();

                    SqlParameter VideoMode = cmd.Parameters.Add("@VideoMode", System.Data.SqlDbType.Int);
                    VideoMode.Direction = System.Data.ParameterDirection.Input;
                    VideoMode.Value = videoDTO.VideoMode;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static void UpdateProcessedVideo_V2(ProcessedVideoDTO ProcessedVideo)
        {
            try
            {
                SqlConnection myConn = UsersDAL.ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateProcessedVideo_V2]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter VideoID = cmd.Parameters.Add("@VideoID", System.Data.SqlDbType.Int);
                    VideoID.Direction = System.Data.ParameterDirection.Input;
                    VideoID.Value = ProcessedVideo.VideoID;

                    SqlParameter ProcessedVideoURL = cmd.Parameters.Add("@ProcessedVideoURL", System.Data.SqlDbType.VarChar, 1020);
                    ProcessedVideoURL.Direction = System.Data.ParameterDirection.Input;
                    ProcessedVideoURL.Value = ProcessedVideo.ProcessedVideoURL;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }
    }
}