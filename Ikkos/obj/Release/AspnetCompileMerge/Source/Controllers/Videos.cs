﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Remoting.Channels;
using System.Text.RegularExpressions;
using System.Web.Caching;
using System.Web.Http;
using System.Web;
//using System.Web.Script.Serialization;
using Ikkos.Models;
using System.Data.Entity;
using Ikkos.Models.DAL;
//using System.Data.Objects.DataClasses;
using System.Drawing;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net.Mail;
using System.ComponentModel;
using System.Text;
using System.Collections.Specialized;
using System.Reflection;
using System.Net.Http.Headers;
using Microsoft.WindowsAzure.MediaServices.Client;
using System.Threading;
//using WMPLib;

//using Microsoft.SqlServer.Dac;

namespace Ikkos.Controllers
{
    public class VideosController : ApiController
    {
        public static string Message = "";
        private Ikkos.Utility.CustomResponse Result = new Utility.CustomResponse();
        public string finalVideoUrl = string.Empty;
        private Int32 videoId = 0;

        // JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

        [HttpGet]
        public Utility.CustomResponse AllVideosByStatus(string userName)
        {


            try
            {
                List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

                int userid = 0;
                try
                {
                    userid = Convert.ToInt32(UsersDAL.GetUseridwithEmail(userName));
                }
                catch (Exception ex)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = "Access Denied";

                    return Result;
                }

                int role = UsersDAL.GetUserRoleByEmail(userName);
                // todo: get user id from username.
                if (role == 1 || role == 5)
                {
                    //user is admin so he can see all approved videos
                    videos = UsersDAL.AllVideosByStatus(1);
                }
                else
                {
                    videos = UsersDAL.AllUserVideos(userid);
                }

                if (videos.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videos;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse AllVideos(int UserId)
        {
            try
            {
                List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

                //int userid = 0;
                //// userid = Convert.ToInt32(UsersDAL.GetUseridwithEmail(userName));
                //userid = 1;

                if (UserId > 0)
                {
                    videos = UsersDAL.AllVideos(UserId);
                    if (videos.Count > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = videos;
                        Result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = "Access Denied";
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse AllVideos(string userName, int lastid)
        {
            try
            {
                List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

                int userid = 0;
                // userid = Convert.ToInt32(UsersDAL.GetUseridwithEmail(userName));
                userid = 1;

                if (userid > 0)
                {
                    videos = UsersDAL.AllVideos(userid);
                    if (videos.Count > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = videos;
                        Result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = "Access Denied";
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse Videos(string userName)
        {
            try
            {
                if (!UsersDAL.CheckAdminByEmail(userName))
                {
                    List<VideosInfoDTO> videos = UsersDAL.AllVideos(1);
                    if (videos.Count > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = videos;
                        Result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Access_Denied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetVideo(long videoId, string userName)
        {
            try
            {
                //if (UsersDAL.CheckAdminByEmail(userName))
                VideosInfoDTO video = UsersDAL.GetVideo(videoId);

                if (video.ID > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = video;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddVideo(VideosInfoDTO video)
        {
            try
            {
                // we need to get the role for the user who posted the video and then if he is admin we need to place video status as Approved else pending

                // todo: get user id from username.
                if (UsersDAL.GetUserRole(video.OwnerID))
                {
                    video.Status = 1;
                }
                else
                {
                    video.Status = 7;
                }

                int vid = UsersDAL.AddVideo(video);
                if (vid > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = vid;
                    Result.Message = CustomConstants.Video_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse StreamVideo(VideoFilesDTO video)
        {
            try
            {
                // we need to get the role for the user who posted the video and then if he is admin we need to place video status as Approved else pending

                Random r = new Random();
                string filename = "" + r.Next(999999) + DateTime.Now.Millisecond.ToString() + r.Next(99999).ToString() +
                                  "." + video.VideoFormat;
                string filename1 = "" + r.Next(999999) + DateTime.Now.Millisecond.ToString() + r.Next(99999).ToString() +
                                   "." + video.ThumbnailFormat;

                // windows azure blob uploading
                // ------------------------------------------------------- video uploading stat----------------------------------

                //  UploadVideoFileNew(video.Base64String, "test.3gp");

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccountvideo =
                    CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClientvideo =
                    storageAccountvideo.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer containervideo =
                    blobClientvideo.GetContainerReference("videosstore");

                // Create the container if it doesn't already exist.
                containervideo.CreateIfNotExists();


                containervideo.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess = Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideo =
                    containervideo.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydatavideo = Convert.FromBase64String(video.VideoBase64String);

                //using (var fileStream = binarydata)
                //{
                blockBlobvideo.Properties.ContentType = "video/" + video.VideoFormat;
                blockBlobvideo.UploadFromByteArray(binarydatavideo, 0, binarydatavideo.Length);

                //UploadFromStream(fileStream);
                //}


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideourl =
                    containervideo.GetBlockBlobReference(filename);

                string imageurlvideo = blockBlobvideourl.Uri.ToString();

                video.URL = imageurlvideo;


                // blob addition end
                //      ----------------------------video uploading end-----------------------------------------------------------


                //thumbnail start
                // windows azure blob uploading

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccountimage =
                    CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClientimage =
                    storageAccountimage.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer containerimage =
                    blobClientimage.GetContainerReference("imagesstore");

                // Create the container if it doesn't already exist.
                containerimage.CreateIfNotExists();


                containerimage.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobimage =
                    containerimage.GetBlockBlobReference(filename1);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydataimage = Convert.FromBase64String(video.ThumbnailBase64);

                blockBlobimage.Properties.ContentType = "image/" + video.ThumbnailFormat;
                blockBlobimage.UploadFromByteArray(binarydataimage, 0, binarydataimage.Length);

                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobimageurl =
                    containerimage.GetBlockBlobReference(filename1);

                string imageurlimage = blockBlobimageurl.Uri.ToString();
                video.ThumbNailURL = imageurlimage;

                UsersDAL.StreamVideo(video);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.Video_Uploaded_Successfully;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AdminAddVideo(VideosInfoDTO video)
        {
            try
            {
                // we need to get the role for the user who posted the video and then if he is admin we need to place video status as Approved else pending

                Random r = new Random();
                string filename = "" + r.Next(999999) + DateTime.Now.Millisecond.ToString() + r.Next(99999).ToString() +
                                  "." + video.Format;
                string filename1 = "" + r.Next(999999) + DateTime.Now.Millisecond.ToString() + r.Next(99999).ToString() +
                                   "." + video.ThumbnailFormat;

                // windows azure blob uploading
                // ------------------------------------------------------- video uploading stat----------------------------------

                //  UploadVideoFileNew(video.Base64String, "test.3gp");

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccountvideo =
                    CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClientvideo =
                    storageAccountvideo.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer containervideo =
                    blobClientvideo.GetContainerReference("videosstore");

                // Create the container if it doesn't already exist.
                containervideo.CreateIfNotExists();


                containervideo.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess = Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideo =
                    containervideo.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydatavideo = Convert.FromBase64String(video.Base64String);

                //using (var fileStream = binarydata)
                //{
                blockBlobvideo.Properties.ContentType = "video/" + video.Format;
                blockBlobvideo.UploadFromByteArray(binarydatavideo, 0, binarydatavideo.Length);

                //UploadFromStream(fileStream);
                //}


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideourl =
                    containervideo.GetBlockBlobReference(filename);

                string imageurlvideo = blockBlobvideourl.Uri.ToString();

                video.URL = imageurlvideo;


                // blob addition end
                //      ----------------------------video uploading end-----------------------------------------------------------


                //thumbnail start
                // windows azure blob uploading

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccountimage =
                    CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClientimage =
                    storageAccountimage.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer containerimage =
                    blobClientimage.GetContainerReference("imagesstore");

                // Create the container if it doesn't already exist.
                containerimage.CreateIfNotExists();


                containerimage.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobimage =
                    containerimage.GetBlockBlobReference(filename1);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydataimage = Convert.FromBase64String(video.ThumbnailString);

                blockBlobimage.Properties.ContentType = "image/" + video.ThumbnailFormat;
                blockBlobimage.UploadFromByteArray(binarydataimage, 0, binarydataimage.Length);

                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobimageurl =
                    containerimage.GetBlockBlobReference(filename1);

                string imageurlimage = blockBlobimageurl.Uri.ToString();
                video.ThumbNailURL = imageurlimage;

                // we need to get the role for the user who posted the video and then if he is admin we need to place video status as Approved else pending
                video.Status = 1;
                int vid = UsersDAL.AdminAddVideo(video);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = vid;
                Result.Message = CustomConstants.Video_Added_Successfully;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse UpdateVideo(VideosInfoDTO video, string userName)
        {
            try
            {
                //if (UsersDAL.CheckAdminByEmail(userName))
                //{
                if (UsersDAL.CheckVideoExists(video.ID))
                {
                    //in windows azure we need to delete the existing blob and then add new blob in database


                    CloudStorageAccount storageAccount =
                        CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve reference to a previously created container.
                    CloudBlobContainer container = blobClient.GetContainerReference("videosstore");

                    // Retrieve reference to a blob named "photo1.jpg".

                    string uri = UsersDAL.GetBlobUriByVideoId(video.ID);
                    string format = "";

                    try
                    {
                        format = uri.Substring(uri.LastIndexOf('.') + 1);
                    }
                    catch (Exception ex)
                    {
                    }

                    if (video.Base64String != null)
                    {
                        CloudBlockBlob blockBlobdel = container.GetBlockBlobReference(uri.Trim());
                        if (blockBlobdel.Exists(null, null))
                        {
                            blockBlobdel.Delete();

                        }

                        //now we need to add the given base64string into azure

                        // blob addition end


                        Random r = new Random();
                        string filename1 = "" + r.Next(999999) + DateTime.Now.Millisecond.ToString() +
                                           r.Next(99999).ToString() + "." + video.Format;
                        ;

                        // Retrieve storage account from connection string.
                        CloudStorageAccount storageAccountvideo =
                            CloudStorageAccount.Parse(
                                ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                        // Create the blob client.
                        Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClientvideo =
                            storageAccountvideo.CreateCloudBlobClient();

                        // Retrieve a reference to a container. 
                        Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer containervideo =
                            blobClientvideo.GetContainerReference("videosstore");

                        // Create the container if it doesn't already exist.
                        containervideo.CreateIfNotExists();


                        containervideo.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                        {
                            PublicAccess = Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                        });


                        // Retrieve reference to a blob named "myblob".
                        Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideo =
                            containervideo.GetBlockBlobReference(filename1);

                        // Create or overwrite the "myblob" blob with contents from a local file.

                        byte[] binarydatavideo = Convert.FromBase64String(video.Base64String);

                        //using (var fileStream = binarydata)
                        //{
                        blockBlobvideo.Properties.ContentType = "video/" + video.Format;
                        blockBlobvideo.UploadFromByteArray(binarydatavideo, 0, binarydatavideo.Length);

                        //UploadFromStream(fileStream);
                        //}


                        // Retrieve reference to a blob named "myblob".
                        Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideourl =
                            containervideo.GetBlockBlobReference(filename1);

                        string imageurlvideo = blockBlobvideourl.Uri.ToString();
                        video.URL = imageurlvideo;
                    }
                    else
                    {
                        video.URL = uri;
                        video.Format = format;
                    }

                    // if thumbnail string is not empty then only we need to delete existing blob in azure and then we need to add new blob

                    CloudBlobContainer containerthumbnail = blobClient.GetContainerReference("imagesstore");

                    // Retrieve reference to a blob named "photo1.jpg".

                    string urithumbnail = UsersDAL.GetBlobThumbnailUriByVideoId(video.ID);

                    if (video.ThumbnailString != null)
                    {

                        CloudBlockBlob blockBlobdel = container.GetBlockBlobReference(urithumbnail.Trim());
                        if (blockBlobdel.Exists(null, null))
                        {
                            blockBlobdel.Delete();

                        }

                        // now we need to add new thumbnail to azure

                        Random r = new Random();
                        string filename1 = "" + r.Next(999999) + DateTime.Now.Millisecond.ToString() +
                                           r.Next(99999).ToString() + "." + video.ThumbnailFormat;

                        // Retrieve storage account from connection string.
                        CloudStorageAccount storageAccountvideo =
                            CloudStorageAccount.Parse(
                                ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                        // Create the blob client.
                        Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClientvideo =
                            storageAccountvideo.CreateCloudBlobClient();

                        // Retrieve a reference to a container. 
                        Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer containervideo =
                            blobClientvideo.GetContainerReference("imagesstore");

                        // Create the container if it doesn't already exist.
                        containervideo.CreateIfNotExists();


                        containervideo.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                        {
                            PublicAccess = Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                        });


                        // Retrieve reference to a blob named "myblob".
                        Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideo =
                            containervideo.GetBlockBlobReference(filename1);

                        // Create or overwrite the "myblob" blob with contents from a local file.

                        byte[] binarydatavideo = Convert.FromBase64String(video.ThumbnailString);

                        //using (var fileStream = binarydata)
                        //{
                        blockBlobvideo.Properties.ContentType = "image/" + video.ThumbnailFormat;
                        blockBlobvideo.UploadFromByteArray(binarydatavideo, 0, binarydatavideo.Length);

                        //UploadFromStream(fileStream);
                        //}

                        // Retrieve reference to a blob named "myblob".
                        Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideourl =
                            containervideo.GetBlockBlobReference(filename1);

                        string imageurlvideo = blockBlobvideourl.Uri.ToString();
                        video.ThumbNailURL = imageurlvideo;
                    }
                    else
                    {
                        video.ThumbNailURL = urithumbnail;
                    }

                    UsersDAL.UpdateVideo(video);
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.Video_Updated_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Video_DoesNot_Exists;
                }
                // }
                //else
                //{
                //    Result.Status = Utility.CustomResponseStatus.UnSuccessfull;
                //    Result.Message = CustomConstants.Access_Denied;
                //}
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        // <summary> This API is used to return recommendedVideos </summary>
        [HttpGet]
        public Utility.CustomResponse RecommendedVideos()
        {
            try
            {
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.Visibility_Update_Fail;
                Result.Response = UsersDAL.GetRecommendedVideos();


            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        // <summary> This API is used to return recommendedVideos For App </summary>
        [HttpGet]
        public Utility.CustomResponse GetRecommendedVideos(int userid)
        {
            try
            {

                Result.Response = UsersDAL.GetRecommendedVideosApp(userid);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.Details_Get_Successfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        // [HttpDelete]
        [HttpPost]
        public Utility.CustomResponse DeleteVideo(int videoId, int userid)
        {
            try
            {

                if (UsersDAL.CheckVideoExists(videoId))
                {
                    //string bloburi = UsersDAL.GetBlobUriByVideoId(videoId);

                    //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());

                    //// Create the blob client.
                    //CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    //// Retrieve reference to a previously created container.
                    //CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                    //// Retrieve reference to a blob named "photo1.jpg".



                    //CloudBlockBlob blockBlob = container.GetBlockBlobReference(bloburi);
                    //if(blockBlob.Exists(null,null))
                    //{
                    //    blockBlob.Delete();

                    //}

                    //// blob deletion end
                    UsersDAL.DeleteVideo(videoId, userid);
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.Video_Deleted_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Video_DoesNot_Exists;
                }
                //}
                //else
                //{
                //    Result.Status = Utility.CustomResponseStatus.UnSuccessfull;
                //    Result.Message = CustomConstants.Access_Denied;
                //}
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse AllPendingVideos(string userName)
        {
            try
            {
                if (UsersDAL.CheckUserByEmail(userName))
                {
                    List<VideosInfoDTO> videos = UsersDAL.AllVideosByStatus(7);
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videos;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        public HttpResponseMessage UploadVideo()
        {
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;

            // Check if files are available
            if (httpRequest.Files.Count > 0)
            {
                var files = new List<string>();

                // interate the files and save on the server
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    var filePath = HttpContext.Current.Server.MapPath("~/Videos/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);

                    files.Add(filePath);
                }

                // return result
                result = Request.CreateResponse(HttpStatusCode.Created, files);
            }
            else
            {
                // return BadRequest (no file(s) available)
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return result;
        }



        [HttpGet]
        public Utility.CustomResponse ApproveVideo(int videoid, string updatedby, int status)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                //check if updatedby email is in role of
                int role = UsersDAL.GetUserRoleByEmail(updatedby);
                if (role == 1 || role == 3)
                {
                    //valid roles so we need to update the record

                    UsersDAL.ApproveVideo(videoid, status, updatedby);
                    if (status == 1)
                        res.Message = CustomConstants.Video_Accepted_Successfully;
                    else if (status == 3)
                        res.Message = CustomConstants.Video_Rejected_Successfully;
                }
                else
                {
                    res.Message = CustomConstants.Access_Denied;
                }
            }
            catch (Exception ex)
            {
                res.Status = Utility.CustomResponseStatus.Exception;
                res.Message = ex.Message;
            }

            return res;
        }

        [HttpGet]
        public Utility.CustomResponse VideoOfTheDay(int userId, int categoryId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                int DeviceId = 0;

                var headers = System.Web.HttpContext.Current.Request.Headers;

                if (headers.AllKeys.Contains("APPID"))
                {

                    string AppId = headers.GetValues("APPID").First();
                    string[] arryAppId = AppId.Split('_');
                    DeviceId = Convert.ToInt32(arryAppId[1]);
                }
                else if (headers.AllKeys.Contains("AppID"))
                {

                    string AppId = headers.GetValues("AppID").First();
                    string[] arryAppId = AppId.Split('_');
                    DeviceId = Convert.ToInt32(arryAppId[1]);
                }
                var videooftheday = UsersDAL.GetVideoOfTheDay(userId, categoryId, DeviceId);
                if (videooftheday.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videooftheday;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; // javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetTypes(int typeid)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var types = UsersDAL.GetUserTypes();
                if (types.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = types;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; // javaScriptSerializer.Serialize(Result);
            }

            return Result;
        }

        //<summary>This API is used to return subcategories </summary>
        [HttpGet]
        public Utility.CustomResponse GetSegments()
        {
            try
            {
                var segments = UsersDAL.Segments();
                if (segments.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = segments;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; // javaScriptSerializer.Serialize(Result);
            }
        }

        //<summary>This API is used to return subcategories </summary>
        [HttpGet]
        public Utility.CustomResponse GetCategories(string segment)
        {
            try
            {
                var categories = UsersDAL.Categories(segment);
                if (categories.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = categories;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; // javaScriptSerializer.Serialize(Result);
            }
        }

        //<summary>This API is used to return subcategories </summary>
        [HttpGet]
        public Utility.CustomResponse GetSubCategories(string categoryname)
        {
            try
            {
                var subcategories = UsersDAL.Subcategories(categoryname);
                if (subcategories.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = subcategories;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; // javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public Utility.CustomResponse Catalog(int lastID)
        {
            try
            {
                var catalog = UsersDAL.GetCatalog(lastID);
                if (catalog.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = catalog;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; // javaScriptSerializer.Serialize(Result);
            }
        }

        //Added By Bindu
        [HttpGet]
        public Utility.CustomResponse VideosSearch(int userId, string segment, string category, string subCategory,
            int offset, int max, string searchText)
        {
            try
            {
                LoginController login = new LoginController();
                Utility.CustomResponse res = login.GetUserSubscription(userId);
                bool isSubscribed = Convert.ToBoolean(res.Response);

                var videos = UsersDAL.GetAllVideosSearch(userId, segment, category, subCategory, offset, max,
                    searchText, isSubscribed);
                if (videos.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videos;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; // javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public Utility.CustomResponse VideoSubCategories()
        {
            try
            {
                var videos = UsersDAL.GetVideoSubCategories();
                if (videos.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videos;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; // javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public Utility.CustomResponse VideoCatalog(int lastID)
        {
            try
            {
                var videos = UsersDAL.GetVideoCatalogList(lastID);
                if (videos.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videos;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; // javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public Utility.CustomResponse VideoBySearchText(string searchText)
        {
            try
            {

                var videos = UsersDAL.GetVideoBySearchText(searchText);
                if (videos.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videos;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; // javaScriptSerializer.Serialize(Result);
            }
        }

        //End Of Code by Bindu

        //<summary>This API is used to return Flagreasons </summary>
        [HttpGet]
        public Utility.CustomResponse GetFlagReasons()
        {
            try
            {
                var FlagReasons = UsersDAL.GetFlagReasons();
                if (FlagReasons.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = FlagReasons;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        // Add videoFlagReason
        [HttpPost]
        public Utility.CustomResponse AddVideoFlag(VideosFlagReasonDTO FlagReasonDTO)
        {
            try
            {

                //   rating.UserId =Convert.ToInt32(Userid);
                UsersDAL.AddVideoFlag(FlagReasonDTO);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.Video_Flag_Added_Successfully;
                Result.Response = FlagReasonDTO.VideoId;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse AllUserVideos(string userName)
        {
            try
            {
                List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

                int userid = 0;
                userid = Convert.ToInt32(UsersDAL.GetUseridwithEmail(userName));


                if (userid > 0)
                {
                    videos = UsersDAL.AllUserVideos(userid);
                    if (videos.Count > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = videos;
                        Result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Access_Denied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        //Get a particular Pending Video using VideoID and Username
        [HttpGet]
        public Utility.CustomResponse GetPendingVideo(long videoId, string userName)
        {
            try
            {

                VideosInfoDTO video = UsersDAL.GetPendingVideo(videoId);

                if (video.ID > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = video;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse UpdateVideoUrl(long VideoID, int UserId, string VideoURL, string ThumbNailURL)
        {
            try
            {

                UsersDAL.UpdateVideoUrl(VideoID, UserId, VideoURL, ThumbNailURL);


                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = CustomConstants.Video_Updated_Successfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        private string GetFileURL(string filename1)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999) +
                                  r.Next(9999).ToString() + DateTime.Now.Millisecond;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount =
                    CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container =
                    blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                using (var fileStream = System.IO.File.OpenRead(filename1))
                {
                    blockBlob.Properties.ContentType = ".p12";
                    blockBlob.UploadFromStream(fileStream);
                }
                //using (var fileStream = binarydata)
                //{



                //UploadFromStream(fileStream);
                //}


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                imageurl = blockBlob1.Uri.ToString();
                // -- todo: rohan - save in db so we can clean up later.
                // UsersDAL.StoreImageInDB(imageurl, 1);

            }
            catch (Exception ex)
            {
                //lblstatus.Text = "Failed to upload image to Azure Server";
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = "Failed to add image";
            }

            return imageurl;
        }

        [HttpGet]
        public Utility.CustomResponse GetVideosInRange(int videoID1, int videoID2)
        {
            try
            {
                List<VideosInfoDTO> videos = new List<VideosInfoDTO>();


                videos = UsersDAL.GetVideosInRange(videoID1, videoID2);
                if (videos.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videos;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse PurchaseVideos(int UserId)
        {
            try
            {
                List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

                videos = UsersDAL.GetPurchaseVideos(UserId);
                if (videos.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videos;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse streamVideoThumbNail(ThumbNailDTO thumbNailDTO)
        {
            try
            {
                string imageURL = GetImageURL(thumbNailDTO.ThumbNailBase64, thumbNailDTO.ThumbNailFormat);

                UsersDAL.UpdateVideoUrl(thumbNailDTO.VideoID, thumbNailDTO.UserId, "", imageURL);


                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.Video_Updated_Successfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        public string GetImageURL(string binary, string format)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999) +
                                  r.Next(9999).ToString() + DateTime.Now.Millisecond;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount =
                    CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container =
                    blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydata = Convert.FromBase64String(binary);

                //using (var fileStream = binarydata)
                //{
                blockBlob.Properties.ContentType = format;
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);

                //UploadFromStream(fileStream);
                //}


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                imageurl = blockBlob1.Uri.ToString();

            }
            catch (Exception ex)
            {
                //lblstatus.Text = "Failed to upload image to Azure Server";
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = "Failed to add image";
            }

            return imageurl;
        }


        [HttpGet]
        public Utility.CustomResponse YouTubeDownloadUpload(string youTubeUrl)
        {
            try
            {
                if (!youTubeUrl.Contains("youtube.com/watch?v="))
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = null;
                    Result.Message = CustomConstants.videoNotExist;
                }
                else
                {
                    videoId = UsersDAL.AddYouTubeUrl(youTubeUrl);


                    string[] parts = youTubeUrl.Split('?');
                    string temp12 = string.Empty;
                    string videoID = string.Empty;
                    //  var videoUri;
                    if (parts[1].Length > 13)
                    {
                        string[] parts1 = Convert.ToString(parts[1]).Split('-');
                        foreach (string part in parts1)
                        {
                            if (part.StartsWith("v="))
                            {
                                var len = part.Length;
                                temp12 = part.Substring(2);
                            }
                        }

                    }
                    else
                    {
                        temp12 = Convert.ToString(parts[1]).Substring(2);
                    }



                    string videoInfoUrl = "http://www.youtube.com/get_video_info?video_id=" + temp12;
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(videoInfoUrl);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    Stream responseStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));

                    string videoInfo = HttpUtility.HtmlDecode(reader.ReadToEnd());
                    if (videoInfo.Contains("status=fail"))
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Response = null;
                        if (videoInfo.Contains("errorcode=2"))
                        {
                            Result.Message = CustomConstants.videoNotExist;
                        }
                        else
                        {
                            Result.Message = CustomConstants.CopyRightIssue;
                        }
                    }
                    else
                    {
                        NameValueCollection videoParams = HttpUtility.ParseQueryString(videoInfo);

                        //if (videoParams["reason"] != null)
                        //{
                        //    lblMessage.Text = videoParams["reason"];
                        //    return;
                        //}

                        string[] videoURLs = videoParams["url_encoded_fmt_stream_map"].Split(',');
                        Dictionary<string, string> videoFormatList = new Dictionary<string, string>();
                        foreach (string vURL in videoURLs)
                        {
                            string sURL = HttpUtility.HtmlDecode(vURL);

                            NameValueCollection urlParams = HttpUtility.ParseQueryString(sURL);
                            string videoFormat = HttpUtility.HtmlDecode(urlParams["type"]);

                            sURL = HttpUtility.HtmlDecode(urlParams["url"]);
                            sURL += "&signature=" + HttpUtility.HtmlDecode(urlParams["sig"]);
                            sURL += "&type=" + videoFormat;
                            sURL += "&title=" + HttpUtility.HtmlDecode(videoParams["title"]);

                            videoFormat = urlParams["quality"] + " - " + videoFormat.Split(';')[0].Split('/')[1];
                            if (!videoFormatList.ContainsKey(videoFormat))
                            {
                                videoFormatList.Add(videoFormat, sURL);

                            }
                        }
                        if (videoFormatList.ContainsKey("medium - mp4"))
                        {
                            string sURL = videoFormatList["medium - mp4"];

                            NameValueCollection urlParams = HttpUtility.ParseQueryString(sURL);
                            string temp = urlParams["title"];
                            string my_String = Regex.Replace(temp, @"[^0-9a-zA-Z]+", " ");
                            string videoTitle = my_String + " " + "medium - mp4";
                            string videoFormt = HttpUtility.HtmlDecode(urlParams["type"]);
                            videoFormt = videoFormt.Split(';')[0].Split('/')[1];

                            //string downloadPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                            //string sFilePath = string.Format(Path.Combine(downloadPath, "Downloads\\{0}.{1}"), videoTitle, videoFormt);
                            string downloadPath = HttpContext.Current.Server.MapPath("~/");
                            //var fileStream = System.IO.File.Create(path);
                            string sFilePath = downloadPath + "\\" + videoTitle + "." + videoFormt;
                            Uri uri = new Uri(sURL);

                            WebClient webClient = new WebClient();
                            webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                            webClient.DownloadFileAsync(new Uri(sURL), sFilePath, sFilePath);

                            Result.Status = Utility.CustomResponseStatus.Successful;
                            Result.Response = Convert.ToString(videoId);
                            Result.Message = CustomConstants.Details_Get_Successfully;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            string filename = (string)e.UserState;
            finalVideoUrl = Handlefile(filename);
        }
        private string Handlefile(string fileName)
        {
            //var duration = Duration(fileName);
            string baseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            string filePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite);
            //int length = (int)Request.InputStream.Length;
            int length = (int)fileStream.Length;
            byte[] buffer = new byte[length];
            Stream stream = fileStream as Stream;
            string VideoFormat = "mp4";//string VideoFormat = Request.QueryString["VideoFormat"].ToString().Trim();
            // string ThumbnailBase64 = Request.QueryString["ThumbnailURL"].Trim().ToString();
            // string ThumbnailFormat = Request.QueryString["ThumbnailFormat"].Trim().ToString();

            //VideoDTO video = new VideoDTO();
            var videoName = string.Empty;
            string videoURL = streamaudio(stream, VideoFormat, ref videoName);
            finalVideoUrl = videoURL;
            Result.Response = finalVideoUrl;
            UsersDAL.UpdateStorageVideo(videoId, videoURL, videoName);

            return videoURL;
        }

        private string streamaudio(Stream stream, string format, ref string videoName)
        {
            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next();
            videoName = fileNamebanners;
            fileNamebanners = fileNamebanners + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);

            var fileStream = System.IO.File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();

            var ext = format;

            Stream fileStreambanners = stream;

            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string videoURL = UploadImage1(fileNamebanners, path, ext, fileStreambanners);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            finalVideoUrl = videoURL;
            return videoURL;
        }

        private string UploadImage1(string fileName, string path, string ext, Stream InputStream)
        {
            string account = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountName"].ToString();
            string key = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountKey"].ToString();

            CloudMediaContext context = new CloudMediaContext(account, key);

            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);

            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));

            assetFile.Upload(path);
            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 365;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive),
                                                     AccessPermissions.Read);
            string streamingUrl = string.Empty;
            var assetFiles = streamingAsset.AssetFiles.ToList();
            var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".mp4")).FirstOrDefault();
            if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            {
                var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                var mp4Uri = new UriBuilder(locator.Path);
                mp4Uri.Path += "/" + streamingAssetFile.Name;
                streamingUrl = mp4Uri.ToString();
            }
            return streamingUrl;

        }

        [HttpGet]
        public Utility.CustomResponse GetYoutubeStorageUrl(int videoId, string youTubeUrl)
        {
            try
            {
                var FlagReasons = UsersDAL.GetStorageVideoUrl(videoId, youTubeUrl);
                if (FlagReasons != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = FlagReasons;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetVideoUrl()
        {
            try
            {
                var videoUrl = UsersDAL.GetVideoUrl();
                if (videoUrl != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videoUrl;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
    }

    public class UploadBkpDTO
    {
        public string videostring { get; set; }
    }





}
