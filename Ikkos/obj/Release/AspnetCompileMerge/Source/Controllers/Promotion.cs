﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
//using System.Web.Script.Serialization;
using Ikkos.Models;
using System.Data.Entity;
using Ikkos.Models.DAL;
//using System.Data.Objects.DataClasses;
using System.Drawing;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net.Mail;

//using Microsoft.SqlServer.Dac;

namespace Ikkos.Controllers
{ 
    public class PromotionController : ApiController
    {
        public static string Message = "";
        Ikkos.Utility.CustomResponse Result = new Utility.CustomResponse();
        // JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

        [HttpGet]
        public Utility.CustomResponse PromotionalContent()
        {
            try
            {
                List<PromotionalContentDTO> promoContent = new List<PromotionalContentDTO>();
                DateTime today = DateTime.Now;
                promoContent = UsersDAL.GetPromotionalContent(today);
                if (promoContent.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = promoContent;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
 
        [HttpPost]
        public Utility.CustomResponse AddPromotionalContent(PromotionalContentDTO PromotionalContentDTO)
        {
            try
            { 
                string contentType = string.Empty;
                if (!PromotionalContentDTO.URL.StartsWith("http"))
                {
                    contentType = "video/"+ PromotionalContentDTO.URLFormat;
                    PromotionalContentDTO.URL = GetImageURL(PromotionalContentDTO.URL, PromotionalContentDTO.URLFormat, contentType);
                }
                if (!PromotionalContentDTO.ThumbNailURL.StartsWith("http"))
                {
                    contentType = "image/" + PromotionalContentDTO.URLFormat;
                    PromotionalContentDTO.ThumbNailURL = GetImageURL(PromotionalContentDTO.ThumbNailURL, PromotionalContentDTO.ThumbNailURLFormat,contentType);
                }
                 PromotionalContentDTO PromotionalContent = new PromotionalContentDTO();
                 PromotionalContent = UsersDAL.AddPromotionalContent(PromotionalContentDTO);

                 if (PromotionalContent.Id > 0)
                {
                    Result.Response = PromotionalContent;
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.Content_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse UpdatePromotionalContent(PromotionalContentDTO PromotionalContentDTO)
        {
            try
            {

                string contentType = string.Empty;
                if (!PromotionalContentDTO.URL.StartsWith("http"))
                {
                    contentType = "video/" + PromotionalContentDTO.URLFormat;
                    PromotionalContentDTO.URL = GetImageURL(PromotionalContentDTO.URL, PromotionalContentDTO.URLFormat, contentType);
                }
                if (!PromotionalContentDTO.ThumbNailURL.StartsWith("http"))
                {
                    contentType = "image/" + PromotionalContentDTO.URLFormat;
                    PromotionalContentDTO.ThumbNailURL = GetImageURL(PromotionalContentDTO.ThumbNailURL, PromotionalContentDTO.ThumbNailURLFormat, contentType);
                }
                PromotionalContentDTO PromotionalContent = new PromotionalContentDTO();
                PromotionalContent = UsersDAL.UpdatePromotionalContent(PromotionalContentDTO);

                if (PromotionalContent.Id > 0)
                {
                    Result.Response = PromotionalContent;
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.Content_Updated_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }



        [HttpPost]
        public Utility.CustomResponse UpdatePromotionalStatus(PromotionalContentStatus promotionalContentStatus)
        {
            try
            {
              
                    UsersDAL.UpdatePromotionalStatus(promotionalContentStatus.PromotionalIds.ToString(), promotionalContentStatus.TypeId);
               

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.PromotionStatus_Updated_Successfully;


            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse DeletePromotionalContent(int id, int userid)
        {
            try
            {

                UsersDAL.DeletePromotionalContent(id, userid);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.Content_Deleted_Successfully;
                Result.Response = id;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        //Portal APIS

        [HttpGet]
        public Utility.CustomResponse GetPromotionalContentByType(int Type)
        {
            try
            {
                List<PromotionalContentDTO> promoContent = new List<PromotionalContentDTO>();
             
                promoContent = UsersDAL.GetPromotionalContentByType(Type);
                if (promoContent.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = promoContent;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetPromotionalContentById(int Id)
        {
            try
            {
                List<PromotionalContentDTO> promoContent = new List<PromotionalContentDTO>();              
                promoContent = UsersDAL.GetPromotionalContentByID(Id);
                if (promoContent.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = promoContent;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

      
        public string GetImageURL(string binary, string format, string contentType)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = "" + r.Next(999999) + DateTime.Now.Millisecond.ToString() + r.Next(99999).ToString() + format;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("videosstore");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydata = Convert.FromBase64String(binary);


                blockBlob.Properties.ContentType = contentType;
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);

                //UploadFromStream(fileStream);
                //}


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                imageurl = blockBlob1.Uri.ToString();

            }
            catch (Exception ex)
            {
                //lblstatus.Text = "Failed to upload image to Azure Server";
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = "Failed to add image";
            }

            return imageurl;
        }
    }
}
