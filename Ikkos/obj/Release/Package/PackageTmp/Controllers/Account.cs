﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
//using System.Web.Script.Serialization;
using Ikkos.Models;
using System.Data.Entity;
using Ikkos.Models.DAL;
//using System.Data.Objects.DataClasses;
using System.Drawing;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net.Mail;

//using Microsoft.SqlServer.Dac;

namespace Ikkos.Controllers
{
    public class AccountController : ApiController
    {
        public static string Message = "";
        Ikkos.Utility.CustomResponse Result = new Utility.CustomResponse();
        // JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

        [HttpGet]
        public Utility.CustomResponse HowToIkkos()
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var howtoikkos = UsersDAL.HowTo(1, 1);
                if (howtoikkos.ID > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = howtoikkos;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public Utility.CustomResponse HowToCopyMe()
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var howtocopy = UsersDAL.HowTo(1, 2);
                if (howtocopy.ID > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = howtocopy;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public Utility.CustomResponse HowToPublish()
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var howtopublish = UsersDAL.HowTo(1, 3);
                if (howtopublish.ID > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = howtopublish;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public Utility.CustomResponse Organizations()
        {
            try
            {
                var orgs = UsersDAL.GetOrganizations();
                if (orgs.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = orgs;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }

        //<summary>This API Stores azure backup files to blob in azure</summary>
        [HttpPost]
        public Utility.CustomResponse BackupDatabase(UploadBkpDTO objUploadBkpDTO)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {
                // Get the Storage Account
                //    var backupStorageAccount =CloudStorageAccount.FromConfigurationSetting("StorageAccount");
                CloudStorageAccount backupStorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());

                // The container to store backups
                //  var backupBlobClient = backupStorageAccount.CreateCloudBlobClient();
                //    var  backupContainer = backupBlobClient.GetContainerReference("backups");
                //       backupContainer.CreateIfNotExist();

                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient backupBlobClient = backupStorageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer backupContainer = backupBlobClient.GetContainerReference("ikkosbackups");

                // Create the container if it doesn't already exist.
                backupContainer.CreateIfNotExists();

                // The backup file on blob storage
                var storageName = string.Format("Backup_{0}.bacpac", DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob backupFile = backupContainer.GetBlockBlobReference(storageName);

                //   var backupFile = backupContainer.GetBlobReferenceFromServer(storageName);

                //   // Get a reference to the temporary files
                //   string localResource = HttpContext.Current.Server.MapPath("~/BackupTempFiles");

                //       //RoleEnvironment.GetLocalResource("TempFiles");
                //  // var file = string.Format("{0}{1}", localResource.RootPath, backupFile.Name);
                //   string file = localResource +'/'+ backupFile.Name;
                //   // Connect to the DacServices

                //   var services = new DacServices(ConfigurationManager
                //                  .ConnectionStrings["CopyMe"].ConnectionString);
                //   services.Message += (sender, e) =>
                //   {
                //       // If you use a lock file, 
                //       // this would be a good location to extend the lease
                //   };


                //   // Export the database to the local disc
                //   services.ExportBacpac(file, "CopyMe");

                // Upload the file to Blob Storage
                //    FileStream stream = File.OpenRead(HttpContext.Current.Server.MapPath("~/BackupTempFiles/") + "Backup_20140920-111102.bacpac");
                //    byte[] fileBytes = new byte[stream.Length];
                byte[] fileBytes = Convert.FromBase64String(objUploadBkpDTO.videostring);
                backupFile.UploadFromByteArray(fileBytes, 0, fileBytes.Length);

                //  backupFile.Properties.ContentType = "binary/octet-stream";
                // backupFile.UploadFromFile(HttpContext.Current.Server.MapPath("~/BackupTempFiles/") + "Backup_20140920-111102.bacpac", FileMode.Append);
                // Remove the temporary file
                //   File.Delete(file);
                res.Status = 0;
                res.Message = CustomConstants.Uploaded_Successfully;
                return res;
            }
            catch (Exception ex)
            {
                res.Status = 0;
                res.Message = "Failed to upload file :" + ex.Message;
                return res;
            }

        }


        // Add Usage Tracking
        [HttpPost]
        public Utility.CustomResponse UsageTracking(IEnumerable<UsageDTO> usageDTO)
        {
            try
            {
                foreach (var u in usageDTO)
                {
                    UsersDAL.AddUsageTracking(u);
                }


                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.Usage_Tracking_Successfully;


            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddPushNotifications(string message, int badge)
        {

            // string filename = GetFileURL("D:\\iKKOSAdminPushCertificates.p12");
            try
            {
                //   rating.UserId =Convert.ToInt32(Userid);
                UsersDAL.AddPushNotifications(message, badge);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.PushNotifications;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse UsageTrack()
        {
            try
            {
                var orgs = UsersDAL.UsersTracking();
                if (orgs != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = orgs;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }
    }
}
