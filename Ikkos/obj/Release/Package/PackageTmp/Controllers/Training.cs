﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web; 
using Ikkos.Models;
using System.Data.Entity;
using Ikkos.Models.DAL;
using System.Configuration;
using System.Data.SqlClient;

namespace Ikkos.Controllers
{
    public class TrainingController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        //[HttpGet]
        //public Utility.CustomResponse BestTimes(int userId, int strokeTypeId)
        //{
        //    try
        //    {

        //        List<BestTimeDTO> BestTime = UsersDAL.GetUserPernalizationData(userId, strokeTypeId);
        //        if (BestTime.Count > 0)
        //        {
        //            Result.Status = Utility.CustomResponseStatus.Successful;
        //            Result.Response = BestTime;
        //            Result.Message = CustomConstants.Details_Get_Successfully;
        //        }
        //        else
        //        {
        //            Result.Status = Utility.CustomResponseStatus.UnSuccessful;
        //            Result.Message = CustomConstants.NoRecordsFound;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Result.Status = Utility.CustomResponseStatus.Exception;
        //        Result.Message = ex.Message;
        //    }
        //    return Result;
        //}


        [HttpPost]
        public Utility.CustomResponse AddUserPersonalization(PersonalizationDTO personalizationDTO)
        {
            try
            {
                TrainingPlanDTO PlanDetails = new TrainingPlanDTO();

               int TemplateID = UsersDAL.AddUserPersonalization(personalizationDTO);
           
               if (TemplateID > 0)
                {
                    Result.Response = TemplateID;
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Personalization_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        //[HttpPost]
        //public Utility.CustomResponse UpdateUserPersonalization(PersonalizationDTO personalizationDTO)
        //{
        //    try
        //    {
                
        //        int TemplateID = UsersDAL.UpdateUserPersonalization(personalizationDTO);

        //        if (TemplateID > 0)
        //        {
        //            Result.Response = TemplateID;
        //            Result.Status = Utility.CustomResponseStatus.Successful;
        //            Result.Message = CustomConstants.Personalization_Updated_Successfully;
        //        }
        //        else
        //        {
        //            Result.Status = Utility.CustomResponseStatus.UnSuccessful;
        //            Result.Message = CustomConstants.NoRecordsFound;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Result.Status = Utility.CustomResponseStatus.Exception;
        //        Result.Message = ex.Message;
        //    }
        //    return Result;
        //}

        [HttpPost]
        public Utility.CustomResponse UpdateUserTrainingStartDate(UserTrainingPlanDTO userTrainingPlanDTO)
        {
            try
            {

                    UsersDAL.UpdateUserTrainingStartDate(userTrainingPlanDTO); 
                    Result.Response = userTrainingPlanDTO;
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.TrainingPlan_StartDate_Updated_Successfully;
                

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddUserTrainingPlan(UserTrainingPlanDTO userTrainingPlanDTO)
        {
            try
            {
                int ID = UsersDAL.AddUserTrainingPlan(userTrainingPlanDTO);

                if (ID > 0)
                {
                    Result.Response = ID;
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.UserTrainingPlan_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse UpdateUserTrainingPlan(UserTrainingPlanDTO userTrainingPlanDTO)
        {
            try
            {
                int ID = UsersDAL.UpdateUserTrainingPlan(userTrainingPlanDTO);

                if (ID > 0)
                {
                    Result.Response = ID;
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.UserTrainingPlan_Updated_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddTrainingPlan(TrainingPlanDTO trainingPlanDTO)
        {
            try
            {
             
            
                int ID = UsersDAL.AddTrainingPlan(trainingPlanDTO);
                if (ID == -99)
                {

                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.TrainingPlan_Exists;
                }
                else if (ID > 0)
                {
                    TrainingVideoDTO trainingVideoDTO = new TrainingVideoDTO();
                    List<int> videoids = trainingPlanDTO.VideoId;
                    if (videoids.Count > 0)
                    {
                        for (int i = 0; i < videoids.Count; i++)
                        {
                            trainingVideoDTO.PlanId = ID;
                            trainingVideoDTO.VideoId = Convert.ToInt32(videoids[i]);
                            AddTrainingVideo(trainingVideoDTO);
                        }
                    }
                    
                    Result.Response = ID;
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.TrainingPlan_Added_Successfully;
                }              
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

       

        [HttpPost]
        public Utility.CustomResponse UpdateTrainingPlan(TrainingPlanDTO trainingPlanDTO)
        {
            try
            {
                int ID = UsersDAL.UpdateTrainingPlan(trainingPlanDTO);

                if (ID > 0)
                {
                

                    TrainingVideoDTO trainingVideoDTO = new TrainingVideoDTO();
                    UsersDAL.DeleteTrainingVideos(trainingPlanDTO.Id);
                    List<int> videoids = trainingPlanDTO.VideoId;
                    if (videoids.Count > 0)
                    {
                        for (int i = 0; i < videoids.Count; i++)
                        {
                            trainingVideoDTO.PlanId = ID;
                            trainingVideoDTO.VideoId = Convert.ToInt32(videoids[i]);
                            AddTrainingVideo(trainingVideoDTO);
                        }
                    }

                    Result.Response = ID;
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.TrainingPlan_Updated_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse UpdateQuestionAnswer(UserTrainingPlanDTO userTrainingPlanDTO)
        {
            try
            {
                    UsersDAL.UpdateQuestionAnswer(userTrainingPlanDTO);                 
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.TrainingPlan_Updated_Successfully;
                
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
        [HttpPost]
        public Utility.CustomResponse AddTrainingVideo(TrainingVideoDTO trainingVideoDTO)
        {
            try
            {
                int ID = UsersDAL.AddTrainingVideo(trainingVideoDTO);

                if (ID > 0)
                {
                    Result.Response = ID;
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.TrainingVideo_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        //[HttpPost]
        //public Utility.CustomResponse UpdateTrainingVideo(TrainingVideoDTO trainingVideoDTO)
        //{
        //    try
        //    {
        //        int ID = UsersDAL.UpdateTrainingVideo(trainingVideoDTO);

        //        if (ID > 0)
        //        {
        //            Result.Response = ID;
        //            Result.Status = Utility.CustomResponseStatus.Successful;
        //            Result.Message = CustomConstants.TrainingVideo_Updated_Successfully;
        //        }
        //        else
        //        {
        //            Result.Status = Utility.CustomResponseStatus.UnSuccessful;
        //            Result.Message = CustomConstants.NoRecordsFound;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Result.Status = Utility.CustomResponseStatus.Exception;
        //        Result.Message = ex.Message;
        //    }
        //    return Result;
        //}

        [HttpGet]
        public Utility.CustomResponse TrainingPlanVideos(int TrainingPlanId)
        {
            try
            {

                List<VideosInfoDTO> videos = UsersDAL.GetTrainingPlanVideos(TrainingPlanId);
                if (videos.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videos;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse UploadRecordedVideo(TrainingRecordedVideosDTO trainingRecordedVideosDTO)
        {
            try
            {
                int recordedvideoid = 0;
                int traningRecordVideoId = 0;
                SqlConnection conn = UsersDAL.ConnectTODB();
                SqlTransaction transaction;
                transaction = conn.BeginTransaction();
                SqlCommand command = conn.CreateCommand();
                command.Transaction = transaction;

                try
                {
                     
                    trainingRecordedVideosDTO.Orientation = 0;
                    trainingRecordedVideosDTO.Facing = 0;
                    trainingRecordedVideosDTO.Status = 7;                     
                    VideosController video = new VideosController();
                    var response = video.AdminAddVideo(trainingRecordedVideosDTO);
                    recordedvideoid = Convert.ToInt32(response.Response);
                    trainingRecordedVideosDTO.RecordedVideoId = recordedvideoid;
                    int trainVideoId = UsersDAL.GetIdealVideoId(trainingRecordedVideosDTO.SubCategory);
                    trainingRecordedVideosDTO.TrainingVideoId = trainVideoId; 
                    
                    if (recordedvideoid > 0)
                    {
                        traningRecordVideoId = UsersDAL.AddRecordedVideos(trainingRecordedVideosDTO);
                        UsersDAL.UpdateUserTrainingPlanRecordId(trainingRecordedVideosDTO);
                        transaction.Commit();
                        if (traningRecordVideoId > 0)
                        {
                            Result.Status = Utility.CustomResponseStatus.Successful;
                            Result.Response = recordedvideoid;
                            Result.Message = CustomConstants.Details_Get_Successfully;
                        }
                        else
                        {
                            Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                            Result.Message = CustomConstants.NoRecordsFound;
                        }
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse UserTrainingPlanDetails(int TrainingTemplateID, int UserId)
        {
            try
            {

                UserTrainingPlanDTO TrainingPlanDetails = UsersDAL.GetUserTrainingPlanDetails(TrainingTemplateID, UserId);
                if (TrainingPlanDetails.TrainingPlanId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = TrainingPlanDetails;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetEventTypes(int age, int gender)
        {
            try
            {

                List<EventTypeDTO> eventType = new List<EventTypeDTO>();
                eventType = UsersDAL.GetEventTypes(age,gender);
                if (eventType.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = eventType;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        //Portal 
        [HttpGet]
        public Utility.CustomResponse GetBestTimes(int eventid, int age, int gender)
        {
            try
            {
 
                List<BestTimesValuesDTO> BestTimes = new List<BestTimesValuesDTO>();
                BestTimes = UsersDAL.GetBestTimes(eventid, age, gender);
                if (BestTimes.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = BestTimes;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetTrainingPlan(int Planid)
        {
            try
            {

                PlanDTO TrainingPlan = new PlanDTO();
                TrainingPlan = UsersDAL.GetTrainingPlan(Planid);
                if (TrainingPlan.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = TrainingPlan;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse GetTrainingPlanDays(TrainingPlanDTO planDTO)
        {
            try
            {

                List<PlanDaysDTO> planDaysDTO = new List<PlanDaysDTO>();
                planDaysDTO = UsersDAL.GetTrainingPlanDays(planDTO);
                if (planDaysDTO.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = planDaysDTO;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse RelavantVideosForSubcategory(string Subcategory)
        {
            try
            {

                List<VideoURLDTO> videos = UsersDAL.GetRelavantVideosForSubcategory(Subcategory);
                if (videos.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = videos;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        //[HttpGet]
        //public Utility.CustomResponse AllRelavantVideosForStrokes()
        //{
        //    try
        //    {

        //        List<VideoURLDTO> videos = UsersDAL.GetRelavantVideosForStrokes("");
        //        if (videos.Count > 0)
        //        {
        //            Result.Status = Utility.CustomResponseStatus.Successful;
        //            Result.Response = videos;
        //            Result.Message = CustomConstants.Details_Get_Successfully;
        //        }
        //        else
        //        {
        //            Result.Status = Utility.CustomResponseStatus.UnSuccessful;
        //            Result.Message = CustomConstants.NoRecordsFound;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Result.Status = Utility.CustomResponseStatus.Exception;
        //        Result.Message = ex.Message;
        //    }
        //    return Result;
        //}

        [HttpGet]
        public Utility.CustomResponse GetEventTypes(int userid)
        {
            try
            {
                UserInfoDTO userInfo = new UserInfoDTO();
                userInfo = UsersDAL.GetUserAgeGender(userid);

                List<EventTypeDTO> eventTypes = new List<EventTypeDTO>();
                eventTypes = UsersDAL.GetEventTypes(userInfo.Age, userInfo.Gender);
                //if (eventType.Count > 0)
                //{
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = eventTypes;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                //}
                //else
                //{
                //    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                //    Result.Message = CustomConstants.NoRecordsFound;
                //}
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
        [HttpGet]
        public Utility.CustomResponse GetBestTimes(int eventid, int userid)
        {
            try
            {
                UserInfoDTO userInfo = new UserInfoDTO();
                userInfo = UsersDAL.GetUserAgeGender(userid);

                List<BestTimesValuesDTO> BestTimes = new List<BestTimesValuesDTO>();
                BestTimes = UsersDAL.GetBestTimes(eventid, userInfo.Age, userInfo.Gender);
                if (BestTimes.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = BestTimes;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
    }
}
