﻿using Microsoft.WindowsAzure.Storage;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.WindowsAzure.MediaServices.Client;
using System.Text;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using Ikkos.Models;


namespace Ikkos
{
    public partial class UploadRecordedVideo : System.Web.UI.Page
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _result = Handlefile();
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string jsonString = javaScriptSerializer.Serialize(_result);
                Response.Write(jsonString);
            }
        }

        private Utility.CustomResponse Handlefile()
        {
            string baseUrl = ConfigurationManager.AppSettings["BaseURL"];
            Stream stream = Request.InputStream;
            string videoFormat = Request.QueryString["VideoFormat"].Trim();
            string duration = Request.QueryString["Duration"].Trim();
            int UserID = Convert.ToInt32(Request.QueryString["UserId"].Trim());
            string MovementName = Request.QueryString["VideoName"].Trim();
            RecordedVideoInfoDTO video = new RecordedVideoInfoDTO();
            string videoUrl = Streamaudio(stream, videoFormat);
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


            video.ID = 0;
            video.VideoUrl = videoUrl;
            video.CreatedBy = UserID;
            video.Format = videoFormat;
            video.Duration = Convert.ToDouble(duration);
            video.Status = 1;
            video.MovementName = MovementName;
            video.CreatedDate = DateTime.Now;
            video.ThumbnailUrl = GetVideoThumbnails(video); 

            var response = client.PostAsJsonAsync("VideosV2/AddUserRecordedVideo", video).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;  
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = Convert.ToInt32(res.Response);
                _result.Message = CustomConstants.Video_Added_Successfully;
            }
            return _result;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        public string GetVideoThumbnails(RecordedVideoInfoDTO videoResp)
        {
            var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
            double totDuration = videoResp.Duration;
            float startPoint = 0;
            float dur = (float)totDuration / 6;

            var frame = startPoint + (float)Math.Round(dur, 2);
            string thumbnailJpeGpath = System.Web.HttpContext.Current.Server.MapPath("~/videoThumb" + ".jpg");
            ffMpeg.GetVideoThumbnail(videoResp.VideoUrl, thumbnailJpeGpath, frame);
            byte[] bytes = System.IO.File.ReadAllBytes(thumbnailJpeGpath);
            string base64String = Convert.ToBase64String(bytes);
            string thumbnailUrl = GetImageUrl(base64String, ".jpg", "");
            //RecordedThumbnailDTO RecThumbnailDto = new RecordedThumbnailDTO();
            //RecThumbnailDto.UserId = videoResp.CreatedBy;
            //RecThumbnailDto.ThumbnailURL = thumbnailUrl;
            //RecThumbnailDto.VideoId = videoResp.ID;
            ////Add Image to the DB 
            //AddImageToDb(RecThumbnailDto);
            startPoint = startPoint + (float)Math.Round(dur, 2);
            return thumbnailUrl;

        }
        public string Streamaudio(Stream stream, string format)
        {
            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);
            var fileStream = System.IO.File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();
            var ext = format;
            Stream fileStreambanners = stream;
            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string videoUrl = UploadImage1(fileNamebanners, path, ext, fileStreambanners);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            return videoUrl;
        }

        public string UploadImage1(string fileName, string path, string ext, Stream inputStream)
        {
            string account = ConfigurationManager.AppSettings["MediaServicesAccountName"];
            string key = ConfigurationManager.AppSettings["MediaServicesAccountKey"];
            CloudMediaContext context = new CloudMediaContext(account, key);
            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);
            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));
            assetFile.Upload(path);
            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 10000;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive), AccessPermissions.Read);
            string streamingUrl = string.Empty;
            var assetFiles = streamingAsset.AssetFiles.ToList();
            var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".mp4")).FirstOrDefault();
            if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            {
                var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                var mp4Uri = new UriBuilder(locator.Path);
                mp4Uri.Path += "/" + streamingAssetFile.Name;
                streamingUrl = mp4Uri.ToString();
            }
            return streamingUrl;
        }

        public string GetImageUrl(string binary, string format, string contentType)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();
                string filename = "" + r.Next(999999) + DateTime.Now.Millisecond + r.Next(99999) + format;
                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();
                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });
                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
                // Create or overwrite the "myblob" blob with contents from a local file.
                byte[] binarydata = Convert.FromBase64String(binary);
                blockBlob.Properties.ContentType = contentType;
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);
                //UploadFromStream(fileStream);
                //}
                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);
                imageurl = blockBlob1.Uri.ToString();
            }
            catch (Exception ex)
            {
                //lblstatus.Text = "Failed to upload image to Azure Server";
                _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                _result.Message = "Failed to add image";
            }
            return imageurl;
        }
        //private int AddImageToDb(RecordedThumbnailDTO RecThumbnailDto)
        //{
        //    int id = 0;
        //    string baseUrl = ConfigurationManager.AppSettings["BaseURL"];
        //    HttpClient client = new HttpClient();
        //    client.BaseAddress = new Uri(baseUrl);
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //    var response = client.PostAsJsonAsync("VideosV2/UpdateVideoThumbnail", RecThumbnailDto).Result;
        //    if (response.IsSuccessStatusCode)
        //    {
        //        Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
        //        id = Convert.ToInt32(res.Response);
        //        _result.Status = Utility.CustomResponseStatus.Successful;
        //        _result.Response = res.Response;
        //        _result.Message = CustomConstants.ThumbnailUpdateSuccess;
        //    }
        //    return id;
        //}

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    _result = Handlefile();
        //    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //    string jsonString = javaScriptSerializer.Serialize(_result);
        //    Response.Write(jsonString);
        //}
    }
}